package com.drummerscope.android.GlobalSettings;

import android.app.Fragment;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.drummerscope.android.R;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;

/**
 * Класс, отвечающий за выбор цвета для нот на нотном стане
 * <p>
 * Created by kmoaz on 10.02.2017.
 */

public class ColorPickerFragment extends Fragment {

    /**
     * Метод инициализирует View фрагмента для выбора цвета нот
     *
     * @param inflater           - компонент для заполнения View
     * @param container          - контейнер View
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает View фрагмента для выбора цвета нот
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.color_picker, container, false);
        try {
            final ReadWriteJSONSettings readWriteJSONSettings = new ReadWriteJSONSettings(view.getContext());

            final ColorPicker picker = (ColorPicker) view.findViewById(R.id.picker);
            final OpacityBar opacityBar = (OpacityBar) view.findViewById(R.id.opacitybar);
            final SaturationBar saturationBar = (SaturationBar) view.findViewById(R.id.saturationbar);
            final ValueBar valueBar = (ValueBar) view.findViewById(R.id.valuebar);

            picker.addOpacityBar(opacityBar);
            picker.addSaturationBar(saturationBar);
            picker.addValueBar(valueBar);
            picker.setColor(Integer.valueOf(readWriteJSONSettings.read("notecolor")));

            final Button save = (Button) view.findViewById(R.id.save_color);
            save.setBackgroundDrawable(customView(picker.getColor()));
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    readWriteJSONSettings.write(String.valueOf(picker.getColor()), "notecolor");
                    getFragmentManager().popBackStack();
                }
            });

            final Button cancel = (Button) view.findViewById(R.id.cancel_color);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().popBackStack();
                }
            });

            picker.setOnColorChangedListener(new ColorPicker.OnColorChangedListener() {
                @Override
                public void onColorChanged(int color) {
                    save.setBackgroundDrawable(customView(picker.getColor()));
                }
            });

        } catch (Exception e) {
            Toast.makeText(view.getContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    /**
     * Метод создает фигуру с градиентом
     *
     * @param color - исходный цвет
     * @return возвращает фигуру с градиентом указанного цвета
     */
    private GradientDrawable customView(int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{10, 10, 10, 10, 10, 10, 10, 10});
        shape.setColor(color);
        return shape;
    }
}
