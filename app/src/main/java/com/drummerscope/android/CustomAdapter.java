package com.drummerscope.android;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Класс для кастомизации списка доната
 *
 * Created by kmoaz on 07.02.2017.
 */

public class CustomAdapter extends BaseAdapter {

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Поле - Массив названий элементов списка
     */

    private String[]  Title;

    /**
     * Поле - Массив ID картинок элементов списка
     */

    private int[] imge;

    /**
     * Конструктор
     * @param context - Контекст
     * @param text1 - Массив названий элементов списка
     * @param imageIds - Массив ID картинок элементов списка
     */

    public CustomAdapter(Context context, String[] text1, int[] imageIds) {
        mContext = context;
        Title = text1;
        imge = imageIds;
    }

    /**
     * Геттер для количества элементов списка
     * @return Количество элементов списка
     */

    public int getCount() {
        // TODO Auto-generated method stub
        return Title.length;
    }

    /**
     * Геттер для получения объекта по его ID
     * @param arg0 - ID объекта
     * @return Необходимый объект
     */

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Геттер для получения ID элемента по его номеру в списке
     * @param position - Номер элемента в списке
     * @return Номер элемента в списке
     */

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View row;
        row = inflater.inflate(R.layout.drawer_list_item, parent, false);
        TextView title;
        ImageView i1;
        i1 = (ImageView) row.findViewById(R.id.imgIcon);
        title = (TextView) row.findViewById(R.id.text1);
        title.setText(Title[position]);

        if (TextUtils.equals(Title[position], "Donate")) {
            row.setBackgroundColor(row.getContext().getResources().getColor(R.color.myRed));
            title.setBackgroundColor(row.getContext().getResources().getColor(R.color.myRed));
        }

        i1.setImageResource(imge[position]);

        return (row);
    }
}