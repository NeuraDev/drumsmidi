package com.drummerscope.android.ShareDialogMenu;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Класс список для выбора метода расшарить
 *
 * Created by kmoaz on 22.08.2016.
 */
public class ChooserArrayAdapter extends ArrayAdapter<String> {

    /**
     * Поле - Объект текущего расположения класса
     */

    PackageManager mPm;

    /**
     * Поле - ID текствью списка
     */

    int mTextViewResourceId;

    /**
     * Поле - Текстовые значения элементов списка
     */

    List<String> mPackages;

    /**
     * Конструктор
     * @param context - Контекст
     * @param resource - Объект ресурсов приложения
     * @param textViewResourceId - ID текствью списка
     * @param packages - Текстовые значения элементов списка
     */

    public ChooserArrayAdapter(Context context, int resource, int textViewResourceId, List<String> packages) {
        super(context, resource, textViewResourceId, packages);
        mPm = context.getPackageManager();
        mTextViewResourceId = textViewResourceId;
        mPackages = packages;
    }

    /**
     * Метод для отрисовки элементов списка
     * @param position
     * @param convertView
     * @param parent
     * @return
     */

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String pkg = mPackages.get(position);
        View view = super.getView(position, convertView, parent);

        try {
            ApplicationInfo ai = mPm.getApplicationInfo(pkg, 0);

            CharSequence appName = mPm.getApplicationLabel(ai);
            Drawable appIcon = mPm.getApplicationIcon(pkg);

            TextView textView = (TextView) view.findViewById(mTextViewResourceId);
            textView.setText(appName);
            textView.setCompoundDrawablesWithIntrinsicBounds(appIcon, null, null, null);
            textView.setCompoundDrawablePadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getContext().getResources().getDisplayMetrics()));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return view;
    }

}
