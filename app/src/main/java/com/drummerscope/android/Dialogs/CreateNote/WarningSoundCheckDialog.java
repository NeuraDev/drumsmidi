package com.drummerscope.android.Dialogs.CreateNote;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.drummerscope.android.R;

/**
 * Класс, отвечающий за уведомление пользователя о конфликте существующих и добавляемых нот и
 * обработку результата в соответствующем диалоговом окне.
 *
 * Created by evgen on 21.02.2017.
 */

public class WarningSoundCheckDialog extends DialogFragment {

    private static final String TAG = "EXISTING NOTE DIALOG";
    /**
     * Интерфейс для обработки результатов
     */
    private DialogClickListener callback;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.warning_sound_check_dialog, null);

        try {
            callback = (DialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

        TextView titleOfDialog = (TextView) view.findViewById(R.id.title_warning_sound_check_dialog);
        TextView textOfDialog = (TextView) view.findViewById(R.id.text_warning_sound_check_dialog);

        String textForTitleOfDialog = "Warning";
        titleOfDialog.setText(textForTitleOfDialog);

        String textForTextViewOfDialog = "The current configuration will be erased! Do you want to continue?";
        textOfDialog.setText(textForTextViewOfDialog);

        MaterialDialog materialDialog = new MaterialDialog.Builder(view.getContext())
                .customView(view, false)
                .backgroundColor(getResources().getColor(R.color.transparent))
                .backgroundColorRes(R.color.transparent)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Log.e(TAG, "YES CLICK");
                        callback.onYesClick();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        callback.onNoClick();
                    }
                })
                .negativeText("No")
                .positiveText("Yes")
                .negativeColor(getResources().getColor(R.color.myRed))
                .positiveColor(getResources().getColor(R.color.myRed))
                .cancelable(true)
                .build();

        /* Событие нажатия на back */
        materialDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });

        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        materialDialog.getWindow().setBackgroundDrawableResource(R.drawable.create_note_dialog);

        return materialDialog;
    }

}
