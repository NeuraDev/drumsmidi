package com.drummerscope.android.Dialogs.CreateNote;


import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.drummerscope.android.R;
import com.google.firebase.crash.FirebaseCrash;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс, отвечающий за режим саундчека в диалоговом окне.
 *
 * Created by evgenyrasskazov on 04.11.2016.
 */

public class SoundCheckDialog extends DialogFragment implements DialogClickListener {

    private static final String TAG = "SOUND_CHECK";
    /**
     * Поле - объем массива для нот
     */
    private static final int N = 15;
    /**
     * Поле - открыто ли диалоговое окно
     */
    public static boolean dialogOpened = false;
    /**
     * Текстовое поле для названия ноты
     */
    private static TextView noteInfoTextView;
    /**
     * Макеты и View, содержащие информацию о нотах.
     */
    private static LinearLayout linearLayoutVertical;
    private static LinearLayout[] linearLayoutHorizontal = new LinearLayout[N];
    private static TextView[] noteTextView = new TextView[N];
    private static Spinner[] noteImageSpinner;
    private static ImageView[] deleteImageView = new ImageView[N];
    /**
     * Объекты JSON для хранения добавляемых нот
     */
    private static List<String> notes;
    private static JSONObject jsonTracksWithNotes;
    private static JSONObject jsonNotes;
    private static JSONObject jsonTracksWithNoteImages;
    private static JSONObject jsonImages;
    private static JSONObject[] jsonNotesState;
    private static JSONObject[] jsonImagesForNotesState;
    /**
     * Поле - этап саундчека
     */
    private static int soundCheckStep;
    /**
     * Поле - это саундчек?
     */
    private static boolean soundCheck;
    /**
     * Поле - номер дорожки
     */
    private static int trackNumber;
    /**
     * Параметры для соответствующей верстки
     */
    private static LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);
    private static LinearLayout.LayoutParams deleteImageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);
    private static LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);
    private static LinearLayout.LayoutParams linearLayoutHorizontalParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);
    /**
     * Массив с названиями картинок нот
     */
    private static String[] imageNameDatabase = {"triangle_for_settings", "rhombus", "line", "cross_in_circle", "cross"};
    /**
     * Ссылки на кнопки подтверждения и отклонения внесенных изменений
     */
    MDButton positiveBtn;
    MDButton negativeBtn;
    /**
     * Текстовое поле для отображения названия ноты
     */
    private TextView checkSoundText;
    /**
     * Адаптер для отображения картинок нот в выпадающем Spinner-е
     */
    private NoteImageSpinnerAdapter imagesSpinnerAdapter;
    /**
     * View диалогового окна
     */
    private View dialogView;
    /**
     * Массив - названия дорожек
     */
    private String[] trackNames = new String[6];
    /**
     * Интерфейс с методами обратного вызова
     */
    private DialogClickListener callback;

    /**
     * Метод создания экземпляра диалогового окна
     *
     * @param num - номер дорожки
     * @return возвращает экземпляр диалогового окна
     */
    public static SoundCheckDialog newInstance(int num) {
        SoundCheckDialog f = new SoundCheckDialog();
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }

    public static void setNoteInfoTextView(String message) {
        notes.add(message);
        boolean noteExists = false;
        for (int i = 0; i < 6; i++) {
            if (jsonNotesState[i] != null) {
                if (jsonNotesState[i].length() > 0) {
                    for (int j = 0; j < jsonNotesState[i].length(); j++) {
                        try {
                            String note = jsonNotesState[i].getString("note" + String.valueOf(j));
                            if (message.equals(note)) {
                                noteExists = true;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        Log.e(TAG, "NOTES LIST SIZE = " + String.valueOf(notes.size()));
        noteInfoTextView.setText("Beat: " + message);

        if (notes.size() >= 4 && !noteExists) {
            if (notes.get(notes.size() - 1).equals(notes.get(notes.size() - 2)) && notes.get(notes.size() - 3).equals(notes.get(notes.size() - 4)) && notes.get(notes.size() - 1).equals(notes.get(notes.size() - 3))) {
                try {

                    String track = "track" + String.valueOf(trackNumber);
                    Log.e(TAG, "IF IS PASSED");
                    int existNoteSum;

                    /** If there are data at this track */

                    if (jsonNotesState[trackNumber] != null && jsonNotes != null) {
                        //Log.e(TAG, "JSON STATE = " + jsonNotesState[trackNumber].toString() + " JSON NOTES = " + jsonNotes.toString());
                        if (jsonNotesState[trackNumber].length() > 0 && jsonNotes.length() == 0) {
                            jsonNotes = jsonNotesState[trackNumber];
                            Log.e(TAG, "JSON NOTES IS NULL => FILLING FROM EXISTING");
                        }
                    }

                    if (jsonNotes != null) {
                        existNoteSum = jsonNotes.length();
                        Log.e(TAG, " JSON NOTES = " + jsonNotes.toString());
                    } else {
                        existNoteSum = 0;
                    }

                    String note = "note" + String.valueOf(existNoteSum);
                    if (jsonNotes != null) {
                        if (jsonNotes.length() < 10) {

                            boolean isExist = false;
                            for (int i = 0; i < 6; i++) {
                                if (jsonNotesState[i] != null) {
                                    for (int j = 0; j < jsonNotesState[i].length(); j++) {
                                        if (jsonNotesState[i].getString("note" + String.valueOf(j)).equals(message)) {
                                            isExist = true;
                                            break;
                                        }
                                    }

                                }
                            }

                            if (!isExist) {
                                jsonNotes.put(note, message);
                                jsonNotesState[trackNumber] = jsonNotes;

                                jsonImages.put(note, imageNameDatabase[noteImageSpinner[existNoteSum].getSelectedItemPosition()]);
                                jsonImagesForNotesState[trackNumber] = jsonImages;

                                noteTextView[existNoteSum].setText(message);
                                linearLayoutVertical.addView(linearLayoutHorizontal[existNoteSum]);
                                linearLayoutVertical.requestLayout();
                                linearLayoutVertical.invalidate();

                                Log.e(TAG, "JSON NOTES AFTER DRAWING = " + jsonNotes.toString());
                                Log.e(TAG, "JSON IMAGES AFTER DRAWING = " + jsonImages.toString());
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    FirebaseCrash.report(e);
                }
            }
        }
    }

    public static boolean getSoundCheck() {
        return soundCheck;
    }

    /**
     * Метод создания и наполнения диалога контентом
     *
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает заполненный экземпляр диалогового окна
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        dialogView = inflater.inflate(R.layout.sound_check_dialog, null);
        for (int i = 0; i < 6; i++) {
            try {
                trackNames[i] = new JSONObject(readJSON("trackNames")).getString("track" + String.valueOf(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        trackNumber = 0;
        noteImageSpinner = new Spinner[N];
        jsonNotesState = new JSONObject[6];
        jsonImagesForNotesState = new JSONObject[6];
        jsonNotes = new JSONObject();
        jsonTracksWithNotes = new JSONObject();
        jsonTracksWithNoteImages = new JSONObject();
        jsonImages = new JSONObject();

        setTextViewParams(textViewParams);
        setSpinnerParams(spinnerParams);
        setDeleteImageParams(deleteImageParams);
        setLinearLayoutParams(linearLayoutHorizontalParams);

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData(R.drawable.ic_triangle_notes));
        list.add(new ItemData(R.drawable.ic_rhombus_notes));
        list.add(new ItemData(R.drawable.ic_line_notes));
        list.add(new ItemData(R.drawable.ic_cross_in_circle_notes));
        list.add(new ItemData(R.drawable.ic_cross_notes));

        imagesSpinnerAdapter = new NoteImageSpinnerAdapter(getActivity(), R.id.image_for_create_note_spinner, list);

        linearLayoutVertical = (LinearLayout) dialogView.findViewById(R.id.linear_layout_vertical_sound_check);

        createEmptyLayouts();

        WarningSoundCheckDialog warningSoundCheckDialog = new WarningSoundCheckDialog();
        warningSoundCheckDialog.setTargetFragment(this, 0);
        warningSoundCheckDialog.show(((Activity) dialogView.getContext()).getFragmentManager(), warningSoundCheckDialog.getClass().getName());
        dialogOpened = true;

        MaterialDialog materialDialog = new MaterialDialog.Builder(dialogView.getContext())
                .customView(dialogView, false)
                .backgroundColor(getResources().getColor(R.color.transparent))
                .backgroundColorRes(R.color.transparent)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showNextDialog();
                    }
                })
                .positiveText("Next")
                .negativeColor(getResources().getColor(R.color.myRed))
                .positiveColor(getResources().getColor(R.color.myRed))
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showPreviousDialog();
                    }
                })
                .negativeText("Close")
                .autoDismiss(false)
                .cancelable(false)
                .build();

        positiveBtn = materialDialog.getActionButton(DialogAction.POSITIVE);
        negativeBtn = materialDialog.getActionButton(DialogAction.NEGATIVE);

        /* Событие нажатия на back */
        materialDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });

        try {
            callback = (DialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

        noteInfoTextView = (TextView) dialogView.findViewById(R.id.noteInfoSoundCheck);
        checkSoundText = (TextView) dialogView.findViewById(R.id.info_text);

        String message = "Here you can see an information about notes";
        noteInfoTextView.setText(message);

        soundCheckStep = 1;
        notes = new ArrayList<>();
        soundCheck = true;

        beginStep1();

        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        materialDialog.getWindow().setBackgroundDrawableResource(R.drawable.create_note_dialog);
        return materialDialog;
    }

    /**
     * Метод для вызова следующего диалогового окна соответствующего этапа саундчека
     */
    void showNextDialog() {
        toTheNextStep();
    }

    /**
     * Метод для вызова предыдущего диалогового окна соответствующего этапа саундчека
     */
    void showPreviousDialog() {
        toThePreviousStep();
    }

    /**
     * Метод сохранения нынешнеого состояния и заполнения информацией
     */
    void toTheNextStep() {
        trackNumber++;
        soundCheckStep++;

        negativeBtn.setText("Previous");
        negativeBtn.invalidate();
        Log.e(TAG, "TRACK NUMBER = " + String.valueOf(trackNumber));
        Log.e(TAG, "SOUND CHECK STEP = " + String.valueOf(soundCheckStep));

        /*
        Если текущая дорожка - последняя, то записывается JSON-файл с нотами
        Иначе - JSON-файл заполняется с текущей дорожкой
         */
        if (trackNumber == 6) {
            jsonTracksWithNotes = new JSONObject();
            jsonTracksWithNoteImages = new JSONObject();
            for (int i = 0; i < 6; i++) {
                try {
                    jsonTracksWithNotes.put("track" + String.valueOf(i), jsonNotesState[i]);
                    jsonTracksWithNoteImages.put("track" + String.valueOf(i), jsonImagesForNotesState[i]);
                    Log.e(TAG, "RECORDING : JSON NOTES = " + jsonTracksWithNotes.toString() + " |||  JSON IMAGES = " + jsonTracksWithNoteImages.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            recordJSON("notes");
            recordJSON("images");
            callback.onYesClick();
            dismiss();
        } else {
            if (trackNumber == 5) {
                positiveBtn.setText("Save & Exit");
                positiveBtn.invalidate();
            }
            /* Вложение картинок с предыдущего диалога в JSON-объект  */
            if (jsonNotes != null) {
                if (jsonNotes.length() > 0) {
                    Log.e(TAG, "JSON NOTES: " + jsonNotes.toString());
                    for (int i = 0; i < jsonNotes.length(); i++) {
                        try {
                            noteImageSpinner[i].requestLayout();
                            noteImageSpinner[i].invalidate();
                            jsonImages.put("note" + String.valueOf(i), imageNameDatabase[noteImageSpinner[i].getSelectedItemPosition()]);
                            Log.e(TAG, "IMG FROM BASE TO SAVE = " + imageNameDatabase[noteImageSpinner[i].getSelectedItemPosition()]);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (jsonNotesState[trackNumber - 1] != null) {
                    Log.e(TAG, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    if (jsonNotesState[trackNumber - 1].length() > 0) {
                        for (int i = 0; i < jsonNotesState[trackNumber - 1].length(); i++) {
                            try {
                                jsonNotes.put("note" + String.valueOf(i), jsonNotesState[trackNumber - 1].getString("note" + String.valueOf(i)));
                                jsonImages.put("note" + String.valueOf(i), imageNameDatabase[noteImageSpinner[i].getSelectedItemPosition()]);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            /* Сохранение состояния предыдущего диалога */
            if (jsonNotes != null) {
                if (jsonNotes.length() > 0) {
                    Log.e(TAG, "SAVING IN TOTHENEXT");
                    jsonNotesState[trackNumber - 1] = jsonNotes;
                    jsonImagesForNotesState[trackNumber - 1] = jsonImages;

                }
            }

            linearLayoutVertical.removeAllViews();
            /* Если на текущей дорожке есть инофрмация - она добавляется в макет */
            if (jsonNotesState[trackNumber] != null) {
                if (jsonNotesState[trackNumber].length() > 0) {
                    Log.e(TAG, "NEXT STEP STATE IS NOT NULL, JSON STATE = " + jsonNotesState[trackNumber].toString());
                    for (int i = 0; i < jsonNotesState[trackNumber].length(); i++) {
                        try {
                            String imageTextString = jsonImagesForNotesState[trackNumber].getString("note" + String.valueOf(i));
                            String noteTextString = jsonNotesState[trackNumber].getString("note" + String.valueOf(i));
                            Log.e(TAG, "NOTE TEXT = " + noteTextString);
                            noteTextView[i].setText(noteTextString);
                            for (int j = 0; j < imageNameDatabase.length; j++) {
                                if (imageNameDatabase[j].equals(imageTextString)) {
                                    Log.e(TAG, "IMG TEXT = " + imageTextString);
                                    noteImageSpinner[i].setSelection(j);
                                }
                            }
                            linearLayoutVertical.addView(linearLayoutHorizontal[i]);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            linearLayoutVertical.requestLayout();
            linearLayoutVertical.invalidate();

            jsonNotes = new JSONObject();
            jsonImages = new JSONObject();
            switch (soundCheckStep) {
                case 1:
                    beginStep1();
                    notes = new ArrayList<>();
                    break;
                case 2:
                    beginStep2();
                    notes = new ArrayList<>();
                    break;
                case 3:
                    beginStep3();
                    notes = new ArrayList<>();
                    break;
                case 4:
                    beginStep4();
                    notes = new ArrayList<>();
                    break;
                case 5:
                    beginStep5();
                    notes = new ArrayList<>();
                    break;
                case 6:
                    beginStep6();
                    notes = new ArrayList<>();
                    break;
                case 7:
                    end();
                    soundCheck = false;
                    trackNumber = 0;
                    notes = new ArrayList<>();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Метод перехода на предыдущий этап
     */
    void toThePreviousStep() {
        trackNumber--;
        soundCheckStep--;
        positiveBtn.setText("Next");
        positiveBtn.invalidate();

        Log.e(TAG, "TRACK NUMBER = " + String.valueOf(trackNumber));
        Log.e(TAG, "SOUND CHECK STEP = " + String.valueOf(soundCheckStep));

        if (trackNumber == -1) {
            jsonNotesState = new JSONObject[6];
            jsonImagesForNotesState = new JSONObject[6];
            dismiss();
        } else {
            if (trackNumber == 0) {
                negativeBtn.setText("Close");
                negativeBtn.invalidate();
            } else {
                negativeBtn.setText("Previous");
                negativeBtn.invalidate();
            }

            /* Вложение картинок с предыдущего диалога в JSON-объект  */
            if (jsonNotes != null) {
                if (jsonNotes.length() > 0) {
                    Log.e(TAG, "JSON NOTES: " + jsonNotes.toString());
                    for (int i = 0; i < jsonNotes.length(); i++) {
                        try {
                            noteImageSpinner[i].requestLayout();
                            noteImageSpinner[i].invalidate();
                            jsonImages.put("note" + String.valueOf(i), imageNameDatabase[noteImageSpinner[i].getSelectedItemPosition()]);
                            Log.e(TAG, "IMG FROM BASE TO SAVE = " + imageNameDatabase[noteImageSpinner[i].getSelectedItemPosition()]);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (jsonNotesState[trackNumber + 1] != null) {
                    Log.e(TAG, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    if (jsonNotesState[trackNumber + 1].length() > 0) {
                        for (int i = 0; i < jsonNotesState[trackNumber + 1].length(); i++) {
                            try {
                                jsonNotes.put("note" + String.valueOf(i), jsonNotesState[trackNumber + 1].getString("note" + String.valueOf(i)));
                                jsonImages.put("note" + String.valueOf(i), imageNameDatabase[noteImageSpinner[i].getSelectedItemPosition()]);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            /* Сохранение состояние предыдущего диалога */
            if (jsonNotes != null) {
                if (jsonNotes.length() > 0) {
                    jsonNotesState[trackNumber + 1] = jsonNotes;
                    jsonImagesForNotesState[trackNumber + 1] = jsonImages;
                    Log.e(TAG, "SAVING IN TOTHEPREV");
                }
            }

            linearLayoutVertical.removeAllViews();
            if (jsonNotesState[trackNumber] != null) {
                if (jsonNotesState[trackNumber].length() > 0) {
                    Log.e(TAG, "PREV STEP STATE IS NOT NULL, JSON STATE = " + jsonNotesState[trackNumber].toString());
                    for (int i = 0; i < jsonNotesState[trackNumber].length(); i++) {
                        try {

                            String imageTextString = jsonImagesForNotesState[trackNumber].getString("note" + String.valueOf(i));
                            String noteTextString = jsonNotesState[trackNumber].getString("note" + String.valueOf(i));
                            //Log.e(TAG, "IMAGE TEXT = " + imageTextString);
                            noteTextView[i].setText(noteTextString);
                            for (int j = 0; j < imageNameDatabase.length; j++) {
                                if (imageNameDatabase[j].equals(imageTextString)) {
                                    Log.e(TAG, "IMG TEXT = " + imageTextString);
                                    noteImageSpinner[i].setSelection(j);
                                }
                            }
                            linearLayoutVertical.addView(linearLayoutHorizontal[i]);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            linearLayoutVertical.requestLayout();
            linearLayoutVertical.invalidate();


            jsonNotes = new JSONObject();
            jsonImages = new JSONObject();

            switch (soundCheckStep) {
                case 1:
                    beginStep1();
                    notes = new ArrayList<>();
                    break;
                case 2:
                    beginStep2();
                    notes = new ArrayList<>();
                    break;
                case 3:
                    beginStep3();
                    notes = new ArrayList<>();
                    break;
                case 4:
                    beginStep4();
                    notes = new ArrayList<>();
                    break;
                case 5:
                    beginStep5();
                    notes = new ArrayList<>();
                    break;
                case 6:
                    beginStep6();
                    notes = new ArrayList<>();
                    break;
                case 7:
                    end();
                    soundCheck = false;
                    trackNumber = 0;
                    notes = new ArrayList<>();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @see CreateNoteDialog
     */
    public void setTextViewParams(LinearLayout.LayoutParams params) {
        int margin = dpToPixels(16);
        params.setMargins(margin, 0, 0, 0);
        params.gravity = Gravity.CENTER;
        params.weight = 1;
    }

    public void setSpinnerParams(LinearLayout.LayoutParams params) {
        int margin = dpToPixels(16);
        params.setMargins(margin, 0, 0, 0);
        params.gravity = Gravity.CENTER;

    }

    private void setDeleteImageParams(LinearLayout.LayoutParams params) {
        int margin = dpToPixels(16);
        params.setMargins(margin, 0, 0, 0);
        params.weight = 1;
        params.gravity = Gravity.RIGHT;
        params.gravity = Gravity.CENTER_VERTICAL;
    }

    private void setLinearLayoutParams(LinearLayout.LayoutParams params) {
        int margin = dpToPixels(16);
        params.setMargins(0, 0, 0, margin);
    }

    private int dpToPixels(int dpValue) {
        float d = dialogView.getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * d);
    }

    /**
     * Методы установки сообщений в соответствии с этапом
     */
    private void beginStep1() {
        String message = "Play to " + trackNames[soundCheckStep - 1];
        checkSoundText.setText(message);
    }

    private void beginStep2() {
        String message = "Play to " + trackNames[soundCheckStep - 1];
        checkSoundText.setText(message);
    }

    private void beginStep3() {
        String message = "Play to " + trackNames[soundCheckStep - 1];
        checkSoundText.setText(message);
    }

    private void beginStep4() {
        String message = "Play to " + trackNames[soundCheckStep - 1];
        checkSoundText.setText(message);
    }

    private void beginStep5() {
        String message = "Play to " + trackNames[soundCheckStep - 1];
        checkSoundText.setText(message);
    }

    private void beginStep6() {
        String message = "Play to " + trackNames[soundCheckStep - 1];
        checkSoundText.setText(message);
    }

    private void end() {
        checkSoundText.setText("Sound check is ended! Click on this window to leave");
        noteInfoTextView.setText("");
        Log.e(TAG, String.valueOf(jsonTracksWithNotes));
    }

    /**
     * Метод наполнения пустыми элементами интерфейса
     */
    private void createEmptyLayouts() {
        for (int i = 0; i < N; i++) {
            final int I = i;
            linearLayoutHorizontal[i] = new LinearLayout(dialogView.getContext());
            linearLayoutHorizontal[i].setLayoutParams(linearLayoutHorizontalParams);
            noteTextView[i] = new TextView(dialogView.getContext());
            noteTextView[i].setLayoutParams(textViewParams);
            noteTextView[i].setTextColor(getActivity().getResources().getColor(android.R.color.black));
            noteImageSpinner[i] = new Spinner(dialogView.getContext());
            noteImageSpinner[i].setAdapter(imagesSpinnerAdapter);
            noteImageSpinner[i].setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        Log.e(TAG, "IIIII: " + String.valueOf(i));
                        Log.e(TAG, "LLLLL: " + String.valueOf(l));
                        Log.e(TAG, "NOTE SELECTED: " + imageNameDatabase[adapterView.getSelectedItemPosition()]);
                        jsonImages.put("note" + String.valueOf(I), imageNameDatabase[adapterView.getSelectedItemPosition()]);
                        jsonImagesForNotesState[trackNumber] = jsonImages;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            deleteImageView[i] = new ImageView(getActivity().getApplicationContext());
            deleteImageView[i].setImageResource(R.drawable.cross);
            deleteImageView[i].setLayoutParams(deleteImageParams);
            final int finalI = i;
            deleteImageView[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayoutVertical.removeView(linearLayoutHorizontal[finalI]);
                    jsonNotesState[trackNumber].remove("note" + String.valueOf(finalI));
                    jsonImagesForNotesState[trackNumber].remove("note" + String.valueOf(finalI));
                    jsonNotes.remove("note" + String.valueOf(finalI));
                    jsonImages.remove("note" + String.valueOf(finalI));
                    JSONObject bufferNotes = new JSONObject();
                    JSONObject bufferImagesOfNotes = new JSONObject();
                    int bufferCounter = 0;
                    for (int i = 0; i <= jsonNotesState[trackNumber].length(); i++) {
                        try {
                            bufferNotes.put("note" + String.valueOf(bufferCounter), jsonNotesState[trackNumber].getString("note" + String.valueOf(i)));
                            bufferImagesOfNotes.put("note" + String.valueOf(bufferCounter), jsonImagesForNotesState[trackNumber].getString("note" + String.valueOf(i)));
                            bufferCounter++;
                        } catch (JSONException e) {
                            Log.e(TAG, "BUFFERS= " + bufferNotes.toString() + "  |||  " + bufferImagesOfNotes.toString());
                            e.printStackTrace();
                            //continue;
                        }
                        Log.e(TAG, "BUFFERS= " + bufferNotes.toString() + "  |||  " + bufferImagesOfNotes.toString());
                    }
                    jsonNotesState[trackNumber] = bufferNotes;
                    jsonImagesForNotesState[trackNumber] = bufferImagesOfNotes;
                    jsonNotes = bufferNotes;
                    jsonImages = bufferImagesOfNotes;
                }
            });
            linearLayoutHorizontal[i].addView(noteTextView[i]);
            linearLayoutHorizontal[i].addView(noteImageSpinner[i]);
            linearLayoutHorizontal[i].addView(deleteImageView[i]);
        }
    }

    /**
     * Метод записи установленных настроек
     * @param name - имя файла
     */
    private void recordJSON(String name) {
        File fileName = null;
        String sdState = android.os.Environment.getExternalStorageState();
        if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
            String path = dialogView.getContext().getApplicationInfo().dataDir;
            fileName = new File(path, name);
        } else {
            fileName = dialogView.getContext().getCacheDir();
        }

        if (!fileName.exists()) {
            Log.e(TAG, "file not exists");
            fileName.getParentFile().mkdirs();
        }
        try {
            FileWriter f = new FileWriter(fileName);
            if (name.equals("notes")) {
                f.write(jsonTracksWithNotes.toString());
            } else {
                f.write(jsonTracksWithNoteImages.toString());
            }
            Log.e(TAG, "file has written");
            f.flush();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Считывание настроек
     *
     * @param name - имя файла
     * @return возвращает настройки в виде JSON
     */
    private String readJSON(String name) {
        try {
            String path = dialogView.getContext().getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj.toString();
        } catch (JSONException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        }
    }


    /**
     * Метод обратного вызова при подтверждении внесенных изменений
     */
    @Override
    public void onYesClick() {
        dialogOpened = false;
    }

    /**
     * Метод обратного вызова при отклонении внесенных изменений
     */
    @Override
    public void onNoClick() {
        dialogOpened = false;
        dismiss();
    }

    @Override
    public void onYesClick(int first, int second) {

    }

    @Override
    public void onYesClick(String[] booleanArray, int second) {

    }
}
