package com.drummerscope.android.Dialogs.CreateNote;

/**
 * Интерфейс для возврата результата (обратный вызов).
 *
 * Created by evgen on 04.11.2016.
 */
public interface DialogClickListener {

    void onYesClick();
    void onNoClick();
    void onYesClick(int first, int second);
    void onYesClick(String[] booleanArray, int second);
}
