package com.drummerscope.android.Dialogs;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.drummerscope.android.Dialogs.CreateNote.DialogClickListener;
import com.drummerscope.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Класс, отвечающий за работу диалогового окна, предназначенного для настройки размера и доли.
 */
public class TimeSignatureDialog extends DialogFragment {

    /**
     * Количество кликов в такте
     */
    private NumberPicker beats;
    /**
     * Доля клика
     */
    private NumberPicker beatLength;
    /**
     * Корневой линейный макет
     */
    private LinearLayout linearLayout;
    /**
     * Слушатель для передачи результата обратно
     */
    private DialogClickListener callback;

    /**
     * Метод создания диалогового окна
     *
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает экземпляр диалогового окна
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.set_time_signature, null);

        final int beatsValue = getArguments().getInt("beats");
        int lengthValue = getArguments().getInt("length");

        beats = (NumberPicker)view.findViewById(R.id.beatsNumberPicker);
        beats.setMinValue(1);
        beats.setMaxValue(32);
        beats.setValue(beatsValue);

        int numberOfValues = 5;
        int pickerChange = 2;
        beatLength = (NumberPicker)view.findViewById(R.id.beatsLengthNumberPicker);
        final String[] displayedValues = new String[numberOfValues];
        displayedValues[0] = String.valueOf(2);
        for (int i = 1; i < numberOfValues; i++)
            displayedValues[i] = String.valueOf(Integer.parseInt(displayedValues[i-1]) * pickerChange);

        beatLength.setMinValue(0);
        beatLength.setMaxValue(displayedValues.length-1);
        beatLength.setDisplayedValues( displayedValues );
        for (int i = 1; i < numberOfValues; i++)
           if (displayedValues[i].equals(String.valueOf(lengthValue)))
                beatLength.setValue(i);

        beats.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        beatLength.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        linearLayout = (LinearLayout) view.findViewById(R.id.linear_layout_set_ts_dialog);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.requestFocus();
            }
        });


        JSONObject jsonObjectSettings = readJSON("settings.txt");
        if (jsonObjectSettings == null)
            jsonObjectSettings = new JSONObject();

        try {
            callback = (DialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

        MaterialDialog materialDialog = new MaterialDialog.Builder(view.getContext())
                .customView(view, false)
                .backgroundColor(getResources().getColor(R.color.transparent))
                .backgroundColorRes(R.color.transparent)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        callback.onYesClick(beats.getValue(), Integer.parseInt(displayedValues[beatLength.getValue()]));
                    }
                })
                .negativeText("Cancel")
                .positiveText("Set")
                .negativeColor(getResources().getColor(R.color.myRed))
                .positiveColor(getResources().getColor(R.color.myRed))
                .cancelable(true)
                .build();

        materialDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });

        int width = getResources().getDimensionPixelSize(R.dimen.width);

        materialDialog.getWindow().setLayout(width, FrameLayout.LayoutParams.WRAP_CONTENT);
        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        materialDialog.getWindow().setBackgroundDrawableResource(R.drawable.create_note_dialog);

        return materialDialog;
    }

    /**
     * Метод создает экземпляр диалогового окна
     *
     * @param beatsInBar - количество кликов в такте
     * @param beatLength - длина клика (1/2, 1/4, 1/8, ...)
     * @return возвращает экземпляр диалогового окна
     */
    public static TimeSignatureDialog newInstance(int beatsInBar, int beatLength) {
        TimeSignatureDialog f = new TimeSignatureDialog();
        Bundle args = new Bundle();
        args.putInt("beats", beatsInBar);
        args.putInt("length", beatLength);
        f.setArguments(args);
        return f;
    }

    public JSONObject readJSON(String name) {
        try {
            String path = getActivity().getApplicationContext().getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj;
        } catch (JSONException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
