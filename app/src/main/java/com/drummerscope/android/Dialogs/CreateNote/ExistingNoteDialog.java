package com.drummerscope.android.Dialogs.CreateNote;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.drummerscope.android.R;

/**
 * Класс, диалогового окна, выводящегося на экран в ситуации, когда пользователь
 * пытается добавить уже существующие ноты.
 * <p>
 * Created by evgenyrasskazov on 21.02.2017.
 */

public class ExistingNoteDialog extends DialogFragment {
    private static final String TAG = "EXISTING NOTE DIALOG";
    /**
     * Массив с именами нот для соответствующей i-ой дорожки
     */
    private String[] trackNumbersWithNote;
    /**
     * Номер дорожки, на которую добавляются ноты
     */
    private int newTrack;
    /**
     * Интерфейс для обратного вызова по результату действий в диалоговом окне
     */
    private DialogClickListener callback;

    /**
     * Создает экземпляр диалогового окна.
     *
     * @param trackWithOldNote - массив с имеющимися конфликтными нотами на соответсвующей i-ой дорожке.
     * @param newTrack - номер дорожки, на которую добавляеются ноты.
     * @return возвращает экземпляр диалогового окна.
     */
    public static ExistingNoteDialog newInstance(String[] trackWithOldNote, int newTrack) {
        ExistingNoteDialog f = new ExistingNoteDialog();
        for (int i = 0; i < trackWithOldNote.length; i++) {
            if (i == trackWithOldNote.length - 1) {
                Log.e(TAG, trackWithOldNote[i]);
            } else {
                Log.e(TAG, trackWithOldNote[i] + ", ");
            }
        }
        Bundle args = new Bundle();
        args.putStringArray("old", trackWithOldNote);
        args.putInt("new", newTrack);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        trackNumbersWithNote = getArguments().getStringArray("old");
        newTrack = getArguments().getInt("new");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.existing_note_dialog, null);

        try {
            callback = (DialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

        TextView titleOfDialog = (TextView) view.findViewById(R.id.title_existing_note_dialog);
        TextView textOfDialog = (TextView) view.findViewById(R.id.text_existing_note_dialog);

        String textForTitleOfDialog = "Attention";
        titleOfDialog.setText(textForTitleOfDialog);

        String trackNumberWithNote = "";
        for (int i = 0; i < trackNumbersWithNote.length; i++) {
            if (i + 1 == trackNumbersWithNote.length) {
                trackNumberWithNote += trackNumbersWithNote[i];
            } else {
                trackNumberWithNote += trackNumbersWithNote[i] + ", ";
            }
        }
        Log.e(TAG, "ALREADY EXISTS AT = " + trackNumberWithNote);

        String textForTextViewOfDialog = "These notes already exist at the track numbers " + String.valueOf(trackNumberWithNote) + ". \nDo you really want to delete the notes from the old tracks and add it to the current one?";
        textOfDialog.setText(textForTextViewOfDialog);

        MaterialDialog materialDialog = new MaterialDialog.Builder(view.getContext())
                .customView(view, false)
                .backgroundColor(getResources().getColor(R.color.transparent))
                .backgroundColorRes(R.color.transparent)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Log.e(TAG, "YES CLICK");
                        callback.onYesClick(trackNumbersWithNote, newTrack);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        callback.onNoClick();
                    }
                })
                .negativeText("No")
                .positiveText("Yes")
                .negativeColor(getResources().getColor(R.color.myRed))
                .positiveColor(getResources().getColor(R.color.myRed))
                .cancelable(true)
                .build();

        /* Событие нажатия на back */
        materialDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });

        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        materialDialog.getWindow().setBackgroundDrawableResource(R.drawable.create_note_dialog);

        return materialDialog;
    }
}