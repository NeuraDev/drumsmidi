package com.drummerscope.android.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.drummerscope.android.R;

/**
 * Класс, отвечающий за вывод информации о приложении в виде диалогового окна.
 *
 * Created by kmoaz on 04.04.2017.
 */

public class QuestionDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.question_dialog, null);

        TextView helpTextView = (TextView) view.findViewById(R.id.help_text_view);
        helpTextView.setMovementMethod(LinkMovementMethod.getInstance());

        MaterialDialog materialDialog = new MaterialDialog.Builder(view.getContext())
                .customView(view, false)
                .backgroundColor(getResources().getColor(R.color.transparent))
                .backgroundColorRes(R.color.transparent)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .positiveText("Ok")
                .positiveColor(getResources().getColor(R.color.myRed))
                .cancelable(true)
                .build();

        int width = ViewGroup.LayoutParams.MATCH_PARENT;

        materialDialog.getWindow().setLayout(width, FrameLayout.LayoutParams.WRAP_CONTENT);
        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        materialDialog.getWindow().setBackgroundDrawableResource(R.drawable.create_note_dialog);

        return materialDialog;
    }
}
