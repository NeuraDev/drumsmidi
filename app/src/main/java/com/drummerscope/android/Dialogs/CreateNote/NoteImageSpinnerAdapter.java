package com.drummerscope.android.Dialogs.CreateNote;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.drummerscope.android.R;

import java.util.ArrayList;

/**
 * Адаптер с возможными изображениями нот для выпадающего списка.
 *
 * Created by evgen on 04.11.2016.
 */

public class NoteImageSpinnerAdapter extends ArrayAdapter<ItemData> {
    /**
     * Список с картинками соответсвующих нот
     */
    private ArrayList<ItemData> listOfItemData;
    /**
     * Поле - компонент для заполнения интерейса
     */
    private LayoutInflater layoutInflater;

    /**
     * Конструктор - создание нового адаптера с определенными значениями.
     *
     * @param activity - родительская активити
     * @param id - идентификатор ресурса
     * @param listOfItemData - список с картинками соответствующих нот
     */
    NoteImageSpinnerAdapter(Activity activity, int id, ArrayList<ItemData> listOfItemData) {
        super(activity, id, listOfItemData);
        this.listOfItemData = listOfItemData;
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Метод получения View по позиции адаптера
     *
     * @param position - позиция адаптера
     * @param convertView - смотри метод выше по иерархии
     * @param parent - родительский GroupView
     * @return возвращает View по соответствующей позиции адаптера
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    /**
     * Метод получения View выпадающего списка по позиции
     *
     * @param position - позиция адаптера
     * @param convertView - смотри метод выше по иерархии
     * @param parent - родительский GroupView
     * @return возвращает View по соответствующей позиции адаптера
     */
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    /**
     * Метод получения кастомной View по позиции адаптера
     *
     * @param position - позиция адаптера
     * @param parent - родительский GroupView
     * @return возвращает View по соответствующей позиции адаптера
     */
    private View getCustomView(int position, ViewGroup parent) {
        View row = layoutInflater.inflate(R.layout.row_image_spinner, parent, false);
        ImageView imageView = (ImageView) row.findViewById(R.id.image_for_create_note_spinner);
        imageView.setImageResource(listOfItemData.get(position).getImageId());
        return row;
    }
}
