package com.drummerscope.android.Dialogs.CreateNote;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.drummerscope.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by evgen on 02.09.2016.
 * <p>
 * Класс, отвечающий за добавление нот и соответствующих им изображений.
 */
public class CreateNoteDialog extends DialogFragment implements AdapterView.OnItemSelectedListener, DialogClickListener {

    /**
     * Тэг, используемых для распечатывания логов с помощью Log.e()
     */
    private static final String TAG = "CREATE_NOTE_DIALOG";
    /**
     * Максимально возможное количество нот на дорожке (используется для задания объема массива)
     */
    private static final int N = 15;
    /**
     * Названия файлов, создаваемых для хранения соответствующих настроек
     */
    private static final String NOTES = "notes";
    private static final String IMAGES = "images";
    private static final String TRACK = "track";
    private static final String NOTE = "note";
    /**
     * Поле - открыто ли диалоговое окно с предупреждением о попытке добавить уже существующую ноту
     */
    public static boolean existingNoteDialogOpened = false;
    /**
     * Поле - добавляется ли нота в данный момент
     */
    public static boolean isNoteCreating = false;
    /**
     * Список, необходимый для определения количества подряд записанных нот при добавлении
     */
    private static List<String> noteClickCount = new ArrayList<>();
    /**
     * Текстовое поле с информацией о зафиксированной ноте
     */
    private static TextView noteInfo;
    /**
     * Текущая дорожка
     */
    private static int currentTrack;
    /**
     * Вертикальный макет для добавления нот
     */
    private static LinearLayout linearLayoutVertical;
    /**
     * Массив макетов для описания нот
     */
    private static LinearLayout[] linearLayoutHorizontal = new LinearLayout[N];
    /**
     * Массив текстовых полей для названия нот
     */
    private static TextView[] noteTextView = new TextView[N];
    /**
     * Массив выпадающих списков для выбора изображения ноты
     */
    private static Spinner[] noteImageSpinner;
    /**
     * Массив изображений для удаления нот
     */
    private static ImageView[] deleteImageView = new ImageView[N];
    /**
     * Массив с названиями картинок нот
     */
    private static String[] imageNameDatabase = {
            "triangle_for_settings", "rhombus", "line", "cross_in_circle", "cross"};

    /**
     * Поле вью - диалоговое окно
     */
    private static View dialogView;
    /**
     * Объекты JSON для хранения добавляемых нот
     */
    private static JSONObject _toAddNotes;
    private static JSONObject _toAddImages;
    private static JSONObject _fromFileNotesFull;
    private static JSONObject _fromFileImagesFull;
    private static JSONObject _toSaveNotes;
    private static JSONObject _toSaveImages;
    /**
     * Список с номерами дорожек, где уже есть добавляемые ноты
     */
    private static List<String> _tracksNumbersWithExistingNotes = new ArrayList<>();
    /**
     * Поле - есть ли такая нота на текущей дорожке
     */
    private static boolean isExistsAtCurrentTrack = false;
    /**
     * Поле - количество нот на текущей дорожке
     */
    private static int notesCount = 0;
    /**
     * Поле - текущая дорожка (для сохранения состояния)
     */
    int savedCurrentTrack;
    /**
     * Адаптер для выпадающего списка картинок нот
     */
    private NoteImageSpinnerAdapter imagesSpinnerAdapter;
    /**
     * Поле - текущий фрагмент
     */
    private Fragment thisFragment;
    /**
     * Реализация интерфейса для обратного вызова при сохранении настроек
     */
    private DialogClickListener callback;
    /**
     * Параметры для соответствующей верстки
     */
    private LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);
    private LinearLayout.LayoutParams deleteImageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);
    private LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);
    private LinearLayout.LayoutParams linearLayoutHorizontalParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);

    /**
     * Метод создания экземпляра диалогового окна
     *
     * @param num - номер дорожки
     * @return возвращает экземпляр диалогового окна
     */
    public static CreateNoteDialog newInstance(int num) {
        CreateNoteDialog f = new CreateNoteDialog();
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }

    /**
     * Метод инициализации JSON-объектов для промежуточного хранения добавляемых настроек
     */
    private static void init() {
        Log.e(TAG, "NEW JSONS");
        _toAddNotes = new JSONObject();
        _toAddImages = new JSONObject();
        _fromFileNotesFull = new JSONObject();
        _fromFileImagesFull = new JSONObject();
        notesCount = 0;

        _toSaveNotes = new JSONObject();
        _toSaveImages = new JSONObject();
        noteClickCount = new ArrayList<>();

        if (_fromFileNotesFull.length() < 6) {
            for (int i = 0; i < 6; i++) {
                try {
                    _fromFileNotesFull.put(TRACK + String.valueOf(i), new JSONObject());
                    _fromFileImagesFull.put(TRACK + String.valueOf(i), new JSONObject());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            _fromFileNotesFull = new JSONObject(readJSON(NOTES));
            _fromFileImagesFull = new JSONObject(readJSON(IMAGES));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "FROM FILE AFTER NEW FILL: " + _fromFileNotesFull.toString());


    }

    /**
     * Метод установки названия играемой ноты.
     * Автоматически добавляет ноту в список, если такой ноты еще нет на данной дорожке.
     *
     * @param note - имя (название) ноты
     * @throws JSONException - пробрасывает исключение при неправильной работе с JSON-объектом
     */
    public static void setNote(String note) throws JSONException {
        Log.e(TAG, "NOTE: " + note);
        isExistsAtCurrentTrack = false;
        noteInfo.setText(note);

        for (int i = 0; i < N; i++) {
            try {
                String noteFromAddList = noteTextView[i].getText().toString();
                Log.e(TAG, "NOTE ADDED ALREADY: " + noteFromAddList);
                if (noteFromAddList.equals(note)) {
                    isExistsAtCurrentTrack = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Log.e(TAG, "CLICKED: " + noteClickCount.toString());
        Log.e(TAG, "IS EXIST AT THE TRACK: " + String.valueOf(isExistsAtCurrentTrack));
        if (!isExistsAtCurrentTrack) {
            noteClickCount.add(note);
            /* Количество подряд идущих кликов на ноту, необходимое для ее добавления */
            int needClicksToAdd = 3;
            if (noteClickCount.size() > needClicksToAdd) {
                if (noteClickCount.get(noteClickCount.size() - 1).equals(noteClickCount.get(noteClickCount.size() - 2)) &&
                        noteClickCount.get(noteClickCount.size() - 3).equals(noteClickCount.get(noteClickCount.size() - 4)) &&
                        noteClickCount.get(noteClickCount.size() - 1).equals(noteClickCount.get(noteClickCount.size() - 3))) {

                    try {
                        linearLayoutVertical.addView(linearLayoutHorizontal[notesCount]);
                    } catch (IllegalStateException e) {
                        linearLayoutVertical.removeAllViews();
                        linearLayoutVertical.addView(linearLayoutHorizontal[notesCount]);
                    }
                    linearLayoutVertical.requestLayout();
                    linearLayoutVertical.invalidate();

                    noteTextView[notesCount].setText(note);
                    notesCount++;

                }
            }

        }
    }

    /**
     * Метод считывания настроек
     *
     * @param name - название файла
     * @return возвращает строку в виде JSON соответствующего файла настроек
     */
    private static String readJSON(String name) {
        try {
            String path = dialogView.getContext().getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj.toString();
        } catch (JSONException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        }
    }

    /**
     * Метод создания диалогового окна и заполнения графического интерфейса.
     *
     * @param savedInstanceState - сохраненное состояние при смене конфигурации приложения
     * @return возвращает заполненное диалоговое окно
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        savedCurrentTrack = getArguments().getInt("num");
        currentTrack = savedCurrentTrack;
        LayoutInflater inflater = getActivity().getLayoutInflater();
        dialogView = inflater.inflate(R.layout.create_note_dialog, null);
        thisFragment = this;
        setTextViewParams(textViewParams);
        setSpinnerParams(spinnerParams);
        setDeleteImageParams(deleteImageParams);
        setLinearLayoutParams(linearLayoutHorizontalParams);

        init();

        MaterialDialog materialDialog = new MaterialDialog.Builder(dialogView.getContext())
                .customView(dialogView, false)
                .backgroundColor(getResources().getColor(R.color.transparent))
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        isNoteCreating = false;
                        reset();
                    }


                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        collectInfoFromViews();
                        if (!existingNoteDialogOpened && checkWhetherNoteExists()) {
                            existingNoteDialogOpened = true;
                            DialogFragment existingNoteDialog = ExistingNoteDialog.newInstance(_tracksNumbersWithExistingNotes.toArray(new String[_tracksNumbersWithExistingNotes.size()]), currentTrack);
                            existingNoteDialog.setTargetFragment(thisFragment, 0);
                            existingNoteDialog.show(((Activity) dialogView.getContext()).getFragmentManager(), existingNoteDialog.getClass().getName());
                        } else {
                            Log.e(TAG, "OK, SAVE NOTES: " + _toSaveNotes.toString());
                            recordJSON(NOTES, _toSaveNotes);
                            recordJSON(IMAGES, _toSaveImages);
                            callback.onYesClick();
                            reset();
                        }
                    }
                })
                .negativeText("Cancel")
                .positiveText("Set")
                .negativeColor(getResources().getColor(R.color.myRed))
                .positiveColor(getResources().getColor(R.color.myRed))
                .cancelable(true)
                .build();

        isNoteCreating = true;
        noteImageSpinner = new Spinner[N];

        ArrayList<ItemData> list = new ArrayList<>();

        list.add(new ItemData(R.drawable.ic_triangle_notes));
        list.add(new ItemData(R.drawable.ic_rhombus_notes));
        list.add(new ItemData(R.drawable.ic_line_notes));
        list.add(new ItemData(R.drawable.ic_cross_in_circle_notes));
        list.add(new ItemData(R.drawable.ic_cross_notes));

        imagesSpinnerAdapter = new NoteImageSpinnerAdapter(getActivity(), R.id.image_for_create_note_spinner, list);

        noteInfo = (TextView) dialogView.findViewById(R.id.createNote_noteInfo);
        linearLayoutVertical = (LinearLayout) dialogView.findViewById(R.id.linearLayoutVertical_create_note);
        createEmptyLayouts();

        try {
            callback = (DialogClickListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        materialDialog.getWindow().setBackgroundDrawableResource(R.drawable.create_note_dialog);

        return materialDialog;
    }

    /**
     * Метод для сбора информации о нотах со всех соответствующих элементов графического интерфейса
     * и сохранения их в JSON-объекты.
     */
    private void collectInfoFromViews() {
        Log.e(TAG, "COLLECTED INFO BEFORE: " + _toAddNotes.toString());
        for (int i = 0; i < N; i++) {
            try {
                String note = noteTextView[i].getText().toString();
                String image = imageNameDatabase[noteImageSpinner[i].getSelectedItemPosition()];
                if (!note.isEmpty() && !image.isEmpty()) {
                    _toAddNotes.put(NOTE + String.valueOf(_toAddNotes.length()), note);
                    _toAddImages.put(NOTE + String.valueOf(_toAddImages.length()), image);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.e(TAG, "COLLECTED INFO: " + _toAddNotes.toString());
        Log.e(TAG, "COLLECTED INFO: " + _toAddImages.toString());
    }

    /**
     * Метод сброса (обнуления) необходимых объектов и параметров
     */
    private void reset() {
        _toAddNotes = new JSONObject();
        _toAddImages = new JSONObject();
        _fromFileNotesFull = new JSONObject();
        _fromFileImagesFull = new JSONObject();
        isExistsAtCurrentTrack = false;
        _toSaveNotes = new JSONObject();
        _toSaveImages = new JSONObject();
        noteClickCount = new ArrayList<>();
        notesCount = 0;
        _tracksNumbersWithExistingNotes = new ArrayList<>();
    }

    /**
     * Метод, вызываемый при уничтожении диалогового окна.
     */
    @Override
    public void onDestroy() {
        isNoteCreating = false;
        existingNoteDialogOpened = false;
        super.onDestroy();
    }

    /**
     * Метод проверки. Были ли добавляемые ноты уже добавлены ранее (в т.ч. и на другие дорожки).
     *
     * @return возвращает логическое значение:
     * true - если такие ноты уже существуют;
     * false - если добавляемые ноты еще не были добавлены.
     */
    private boolean checkWhetherNoteExists() {
        LinkedList<String> _toSaveNotesList;
        LinkedList<String> _toSaveImagesList;
        LinkedList<String> _toCompareNotesList;
        LinkedList<String> _toCompareImagesList;
        _toSaveNotes = new JSONObject();
        _toSaveImages = new JSONObject();
        isExistsAtCurrentTrack = false;
        JSONObject _forCheckNotesAtOtherTracks = null;
        JSONObject _forCheckImagesAtOtherTracks = null;
        Log.e(TAG, "FROM FILE FULL NOTES: " + _fromFileNotesFull.toString());

        for (int j = 0; j < 6; j++) {
            _toSaveNotesList = new LinkedList<>();
            _toSaveImagesList = new LinkedList<>();
            _toCompareNotesList = new LinkedList<>();
            _toCompareImagesList = new LinkedList<>();
            JSONObject _toSaveNotesOneTrack = new JSONObject();
            JSONObject _toSaveImagesOneTrack = new JSONObject();
            try {
                _forCheckNotesAtOtherTracks = _fromFileNotesFull.getJSONObject(TRACK + String.valueOf(j));
                _forCheckImagesAtOtherTracks = _fromFileImagesFull.getJSONObject(TRACK + String.valueOf(j));
                for (int i = 0; i < _forCheckNotesAtOtherTracks.length(); i++) {
                    _toCompareNotesList.add(_forCheckNotesAtOtherTracks.getString(NOTE + String.valueOf(i)));
                    _toCompareImagesList.add(_forCheckImagesAtOtherTracks.getString(NOTE + String.valueOf(i)));
                }
                for (int i = 0; i < _toAddNotes.length(); i++) {
                    _toSaveNotesList.add(_toAddNotes.getString(NOTE + String.valueOf(i)));
                    _toSaveImagesList.add(_toAddImages.getString(NOTE + String.valueOf(i)));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (_forCheckNotesAtOtherTracks != null)
                Log.e(TAG, "TRACK " + j + ": " + _forCheckNotesAtOtherTracks.toString());

            if (j == currentTrack) {
                _toCompareNotesList = _toSaveNotesList;
                _toCompareImagesList = _toSaveImagesList;
            } else {
                Iterator iterator = _toCompareNotesList.iterator();
                while (iterator.hasNext()) {
                    String note = (String) iterator.next();
                    if (_toSaveNotesList.contains(note) && !_tracksNumbersWithExistingNotes.contains(String.valueOf(j))) {
                        _tracksNumbersWithExistingNotes.add(String.valueOf(j));
                        isExistsAtCurrentTrack = true;
                    }
                }

                iterator = ((LinkedList) _toCompareNotesList.clone()).iterator();
                while (iterator.hasNext()) {
                    String note = (String) iterator.next();
                    if (_toSaveNotesList.contains(note)) {
                        int index = _toCompareNotesList.indexOf(note);
                        _toCompareNotesList.remove(index);
                        _toCompareImagesList.remove(index);
                    }
                }
            }

            Log.e(TAG, "TRANSFORM DATA");
            Log.e(TAG, "LIST NOTE: " + _toCompareNotesList.toString());
            for (String note : _toCompareNotesList) {
                try {
                    _toSaveNotesOneTrack.put(NOTE + String.valueOf(_toSaveNotesOneTrack.length()), note);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for (String note : _toCompareImagesList) {
                try {
                    _toSaveImagesOneTrack.put(NOTE + String.valueOf(_toSaveImagesOneTrack.length()), note);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {
                _toSaveNotes.put(TRACK + String.valueOf(j), _toSaveNotesOneTrack);
                _toSaveImages.put(TRACK + String.valueOf(j), _toSaveImagesOneTrack);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("NOTES TO SAVE BY: ", _toSaveNotes.toString());
        }

        try {
            _forCheckNotesAtOtherTracks = _fromFileNotesFull.getJSONObject(TRACK + String.valueOf(currentTrack));
            _forCheckImagesAtOtherTracks = _fromFileImagesFull.getJSONObject(TRACK + String.valueOf(currentTrack));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (_forCheckNotesAtOtherTracks != null)
            Log.e(TAG, "LAST STEP NOTES TO SAVE: " + _forCheckNotesAtOtherTracks.toString());
        Log.e(TAG, "_toAddNotes: " + _toAddNotes.toString());
        for (int i = 0; i < _toAddNotes.length(); i++) {

            assert _forCheckImagesAtOtherTracks != null;
            try {
                _forCheckNotesAtOtherTracks.put(NOTE + _forCheckNotesAtOtherTracks.length(), _toAddNotes.get(NOTE + String.valueOf(i)));
                _forCheckImagesAtOtherTracks.put(NOTE + _forCheckImagesAtOtherTracks.length(), _toAddImages.get(NOTE + String.valueOf(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        try {
            _toSaveNotes.put(TRACK + String.valueOf(currentTrack), _forCheckNotesAtOtherTracks);
            _toSaveImages.put(TRACK + String.valueOf(currentTrack), _forCheckImagesAtOtherTracks);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("NOTES TO SAVE: ", _toSaveNotes.toString());
        return isExistsAtCurrentTrack;
    }

    /**
     * Метод для заполнения массивов соответсвующими пустыми элементами графического интерфейса.
     */
    private void createEmptyLayouts() {
        for (int i = 0; i < N; i++) {
            final int I = i;
            Log.e(TAG, "INDEXES = " + String.valueOf(I));
            linearLayoutHorizontal[i] = new LinearLayout(dialogView.getContext());
            linearLayoutHorizontal[i].setLayoutParams(linearLayoutHorizontalParams);
            noteTextView[i] = new TextView(dialogView.getContext());
            noteTextView[i].setLayoutParams(textViewParams);
            noteTextView[i].setTextColor(getActivity().getResources().getColor(android.R.color.black));
            noteImageSpinner[i] = new Spinner(dialogView.getContext());
            noteImageSpinner[i].setAdapter(imagesSpinnerAdapter);
            deleteImageView[i] = new ImageView(getActivity().getApplicationContext());
            deleteImageView[i].setImageResource(R.drawable.cross);
            deleteImageView[i].setLayoutParams(deleteImageParams);
            final int finalI = i;
            deleteImageView[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayoutVertical.removeView(linearLayoutHorizontal[finalI]);
                    noteTextView[finalI].setText("");
                }
            });
            linearLayoutHorizontal[i].addView(noteTextView[i]);
            linearLayoutHorizontal[i].addView(noteImageSpinner[i]);
            Log.e(TAG, "INDEXES = " + String.valueOf(i));
            linearLayoutHorizontal[i].addView(deleteImageView[i]);
        }
    }

    /**
     * Метод установки параметров для тескта.
     *
     * @param params - параметры верстки текстового элемента.
     */
    public void setTextViewParams(LinearLayout.LayoutParams params) {
        int margin = dpToPixels(16);
        params.setMargins(margin, 0, 0, 0);
        params.gravity = Gravity.CENTER;
        params.weight = 1;
    }

    /**
     * Метод установки параметров для выпадающего списка (спиннера).
     *
     * @param params - параметры верстки для спиннера
     */
    public void setSpinnerParams(LinearLayout.LayoutParams params) {
        int margin = dpToPixels(16);
        params.setMargins(margin, 0, 0, 0);
        params.gravity = Gravity.CENTER;

    }

    /**
     * Метод установки параметров верстки для картинки удаления.
     *
     * @param params - параметры верстки для картинки удаления.
     */
    private void setDeleteImageParams(LinearLayout.LayoutParams params) {
        int margin = dpToPixels(16);
        params.setMargins(margin, 0, 0, 0);
        params.weight = 1;
        params.gravity = Gravity.RIGHT;
        params.gravity = Gravity.CENTER_VERTICAL;
    }

    /**
     * Метод установки параметров верстки для линейного макета.
     *
     * @param params - параметры верстки для линейного макета.
     */
    private void setLinearLayoutParams(LinearLayout.LayoutParams params) {
        int margin = dpToPixels(16);
        params.setMargins(0, 0, 0, margin);
    }

    /**
     * Метод конвертации dp в пиксели.
     *
     * @param dpValue - независимые от плотности пиксели.
     * @return возвращает значение в пикселях.
     */
    private int dpToPixels(int dpValue) {
        float d = dialogView.getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * d);
    }

    /**
     * Метод для записи JSON.
     *
     * @param fileNameString - имя записываемого файла.
     * @param fileToSave     - JSON-объект для записи.
     */
    private void recordJSON(String fileNameString, JSONObject fileToSave) {
        File fileName = null;
        String sdState = android.os.Environment.getExternalStorageState();
        if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
            String path = dialogView.getContext().getApplicationInfo().dataDir;
            fileName = new File(path, fileNameString);
        } else {
            fileName = dialogView.getContext().getCacheDir();
        }
        if (!fileName.exists()) {
            fileName.getParentFile().mkdirs();
        }
        try {
            FileWriter f = new FileWriter(fileName);
            f.write(fileToSave.toString());
            f.flush();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onYesClick() {

    }

    @Override
    public void onNoClick() {
        existingNoteDialogOpened = false;
    }

    @Override
    public void onYesClick(int oldTrack, int newTrack) {

    }

    /**
     * Метод обратного вызова, вызываемый при подтверждении сохранения установленных настроек.
     *
     * @param booleanArray - массив с логическими значениями для каждой дорожки:
     *                     true - на i-ой дорожке уже есть такая нота;
     *                     false - на i-ой дорожке нет такой ноты.
     * @param newTrack     - дорожка, на которую добавляются ноты.
     */
    @Override
    public void onYesClick(String[] booleanArray, int newTrack) {
        Log.e(TAG, "TO SAVE NOTES!: " + _toSaveNotes.toString());
        Log.e(TAG, "TO SAVE IMAGES!: " + _toSaveImages.toString());
        recordJSON(NOTES, _toSaveNotes);
        recordJSON(IMAGES, _toSaveImages);
        callback.onYesClick();
        reset();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}