package com.drummerscope.android.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.drummerscope.android.Fragments.NotesSettingsFragment;
import com.drummerscope.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Класс, отвечающий за выбор картинки для ноты в соответсвующем диалоговом окне.
 *
 * Created by evgenyrasskazov on 04.09.2016.
 */
public class NoteImageDialog extends DialogFragment {

    /**
     * Поле - номер дорожки
     */
    int trackNumber;
    /**
     * Поле - позиция ноты в дорожке
     */
    int notePosition;
    /**
     * Группы RaddioButton для выбора картинки
     */
    private RadioGroup firstRadioGroup;
    private RadioGroup secondRadioGroup;
    /**
     * Поле - блокировка одновременного выбора в двух группах RadioButton
     */
    private boolean isChecking = true;
    /**
     * Drawable картинка соответствующего RadioButton
     */
    private static Drawable imageFromRadioButton;
    /**
     * Поле - идентификатор
     */
    private int checkedRadioButtonId;
    /**
     * JSON-объекты для хранения названий картинок соответствующих нот
     */
    private JSONObject jsonImages;
    private JSONObject jsonTracksWithNoteImages;

    /**
     * Элемент View диалогового окна
     */
    private View dialogView;

    /**
     * Метод создает и наполняет контентом диалоговое окно
     *
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает экземпляр диалогового окна
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trackNumber = getArguments().getInt("num");
        notePosition = getArguments().getInt("caller");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        dialogView = inflater.inflate(R.layout.note_image_dialog, null);
        firstRadioGroup = (RadioGroup) dialogView.findViewById(R.id.firstRadioGroup);
        firstRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i != -1 && isChecking) {
                    isChecking = false;
                    secondRadioGroup.clearCheck();
                    checkedRadioButtonId = i;
                    RadioButton myRadioButton = (RadioButton) dialogView.findViewById(i);
                    imageFromRadioButton = myRadioButton.getCompoundDrawables()[0];
                }
                isChecking = true;
            }
        });

        secondRadioGroup = (RadioGroup) dialogView.findViewById(R.id.secondRadioGroup);
        secondRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i != -1 && isChecking) {
                    isChecking = false;
                    firstRadioGroup.clearCheck();
                    checkedRadioButtonId = i;
                    RadioButton myRadioButton = (RadioButton) dialogView.findViewById(i);
                    imageFromRadioButton = myRadioButton.getCompoundDrawables()[0];
                }
                isChecking = true;
            }
        });


        try {
            jsonImages = new JSONObject(readJSON("images")).getJSONObject("track" + String.valueOf(trackNumber));
            jsonTracksWithNoteImages = new JSONObject(readJSON("images"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        builder.setView(dialogView)
                .setPositiveButton("Set", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String name;
                        switch (checkedRadioButtonId) {
                            case R.id.triangleRadioButton:
                                name = "triangle_for_settings";
                                break;
                            case R.id.rhombusRadioButton:
                                name = "rhombus";
                                break;
                            case R.id.lineRadioButton:
                                name = "line";
                                break;
                            case R.id.crossInCircleRadioButton:
                                name = "cross_in_circle";
                                break;
                            case R.id.crossRadioButton:
                                name = "cross";
                                break;
                            default:
                                name = "triangle_for_settings";
                                break;
                        }
                        try {
                            jsonImages.put("note" + String.valueOf(notePosition), name);
                            jsonTracksWithNoteImages.put("track" + String.valueOf(trackNumber), jsonImages);
                            recordJSON("images");
                            NotesSettingsFragment.setNotesRecyclerViewRecyclerAdapter(trackNumber);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }


    /**
     * Метод создает экземпляр диалогового окна
     *
     * @param num - номер дорожки
     * @param callerItem - порядок ноты в дорожке
     * @return возвращает экземпляр диалогового окна
     */
    public static NoteImageDialog newInstance(int num, int callerItem) {
        NoteImageDialog f = new NoteImageDialog();
        Bundle args = new Bundle();
        args.putInt("num", num);
        args.putInt("caller", callerItem);
        f.setArguments(args);
        return f;
    }

    private void recordJSON (String name) {
        File fileName = null;
        String sdState = android.os.Environment.getExternalStorageState();
        if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
            String path = dialogView.getContext().getApplicationInfo().dataDir;
            fileName = new File(path, name);
        } else {
            fileName = dialogView.getContext().getCacheDir();
        }

        if (!fileName.exists()) {
            fileName.getParentFile().mkdirs();
        }
        try {
            FileWriter f = new FileWriter(fileName);

            f.write(jsonTracksWithNoteImages.toString());
            f.flush();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readJSON (String name) {
        try {
            String path = dialogView.getContext().getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj.toString();
        } catch (JSONException ex) {
            //Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        } catch (IOException ex) {
            //Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        }
    }

}
