package com.drummerscope.android.Dialogs.CreateNote;

/**
 * Модель для элемента ноты с изображением
 *
 * Created by evgen on 04.11.2016.
 */

class ItemData {
    /**
     * resourceId для изображения ноты
     */
    private Integer imageId;

    /**
     * Дефолтный конструктор
     *
     * @param imageId - resourceId для изображения ноты
     */
    ItemData(Integer imageId) {
        this.imageId = imageId;
    }

    /**
     * @return возвращает resourceId для изображения ноты
     */
    Integer getImageId() {
        return imageId;
    }
}
