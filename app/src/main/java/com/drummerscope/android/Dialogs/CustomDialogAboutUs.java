package com.drummerscope.android.Dialogs;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.drummerscope.android.Fragments.FeedbackFragment;
import com.drummerscope.android.R;

/**
 * Класс, отвечающий за вывод диалогового окна в случае, когда пользователь выставляет низкую оценку
 * приложению.
 * <p>
 * Created by kmoaz on 18.12.2016.
 */

public class CustomDialogAboutUs extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_about_us, container, false);

        TextView ok = (TextView) view.findViewById(R.id.about_us_button_ok);
        TextView done = (TextView) view.findViewById(R.id.about_us_button_done);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                FeedbackFragment feedbackFragment = new FeedbackFragment();

                fragmentTransaction.replace(R.id.container, feedbackFragment);
                fragmentTransaction.addToBackStack("FeedbackFromCustomAboutUs");
                fragmentTransaction.commit();

                dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }
}
