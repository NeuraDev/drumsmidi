package com.drummerscope.android.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Класс для кастомизации элемента списка
 */

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    /**
     * Поле - Набор атрибутов списка
     */

    private static final int[] ATTRS = new int[]{
            android.R.attr.listDivider
    };

    /**
     * Поле - Drawable для кастомизации элементов списка
     */

    private Drawable mDivider;

    /**
     * Конструктор
     * @param context - Контекст
     */

    public DividerItemDecoration(final Context context) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
    }

    /**
     * Метод для кастомизации элементов списка
     * @param c
     * @param parent
     * @param state
     */

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    /**
     * Метод для переопределния высоты между элементами списка
     * @param outRect
     * @param view
     * @param parent
     * @param state
     */

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
    }
}
