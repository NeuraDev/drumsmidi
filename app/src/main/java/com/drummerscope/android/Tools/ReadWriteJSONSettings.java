package com.drummerscope.android.Tools;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Класс для чтения и записи данных в файл
 *
 * Created by kmoaz on 14.01.2017.
 */

public class ReadWriteJSONSettings {

    /**
     * Поле - Контекст
     */

    private Context context;

    /**
     * Конструтор
     * @param context - Контекст
     */

    public ReadWriteJSONSettings(Context context) {
        this.context = context;
    }

    /**
     * Метод для записи в файл
     * @param data - Данные для записи
     * @param filename - Имя файла
     */

    public void write (String data, String filename) {
        try {
            OutputStreamWriter outputStreamWriter = null;
            switch (filename) {
                case "bps":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("bps.json", Context.MODE_PRIVATE));
                    break;

                case "timer":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("timer.json", Context.MODE_PRIVATE));
                    break;

                case "dateformat":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("dateformat.json", Context.MODE_PRIVATE));
                    break;

                case "taktcount":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("taktcount.json", Context.MODE_PRIVATE));
                    break;

                case "tracktitle":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("tracktitle.json", Context.MODE_PRIVATE));
                    break;

                case "notecolor":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("notecolor.json", Context.MODE_PRIVATE));
                    break;

                case "devidetakt":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("devidetakt.json", Context.MODE_PRIVATE));
                    break;

                case "notelength":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("notelength.json", Context.MODE_PRIVATE));
                    break;

                case "dividetriplet":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("dividetriplet.json", Context.MODE_PRIVATE));
                    break;

                case "size":
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput("size.json", Context.MODE_PRIVATE));
                    break;

                default:
                    outputStreamWriter = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));
                    break;
            }
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /**
     * Метод для чтения данных из файла
     * @param filename - Имя фала
     * @return Данные из файла
     */

    public String read (String filename) {
        String ret = "";

        try {
            InputStream inputStream = null;
            switch (filename) {
                case "bps":
                    inputStream = context.openFileInput("bps.json");
                    break;

                case "timer":
                    inputStream = context.openFileInput("timer.json");
                    break;

                case "dateformat":
                    inputStream = context.openFileInput("dateformat.json");
                    break;

                case "taktcount":
                    inputStream = context.openFileInput("taktcount.json");
                    break;

                case "tracktitle":
                    inputStream = context.openFileInput("tracktitle.json");
                    break;

                case "notecolor":
                    inputStream = context.openFileInput("notecolor.json");
                    break;

                case "devidetakt":
                    inputStream = context.openFileInput("devidetakt.json");
                    break;

                case "notelength":
                    inputStream = context.openFileInput("notelength.json");
                    break;

                case "dividetriplet":
                    inputStream = context.openFileInput("dividetriplet.json");
                    break;

                case "size":
                    inputStream = context.openFileInput("size.json");
                    break;

                default:
                    inputStream = context.openFileInput(filename);
                    break;
            }

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}