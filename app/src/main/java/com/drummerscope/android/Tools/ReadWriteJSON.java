package com.drummerscope.android.Tools;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Класс для чтения и записи данных в JSON-файле
 *
 * Created by kmoaz on 09.12.2016.
 */

public class ReadWriteJSON {

    /**
     * Конструктор
     */

    public ReadWriteJSON() {

    }

    /**
     * Метод для запись данных в файл
     * @param data - Данные для записи
     * @param context - Контекст
     */

    private void writeToFile (String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("notes.json", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /**
     * Метод для чтения данных из файла
     * @param context - Контекст
     * @return Строка спарсенная из JSON-файла
     */

    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("notes.json");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    /**
     * Метод для очистки файла
     * @param context - Контекст
     */

    public void clearFile (Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("notes.json", Context.MODE_PRIVATE));
            outputStreamWriter.write("");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /**
     * Метод для записи JSON-объекта в файл
     * @param context - Контекст
     * @param BPM - БПМ
     * @param date - Дата
     * @param time - Время
     * @param exercise - Описание в журнале
     * @param dateFormat - Формат даты
     */

    public void writeJSONObject (Context context, String BPM, String date, String time, String exercise, String dateFormat) {
        JSONObject note = new JSONObject();
        JSONObject noteData = new JSONObject();
        JSONArray arrayNotes = new JSONArray();
        HashMap<Integer, List<String>> notes = readJSONObject(context);

        try {
            for (int i = 0; i < notes.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("BPM", notes.get(i).get(0));
                jsonObject1.put("date", notes.get(i).get(1));
                jsonObject1.put("time", notes.get(i).get(2));
                jsonObject1.put("exercise", notes.get(i).get(3));
                jsonObject1.put("dateFormat", notes.get(i).get(4));

                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put(notes.get(i).get(0) + notes.get(i).get(2) + notes.get(i).get(3), jsonObject1);
                arrayNotes.put(jsonObject2);
            }

            JSONArray tempArrayNotes1 = new JSONArray();
            for (int i = arrayNotes.length() - 1; i >= 0; i--) {
                tempArrayNotes1.put(arrayNotes.get(i));
            }
            arrayNotes = tempArrayNotes1;

            noteData.put("BPM", BPM);
            noteData.put("date", date);
            noteData.put("time", time);
            noteData.put("exercise", exercise);
            noteData.put("dateFormat", dateFormat);
            note.put(BPM + time + exercise, noteData);
            arrayNotes.put(note);

            JSONArray tempArrayNotes2 = new JSONArray();
            for (int i = arrayNotes.length() - 1; i >= 0; i--) {
                tempArrayNotes2.put(arrayNotes.get(i));
            }
            arrayNotes = tempArrayNotes2;

            writeToFile(arrayNotes.toString(), context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для получения данных из JSON-файла
     * @param context - Контекст
     * @return Словарь распарсенный из JSON-объекта файла
     */

    public HashMap<Integer, List<String>> readJSONObject (Context context) {
        HashMap<Integer, List<String>> notes = new HashMap();
        try {
            JSONArray jsonArray = new JSONArray(readFromFile(context));

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                List<String> data = new ArrayList<>();

                String str = jsonObject.toString();
                str = str.substring(2, str.length());
                str = str.substring(0, str.indexOf("{") - 2);

                data.add(jsonObject.getJSONObject(str).getString("BPM"));
                data.add(jsonObject.getJSONObject(str).getString("date"));
                data.add(jsonObject.getJSONObject(str).getString("time"));
                data.add(jsonObject.getJSONObject(str).getString("exercise"));
                data.add(jsonObject.getJSONObject(str).getString("dateFormat"));

                notes.put(i, data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return notes;
    }

    /**
     * Метод для редактирования записи журнала
     * @param context - Контекст
     * @param oldBPM - Старое BPM
     * @param olddate - Старая дата
     * @param oldtime - Старое время
     * @param oldexercise - Старое описание в журнале
     * @param newBPM - Новое BPM
     * @param newdate - Новая дата
     * @param newtime - Новое время
     * @param newexercise - Новое описание в журнале
     */

    public void editJSONObject (Context context, String oldBPM, String olddate, String oldtime, String oldexercise, String newBPM, String newdate, String newtime, String newexercise) {
        JSONArray arrayNotes = new JSONArray();
        HashMap<Integer, List<String>> notes = readJSONObject(context);

        try {
            for (int i = 0; i < notes.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                String str1 = notes.get(i).get(0);
                String str2 = notes.get(i).get(1);
                String str3 = notes.get(i).get(2);
                String str4 = notes.get(i).get(3);
                String str5 = notes.get(i).get(4);

                if (str1.equals(oldBPM) && str2.equals(olddate) && str3.equals(oldtime) && str4.equals(oldexercise)) {
                    jsonObject1.put("BPM", newBPM);
                    jsonObject1.put("date", newdate);
                    jsonObject1.put("time", newtime);
                    jsonObject1.put("exercise", newexercise);
                    jsonObject1.put("dateFormat", str5);

                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2.put(newBPM + newtime + newexercise, jsonObject1);
                    arrayNotes.put(jsonObject2);
                } else {
                    jsonObject1.put("BPM", notes.get(i).get(0));
                    jsonObject1.put("date", notes.get(i).get(1));
                    jsonObject1.put("time", notes.get(i).get(2));
                    jsonObject1.put("exercise", notes.get(i).get(3));
                    jsonObject1.put("dateFormat", notes.get(i).get(4));

                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2.put(notes.get(i).get(0) + notes.get(i).get(2) + notes.get(i).get(3), jsonObject1);
                    arrayNotes.put(jsonObject2);
                }
            }

            writeToFile(arrayNotes.toString(), context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для удаления объекта журнала
     * @param context - Контекст
     * @param BPM - BPM
     * @param date - Дата
     * @param time - Время
     * @param exercise - Описание в журнале
     */

    public void removeJSONObject (Context context, String BPM, String date, String time, String exercise) {
        JSONArray arrayNotes = new JSONArray();
        HashMap<Integer, List<String>> notes = readJSONObject(context);

        try {
            for (int i = 0; i < notes.size(); i++) {
                BPM = BPM.substring(BPM.indexOf(" ") + 1, BPM.length());

                if (!(notes.get(i).get(0).equals(BPM) && notes.get(i).get(1).equals(date) && notes.get(i).get(2).equals(time) && notes.get(i).get(3).equals(exercise))) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("BPM", notes.get(i).get(0));
                    jsonObject1.put("date", notes.get(i).get(1));
                    jsonObject1.put("time", notes.get(i).get(2));
                    jsonObject1.put("exercise", notes.get(i).get(3));
                    jsonObject1.put("dateFormat", notes.get(i).get(4));

                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2.put(notes.get(i).get(0) + notes.get(i).get(2) + notes.get(i).get(3), jsonObject1);

                    arrayNotes.put(jsonObject2);
                }
            }

            writeToFile(arrayNotes.toString(), context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для поиска объекта журнала
     * @param context - Контекст
     * @param date - Дата
     * @return Объект в который необходимо было найти
     */

    public HashMap<Integer, List<String>> findJSONObject (Context context, String date) {
        HashMap<Integer, List<String>> notes = new HashMap();
        try {
            JSONArray jsonArray = new JSONArray(readFromFile(context));

            int j = 0;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                List<String> data = new ArrayList<>();

                String str = jsonObject.toString();
                str = str.substring(2, str.length());
                str = str.substring(0, str.indexOf("{") - 2);

                if (date.equals(jsonObject.getJSONObject(str).getString("date"))) {
                    data.add(jsonObject.getJSONObject(str).getString("BPM"));
                    data.add(jsonObject.getJSONObject(str).getString("date"));
                    data.add(jsonObject.getJSONObject(str).getString("time"));
                    data.add(jsonObject.getJSONObject(str).getString("exercise"));
                    notes.put(j, data);
                    j++;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return notes;
    }

    /**
     * Метод для изменения формата даты объекта журнала
     * @param context - Контекст
     * @param dateFormat - Формат даты
     */

    public void changeDateFormat (Context context, String dateFormat) {
        JSONArray arrayNotes = new JSONArray();
        HashMap<Integer, List<String>> notes = readJSONObject(context);

        try {
            for (int i = 0; i < notes.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();

                jsonObject1.put("BPM", notes.get(i).get(0));

                String date = "";
                date += notes.get(i).get(1).substring(notes.get(i).get(1).indexOf('.') + 1, notes.get(i).get(1).length());
                date = date.substring(0, date.indexOf('.') + 1);
                date += notes.get(i).get(1).substring(0, notes.get(i).get(1).indexOf('.') + 1);
                date += notes.get(i).get(1).substring(notes.get(i).get(1).length() - 4, notes.get(i).get(1).length());
                jsonObject1.put("date", date);

                jsonObject1.put("time", notes.get(i).get(2));
                jsonObject1.put("exercise", notes.get(i).get(3));
                jsonObject1.put("dateFormat", dateFormat);

                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put(notes.get(i).get(0) + notes.get(i).get(2) + notes.get(i).get(3), jsonObject1);
                arrayNotes.put(jsonObject2);
            }

            writeToFile(arrayNotes.toString(), context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Получение списка всех данных журнала
     * @param context - Контекст
     * @return Массив строк данных журнала
     */

    public String[] getAllDates (Context context) {
        String[] dates = null;
        try {
            JSONArray jsonArray = new JSONArray(readFromFile(context));
            dates = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                String str = jsonObject.toString();
                str = str.substring(2, str.length());
                str = str.substring(0, str.indexOf("{") - 2);

                String[] splitDate = jsonObject.getJSONObject(str).getString("date").split("\\.");

                switch (jsonObject.getJSONObject(str).getString("dateFormat")) {
                    case "DD.MM.YYYY":
                        dates[i] = splitDate[2] + "-" + String.valueOf(Integer.valueOf(splitDate[1]) - 1) + "-" + splitDate[0];
                        break;

                    case "MM.DD.YYYY":
                        dates[i] = splitDate[2] + "-" + String.valueOf(Integer.valueOf(splitDate[0]) - 1) + "-" + splitDate[1];
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dates;
    }

    /**
     * Метод для сортировки объектов журнала по дате
     * @param context - Контекст
     * @return Отсортированный массив по дате
     */

    public Map<Integer, List<String>> getORDERBYDates (Context context) {
        HashMap<Integer, List<String>> tempNotes = new HashMap();
        HashMap<Integer, List<String>> notes = new HashMap();
        try {
            JSONArray jsonArray = new JSONArray(readFromFile(context));

            int maxMonth = 1;
            int maxDate = 1;
            int maxYear = 1;
            HashMap<Integer, List<Integer>> dates = new HashMap<>();
            String dateFormat = "";

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                List<String> data = new ArrayList<>();

                String str = jsonObject.toString();
                str = str.substring(2, str.length());
                str = str.substring(0, str.indexOf("{") - 2);

                data.add(jsonObject.getJSONObject(str).getString("BPM"));
                data.add(jsonObject.getJSONObject(str).getString("date"));
                data.add(jsonObject.getJSONObject(str).getString("time"));
                data.add(jsonObject.getJSONObject(str).getString("exercise"));
                data.add(jsonObject.getJSONObject(str).getString("dateFormat"));

                notes.put(i, data);

                dateFormat = data.get(4);
                List<Integer> tempDates = new ArrayList<>();
                String[] currentDate = data.get(1).split("\\.");
                switch (dateFormat) {
                    case "DD.MM.YYYY":
                        if (Integer.valueOf(currentDate[2]) > maxYear) {
                            maxDate = Integer.valueOf(currentDate[0]);
                            maxMonth = Integer.valueOf(currentDate[1]);
                            maxYear = Integer.valueOf(currentDate[2]);
                        } else {
                            if (Integer.valueOf(currentDate[2]) == maxYear) {
                                if (Integer.valueOf(currentDate[1]) > maxMonth) {
                                    maxDate = Integer.valueOf(currentDate[0]);
                                    maxMonth = Integer.valueOf(currentDate[1]);
                                    maxYear = Integer.valueOf(currentDate[2]);
                                } else {
                                    if (Integer.valueOf(currentDate[1]) == maxMonth) {
                                        if (Integer.valueOf(currentDate[0]) > maxDate) {
                                            maxDate = Integer.valueOf(currentDate[0]);
                                            maxMonth = Integer.valueOf(currentDate[1]);
                                            maxYear = Integer.valueOf(currentDate[2]);
                                        }
                                    }
                                }
                            }
                        }

                        tempDates.add(Integer.valueOf(currentDate[0]));
                        tempDates.add(Integer.valueOf(currentDate[1]));
                        tempDates.add(Integer.valueOf(currentDate[2]));
                        break;

                    case "MM.DD.YYYY":
                        if (Integer.valueOf(currentDate[2]) > maxYear) {
                            maxDate = Integer.valueOf(currentDate[1]);
                            maxMonth = Integer.valueOf(currentDate[0]);
                            maxYear = Integer.valueOf(currentDate[2]);
                        } else {
                            if (Integer.valueOf(currentDate[2]) == maxYear) {
                                if (Integer.valueOf(currentDate[0]) > maxMonth) {
                                    maxDate = Integer.valueOf(currentDate[1]);
                                    maxMonth = Integer.valueOf(currentDate[0]);
                                    maxYear = Integer.valueOf(currentDate[2]);
                                } else {
                                    if (Integer.valueOf(currentDate[0]) == maxMonth) {
                                        if (Integer.valueOf(currentDate[1]) > maxDate) {
                                            maxDate = Integer.valueOf(currentDate[1]);
                                            maxMonth = Integer.valueOf(currentDate[0]);
                                            maxYear = Integer.valueOf(currentDate[2]);
                                        }
                                    }
                                }
                            }
                        }

                        tempDates.add(Integer.valueOf(currentDate[1]));
                        tempDates.add(Integer.valueOf(currentDate[0]));
                        tempDates.add(Integer.valueOf(currentDate[2]));
                        break;
                }
                dates.put(i, tempDates);
            }

            int count = 0;
            int tempmaxDate = maxDate;
            int tempmaxMonth = maxMonth;
            int tempmaxYear = maxYear;
            while (count != dates.size()) {
                maxDate= tempmaxDate;
                maxMonth = tempmaxMonth;
                maxYear = tempmaxYear;

                tempmaxDate = 1;
                tempmaxMonth = 1;
                tempmaxYear = 1;

                for (int i = 0; i < dates.size(); i++) {
                    if (dates.get(i).get(0) == maxDate && dates.get(i).get(1) == maxMonth && dates.get(i).get(2) == maxYear) {
                        tempNotes.put(count, notes.get(i));
                        count++;
                    }
                    Log.e(dates.get(i).toString(), dates.get(i).toString());

                    boolean flag = false;
                    if (dates.get(i).get(2) < maxYear){
                        flag = true;
                    } else {
                        if (dates.get(i).get(2) == maxYear) {
                            if (dates.get(i).get(1) < maxMonth) {
                                flag = true;
                            } else {
                                if (dates.get(i).get(1) == maxMonth) {
                                    if (dates.get(i).get(0) < maxDate) {
                                        flag = true;
                                    }
                                }
                            }
                        }
                    }

                    if (flag) {
                        if (dates.get(i).get(2) > tempmaxYear) {
                            tempmaxDate = dates.get(i).get(0);
                            tempmaxMonth = dates.get(i).get(1);
                            tempmaxYear = dates.get(i).get(2);
                        } else {
                            if (dates.get(i).get(2) == tempmaxYear) {
                                if (dates.get(i).get(1) > tempmaxMonth) {
                                    tempmaxDate = dates.get(i).get(0);
                                    tempmaxMonth = dates.get(i).get(1);
                                    tempmaxYear = dates.get(i).get(2);
                                } else {
                                    if (dates.get(i).get(1) == tempmaxMonth) {
                                        if (dates.get(i).get(0) > tempmaxDate) {
                                            tempmaxDate = dates.get(i).get(0);
                                            tempmaxMonth = dates.get(i).get(1);
                                            tempmaxYear = dates.get(i).get(2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<Integer, List<String>> map = new TreeMap<>(tempNotes);
        return map;
    }

    /**
     * Метод по поиску объектов журнала по месяцу
     * @param context - Контекст
     * @param month - Месяц
     * @return Объекты журнала конкретного месяца
     */

    public HashMap<Integer, List<String>> findMonth (Context context, String month) {
        HashMap<Integer, List<String>> notes = new HashMap();
        try {
            JSONArray jsonArray = new JSONArray(readFromFile(context));

            int j = 0;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                List<String> data = new ArrayList<>();

                String str = jsonObject.toString();
                str = str.substring(2, str.length());
                str = str.substring(0, str.indexOf("{") - 2);

                switch (jsonObject.getJSONObject(str).getString("dateFormat")) {
                    case "DD.MM.YYYY":
                        if (month.equals(jsonObject.getJSONObject(str).getString("date").split("\\.")[1])) {
                            data.add(jsonObject.getJSONObject(str).getString("BPM"));
                            data.add(jsonObject.getJSONObject(str).getString("date"));
                            data.add(jsonObject.getJSONObject(str).getString("time"));
                            data.add(jsonObject.getJSONObject(str).getString("exercise"));
                            notes.put(j, data);
                            j++;
                        }
                        break;

                    case "MM.DD.YYYY":
                        if (month.equals(jsonObject.getJSONObject(str).getString("date").split("\\.")[0])) {
                            data.add(jsonObject.getJSONObject(str).getString("BPM"));
                            data.add(jsonObject.getJSONObject(str).getString("date"));
                            data.add(jsonObject.getJSONObject(str).getString("time"));
                            data.add(jsonObject.getJSONObject(str).getString("exercise"));
                            notes.put(j, data);
                            j++;
                        }
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return notes;
    }
}