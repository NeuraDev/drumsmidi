package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Класс необходимый для отрисовки прямоугольника нотного стана
 *
 * Created by kmoaz on 20.10.2016.
 */

public class drawStave extends View {

    /**
     * Поле - Параметр для отрисовки ноты
     */

    private Paint paint = new Paint();

    /**
     * Поле - Первая точка для отрисовки ноты
     */

    Point point1;

    /**
     * Поле - Вторая точка для отрисовки ноты
     */

    Point point2;

    /**
     * Конструктор
     * @param context
     */

    public drawStave (Context context) {
        super(context);
    }

    /**
     * Метод отрисовки прямоугольника нотного стана
     * @param canvas
     */

    public void onDraw (Canvas canvas) {
        paint.setColor(Color.parseColor("#2E303A"));
        paint.setStrokeWidth(0);
        canvas.drawRect(point1.x, point1.y, point2.x, point2.y, paint);
    }
}


