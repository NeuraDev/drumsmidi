package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

import com.drummerscope.android.R;

/**
 * Класс необходимый для отрисовки линии метронома
 *
 * Created by kmoaz on 21.01.2017.
 */

public class MetronomeLine extends View {

    /**
     * Поле - Параметр для отрисовки линии метронома
     */

    private Paint paint = new Paint();

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Поле - Первая точка для отрисовки линии метронома
     */

    Point point1;

    /**
     * Поле - Вторая точка для отрисовки линии метронома
     */

    Point point2;

    /**
     * Конструктор
     * @param context - Контекст
     */

    public MetronomeLine(Context context) {
        super(context);
        this.mContext = context;
    }

    /**
     * Метод отрисовки линии метронома
     * @param canvas
     */

    public void onDraw (Canvas canvas) {
        paint.setColor(mContext.getResources().getColor(R.color.myRed));
        paint.setAlpha(150);
        paint.setStrokeWidth(5);

        canvas.drawLine(point1.x, point1.y, point2.x, point2.y, paint);
    }
}
