package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Класс необходимый для отрисовки вертикальной линии нотного стана
 *
 * Created by kmoaz on 16.01.2017.
 */

public class drawVerticalLine extends View {

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Поле - Параметр для отрисовки вертикальной линии нотного стана
     */

    private Paint paint = new Paint();

    /**
     * Поле - Параметр для определения прозрачности вертикальной линии нотного стана
     */

    private int velocity;

    /**
     * Поле - Параметр для прозрачности вертикальной линии нотного стана
     */

    private int transparency;

    /**
     * Поле - Первая точка для отрисовки вертикальной линии нотного стана
     */

    Point point1;

    /**
     * Поле - Вторая точка для отрисовки вертикальной линии нотного стана
     */

    Point point2;

    /**
     * Поле - Цвет вертикальной линии нотного стана
     */

    public int color = -65536;

    /**
     * Конструктор
     * @param context - Контекст
     * @param velocity - Параметр для определения прозрачности вертикальной линии нотного стана
     */

    public drawVerticalLine(Context context, int velocity) {
        super(context);
        mContext = context;
        this.velocity = velocity;
        TransparencyCalc velocityCalc = new TransparencyCalc();
        this.transparency = velocityCalc.calc(velocity);
        //this.transparency = (int)((double)velocity / 128.0 * 255.0);
    }

    /**
     * Метод отрисовки вертикальной линии нотного стана
     *
     * @param canvas
     */

    public void onDraw (Canvas canvas) {
        paint.setColor(color);
        paint.setStrokeWidth(5);
        paint.setAlpha(transparency);
        canvas.drawLine(point1.x, point1.y, point2.x, point2.y, paint);
    }
}
