package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.view.View;

/**
 * Класс необходимый для отрисовки ноты Треугольник
 *
 * Created by kmoaz on 16.01.2017.
 */

public class drawTriangle extends View {

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Поле - Параметр для отрисовки ноты
     */

    private Paint paint = new Paint();

    /**
     * Поле - Параметр для соединения линий в фигуру
     */

    private Path path = new Path();

    /**
     * Поле - Параметр для определения прозрачности ноты
     */

    private int velocity;

    /**
     * Поле - Параметр для прозрачности ноты
     */

    private int transparency;

    /**
     * Поле - Первая точка для отрисовки ноты
     */

    Point point1;

    /**
     * Поле - Вторая точка для отрисовки ноты
     */

    Point point2;

    /**
     * Поле - Третья точка для отрисовки ноты
     */

    Point point3;

    /**
     * Поле - Цвет ноты
     */

    public int color = -65536;

    /**
     * Конструктор
     * @param context - Контекст
     * @param velocity - Параметр для определения прозрачности ноты
     */

    public drawTriangle(Context context,  int velocity) {
        super(context);
        mContext = context;
        this.velocity = velocity;
        TransparencyCalc velocityCalc = new TransparencyCalc();
        this.transparency = velocityCalc.calc(velocity);
        //this.transparency = (int)((double)velocity / 128.0 * 255.0);
    }

    /**
     * Метод отрисовки ноты
     * @param canvas
     */

    public void onDraw (Canvas canvas) {
        //paint.setColor(mContext.getResources().getColor(com.drummerscope.main.R.color.myRed));
        paint.setColor(color);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE);

        path.moveTo(point1.x, point1.y);
        path.lineTo(point2.x, point2.y);
        path.lineTo(point3.x, point3.y);
        path.close();

        canvas.drawPath(path, paint);
    }
}
