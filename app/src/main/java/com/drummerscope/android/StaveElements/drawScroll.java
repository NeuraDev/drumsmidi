package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;


import com.drummerscope.android.Tools.ReadWriteJSONSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by kmoaz on 01.11.2016.
 */

public class drawScroll extends View {

    /**
     * Поле - Тэг
     */

    private static final String TAG = "DRAW SCROLL";

    /**
     * Поле - Высота нотного стана на экране
     */

    private int height;

    /**
     * Поле - Ширина нотного стана на экране
     */

    private int width;

    /**
     * Поле - "Размер" внутри такта
     */

    private double size;

    /**
     * Поле - Количество дорожек нотного стана с учётом нижней границы
     */

    private int rowCount = 7;

    /**
     * Поле - Лист нот на нотном стане
     */

    private List<Beat> beats;

    /**
     * Поле - Высота ноты
     */

    private int _heightNote;

    /**
     * Поле - Ширина ноты
     */

    private int _widthNote;

    /**
     * Поле - Радиус ноты (для круглой фигуры)
     */

    private int _R;

    /**
     * Поле - Ширина от начала экрана до конца названия дорожек
     */

    private int _R1 = 16;

    /**
     * Поле - Массив названия дорожек нотного стана
     */

    private SparseIntArray Yrows;

    /**
     * Поле - Параметр для отрисовки ноты
     */

    private Paint paint = new Paint();

    /**
     * Поле - Первая точка для отрисовки нотного стана
     */

    private Point point1;

    /**
     * Поле - Вторая точка для отрисовки нотного стана
     */

    private Point point2;

    /**
     * Поле - Высота дорожки
     */

    private int countPixelOfHeight;

    /**
     * Поле - Размер номера такта
     */

    private int sizeOfNumber = 16;

    /**
     * Поле - Контекст
     */

    Context context;

    /**
     * Конструктор
     * @param context - Контекст
     * @param width - Ширина нотного стана
     * @param height - Высота нотного стана
     * @param size - "Размер" внутри такта
     * @param beats - Ноты которые были наиграны в процессе
     * @param heightNote - Высота ноты
     * @param widthNote - Ширина ноты
     * @param R - Радиус ноты
     * @param Yrows - Массив названия дорожек нотного стана
     * @param _R1 - Ширина от начала экрана до конца
     * @param sizeOfNumber - Размер номера такта
     */

    public drawScroll(Context context, int width, int height, double size, List<Beat> beats,
                      int heightNote, int widthNote, int R, SparseIntArray Yrows, int _R1, int sizeOfNumber) {
        super(context);
        this.height = height;
        this.width = width;
        this.size = size;
        this.beats = beats;
        this._heightNote = heightNote;
        this._widthNote = widthNote;
        this._R = R;
        this.Yrows = Yrows;
        this.context = context;
        this._R1 = _R1;
        this.sizeOfNumber = sizeOfNumber;

        point1 = new Point((_R1 + _R), 0);
        point2 = new Point(beats.get(beats.size() - 1).displayNumber * (width - (_R1 + _R)) + 2 * (_R1 + _R), height);

        countPixelOfHeight = (height / rowCount);
        countPixelOfHeight += Math.floor(countPixelOfHeight / 12);
    }

    /**
     * Переопределённый метод для расширения ширины вью
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */

    @Override
    public void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(MeasureSpec.getSize(point2.x), MeasureSpec.getSize(heightMeasureSpec));

        //Log.e(TAG, "WIDTH OF STAVE = " + String.valueOf(MeasureSpec.getSize(widthMeasureSpec) + point2.x));
        //Log.e(TAG, "WIDTH OF STAVE ON MEASURE = " + String.valueOf(point2.x));
        //Log.e(TAG, "WIDTH OF STAVE = " + String.valueOf(width));
    }

    /**
     * Метод отрисовки нотного стана
     * @param canvas
     */

    @Override
    public void onDraw (Canvas canvas) {
        //Отрисовка прямоугольника для стана
        paint.setColor(Color.parseColor("#2E303A"));
        paint.setStrokeWidth(0);
        //paint.setColor(Color.GREEN);
        canvas.drawRect(point1.x, point1.y, point2.x, point2.y, paint);

        paint.setColor(Color.parseColor("#FFFFFF"));
        paint.setAlpha(20);
        paint.setStrokeWidth(1);

        ReadWriteJSONSettings readWriteJSONSettings = new ReadWriteJSONSettings(context);
        //String[] trackTitle = readWriteJSONSettings.read("tracktitle").split(" ");
        String[] trackTitle = new String[6];
        for (int i = 0; i < 6; i++) {
            try {
                trackTitle[i] = new JSONObject(readJSON("trackNames")).getString("track" + String.valueOf(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Отрисовка горизонтальных линий
        for (int i = 0; i < rowCount - 1; i++) {
            paint.setAlpha(120);
            canvas.drawLine((_R1 + _R), (i + 1) * countPixelOfHeight, point2.x, (i + 1) * countPixelOfHeight, paint);
        }

        int taktCount = Integer.valueOf(readWriteJSONSettings.read("taktcount"));

        Paint paintNum = new Paint();
        paintNum.setStrokeWidth(2);
        paintNum.setTextSize(sizeOfNumber);
        paintNum.setColor(Color.parseColor("#FFFFFF"));

        Paint paintRect = new Paint();
        paintRect.setStrokeWidth(1);
        paintRect.setStyle(Paint.Style.FILL_AND_STROKE);
        paintRect.setColor(Color.parseColor("#19A4CB"));

        //Отрисовка линий размера
        double countPixelOfWidth = (double) (width - (_R1 + _R)) / (double) (taktCount * size);
        double p = 0;
        if (size % 1 != 0) {
            p = (double) (width - (_R1 + _R)) / (double) (taktCount) - Math.floor(size) * countPixelOfWidth;
        }
        for (int i = 0; i < taktCount * beats.get(beats.size() - 1).displayNumber + 1; i++) {
            for (int j = 0; j < Math.floor(size); j++) {
                paint.setStrokeWidth(3);
                //paint.setAlpha(60);
                paint.setAlpha(150);
                canvas.drawLine((int) Math.round((j + (i * Math.floor(size)) + 1) * countPixelOfWidth + i * p) + (_R1 + _R), 0, (int) Math.round((j + (i * Math.floor(size)) + 1) * countPixelOfWidth + i * p) + (_R1 + _R), height, paint);
            }
        }

        //Отрисовка линий и номера такта
        size = Integer.valueOf(readWriteJSONSettings.read("size"));
        countPixelOfWidth = (double) (width - (_R1 + _R)) / (double) (taktCount * size);
        for (int i = 0; i < taktCount * beats.get(beats.size() - 1).displayNumber + 1; i++) {
            paint.setStrokeWidth(4);
            paint.setAlpha(255);
            paint.setColor(Color.parseColor("#19A4CB"));
            //paint.setAlpha(150);
            canvas.drawLine((int)Math.round(size * i * countPixelOfWidth) + (_R1 + _R), sizeOfNumber, (int)Math.round(size * i * countPixelOfWidth) + (_R1 + _R), height, paint);

            //Отрисовка номера такта
            canvas.drawRoundRect(new RectF((int)Math.round(countPixelOfWidth * size * i - 1.75 * sizeOfNumber) + (_R1 + _R), 2, (int)Math.round(countPixelOfWidth * size * i + 1.75 * sizeOfNumber) + (_R1 + _R), sizeOfNumber + 2), sizeOfNumber / 2, sizeOfNumber / 2, paintRect);
            canvas.drawText(String.valueOf(i + 1), (int)Math.round(countPixelOfWidth * size * i - String.valueOf(i + 1).length() * sizeOfNumber / 4) + (_R + _R1), sizeOfNumber, paintNum);
        }

        //Отрисовка линий внутри размера
        paint.setColor(Color.parseColor("#FFFFFF"));
        double temp = (Double.valueOf(readWriteJSONSettings.read("devidetakt")) / Double.valueOf(readWriteJSONSettings.read("notelength")) * size);
        if (Boolean.valueOf(readWriteJSONSettings.read("dividetriplet"))) {
            temp *= 1.5;
        }
        countPixelOfWidth = (double) (width - (_R1 + _R)) / (taktCount * temp);
        if (temp % 1 != 0) {
            p = (double) (width - (_R1 + _R)) / (double) (taktCount) - Math.floor(temp) * countPixelOfWidth;
        } else {
            p = 0;
        }
        for (int i = 0; i < taktCount * beats.get(beats.size() - 1).displayNumber + 1; i++) {
            for (int j = 0; j < Math.floor(temp); j++) {
                //paint.setStrokeWidth(1);
                //paint.setAlpha(20);
                paint.setStrokeWidth(3);
                paint.setAlpha(60);
                canvas.drawLine((int) Math.round((j + (i * Math.floor(temp)) + 1) * countPixelOfWidth + i * p) + (_R1 + _R), 0, (int) Math.round((j + (i * Math.floor(temp)) + 1) * countPixelOfWidth + i * p) + (_R1 + _R), height, paint);
            }
        }

        //Отрисовка всех нот на стане
        paint.setColor(Integer.valueOf(readWriteJSONSettings.read("notecolor")));
        paint.setStrokeWidth(3);
        TransparencyCalc velocityCalc = new TransparencyCalc();
        //this.transparency = velocityCalc.calc(velocity);
        //this.transparency = (int)((double)velocity / 128.0 * 255.0);

        for (int i = 0; i < beats.size(); i++) {
            int x = beats.get(i).column;
            int y = Yrows.get(beats.get(i).row);

            /**if (x - _widthNote < 0) {
             x = _widthNote;
             }*/

            Path path = new Path();
            switch (beats.get(i).icon) {
                case "triangle_for_settings":
                    paint.setStrokeWidth(1);
                    //paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                    //paint.setAlpha((int)((double)beats.get(i).velocity / 128.0 * 255.0));
                    paint.setAlpha(velocityCalc.calc(beats.get(i).velocity));
                    paint.setStyle(Paint.Style.FILL_AND_STROKE);
                    path.moveTo(x, y - (_widthNote + 6) / 3);
                    path.lineTo(x + (_widthNote + 6) / 3, y + (_heightNote + 6) / 3);
                    path.lineTo(x - (_widthNote + 6) / 3, y + (_heightNote + 6) / 3);
                    path.close();

                    canvas.drawPath(path, paint);
                    break;

                /**case "triangle_for_settings":
                 paint.setStrokeWidth(1);
                 paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                 paint.setStyle(Paint.Style.STROKE);
                 paint.setAlpha((int)((double)beats.get(i).velocity / 127.0 * 255.0));
                 path.moveTo(x, y - (_widthNote + 6) / 2);
                 path.lineTo(x + (_widthNote + 6) / 2, y + (_heightNote + 6) / 2);
                 path.lineTo(x - (_widthNote + 6) / 2, y + (_heightNote + 6) / 2);
                 path.close();

                 canvas.drawPath(path, paint);
                 break;*/

                case "rhombus":
                    //paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                    paint.setStrokeWidth(1);
                    //paint.setAlpha((int)((double)beats.get(i).velocity / 128.0 * 255.0));
                    paint.setAlpha(velocityCalc.calc(beats.get(i).velocity));
                    paint.setStyle(Paint.Style.FILL_AND_STROKE);

                    path.moveTo(x, y - _heightNote / 2);
                    path.lineTo(x + _widthNote / 2 - 5, y);
                    path.lineTo(x, y + _heightNote / 2);
                    path.lineTo(x - _widthNote / 2 + 5, y);

                    path.close();
                    canvas.drawPath(path, paint);
                    break;

                /**case "rhombus":
                 paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                 paint.setStrokeWidth(1);
                 paint.setStyle(Paint.Style.STROKE);
                 paint.setAlpha((int)((double)beats.get(i).velocity / 127.0 * 255.0));

                 path.moveTo(x, y - (_heightNote + 6) / 2);
                 path.lineTo(x + (_widthNote + 6) / 2, y);
                 path.lineTo(x, y + (_heightNote + 6 ) / 2);
                 path.lineTo(x - (_widthNote + 6) / 2, y);

                 path.close();
                 canvas.drawPath(path, paint);
                 break;*/

                case "cross":
                    //paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                    paint.setStrokeWidth(3);
                    //paint.setAlpha((int)((double)beats.get(i).velocity / 128.0 * 255.0));
                    paint.setAlpha(velocityCalc.calc(beats.get(i).velocity));

                    canvas.drawLine(x - (int)(_widthNote / Math.sqrt(2) / 2), y - (int)(_heightNote / Math.sqrt(2) / 2), x + (int)(_widthNote / Math.sqrt(2) / 2), y + (int)(_heightNote / Math.sqrt(2) / 2), paint);
                    canvas.drawLine(x - (int)(_widthNote / Math.sqrt(2) / 2), y + (int)(_heightNote / Math.sqrt(2) / 2), x + (int)(_widthNote / Math.sqrt(2) / 2), y - (int)(_heightNote / Math.sqrt(2) / 2), paint);
                    break;

                case "cross_in_circle":
                    //paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                    paint.setStrokeWidth(3);
                    paint.setStyle(Paint.Style.STROKE);
                    //paint.setAlpha((int)((double)beats.get(i).velocity / 128.0 * 255.0));
                    paint.setAlpha(velocityCalc.calc(beats.get(i).velocity));
                    canvas.drawCircle(x, y, _R, paint);

                    canvas.drawLine(x - (int)(_R / Math.sqrt(2)), y - (int)(_R / Math.sqrt(2)), x + (int)(_R / Math.sqrt(2)), y + (int)(_R / Math.sqrt(2)), paint);
                    canvas.drawLine(x - (int)(_R / Math.sqrt(2)), y + (int)(_R / Math.sqrt(2)), x + (int)(_R / Math.sqrt(2)), y - (int)(_R / Math.sqrt(2)), paint);
                    break;

                case "line":
                    //paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                    paint.setStrokeWidth(5);
                    //paint.setAlpha((int)((double)beats.get(i).velocity / 128.0 * 255.0));
                    paint.setAlpha(velocityCalc.calc(beats.get(i).velocity));

                    canvas.drawLine(x, y - _heightNote / 2, x, y + _heightNote / 2, paint);
                    break;

                /*case "rectangle":
                    paint.setStrokeWidth(1);
                    //paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                    //paint.setAlpha((int)((double)beats.get(i).velocity / 128.0 * 255.0));
                    paint.setAlpha(velocityCalc.calc(beats.get(i).velocity));
                    paint.setStyle(Paint.Style.FILL_AND_STROKE);
                    canvas.drawRect(x - _R, y - _R, x + _R, y + _R, paint);
                    break;*/

                /**case "fill_circle":
                 //paint.setColor(getResources().getColor(com.drummidi.sunwi.main.R.color.myRed));
                 paint.setStrokeWidth(1);
                 //paint.setAlpha((int)((double)beats.get(i).velocity / 128.0 * 255.0));
                 paint.setAlpha(velocityCalc.calc(beats.get(i).velocity));
                 paint.setStyle(Paint.Style.FILL_AND_STROKE);
                 canvas.drawCircle(x, y, _R, paint);
                 break;*/
            }
        }
    }

    /**
     * Метод для считывания JSON-файла
     *
     * @param name - Название файла
     * @return Строка распарсенная из JSON-файла
     */

    private String readJSON(String name) {
        try {
            String path = context.getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj.toString();
        } catch (JSONException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        }
    }
}