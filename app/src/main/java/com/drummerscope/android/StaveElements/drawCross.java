package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Класс необходимый для отрисовки ноты Крест
 *
 * Created by kmoaz on 16.01.2017.
 */

public class drawCross extends View {

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Поле - Параметр для отрисовки ноты
     */

    private Paint paint = new Paint();

    /**
     * Поле - Параметр для определения прозрачности ноты
     */

    private int velocity;

    /**
     * Поле - Параметр для прозрачности ноты
     */

    private int transparency;

    /**
     * Поле - Первая точка для отрисовки ноты
     */

    Point point1;

    /**
     * Поле - Вторая точка для отрисовки ноты
     */

    Point point2;

    /**
     * Поле - Третья точка для отрисовки ноты
     */

    Point point3;

    /**
     * Поле - Четвёртая точка для отрисовки ноты
     */

    Point point4;

    /**
     * Поле - Цвет ноты
     */

    public int color = -65536;

    /**
     * Конструктор
     * @param context - Контекст
     * @param velocity - Параметр для определения прозрачности ноты
     */

    public drawCross(Context context, int velocity) {
        super(context);
        mContext = context;
        this.velocity = velocity;
        TransparencyCalc velocityCalc = new TransparencyCalc();
        this.transparency = velocityCalc.calc(velocity);
        //this.transparency = (int)((double)velocity / 128.0 * 255.0);
    }

    /**
     * Метод отрисовки ноты
     * @param canvas
     */

    public void onDraw (Canvas canvas) {
        paint.setColor(color);
        paint.setStrokeWidth(3);
        paint.setAlpha(transparency);

        canvas.drawLine(point1.x, point1.y, point2.x, point2.y, paint);
        canvas.drawLine(point3.x, point3.y, point4.x, point4.y, paint);
    }
}
