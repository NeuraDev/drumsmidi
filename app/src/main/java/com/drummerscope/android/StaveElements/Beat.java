package com.drummerscope.android.StaveElements;

/**
 * Created by kmoaz on 31.10.2016.
 */

public class Beat {

    /**
     * Поле - Номер дорожки
     */

    int row;

    /**
     * Поле - Номер такта
     */

    int column;

    /**
     * Поле - Номер экрана
     */

    int displayNumber;

    /**
     * Поле - Тип ноты
     */

    String note;

    /**
     * Поле - Иконка ноты
     */

    String icon;

    /**
     * Поле - Прозрачность
     */

    int velocity;

    /**
     * @param column - Номер такта
     * @param displayNumber - Номер экрана
     * @param icon - Иконка ноты
     * @param note - Тип ноты
     * @param row - Номер дорожки
     * @param velocity - Прозрачность
     * Конструктор - вспомогательный со всеми значениями полей
     */

    public Beat(int row, int column, int displayNumber, String note, String icon, int velocity) {
        this.row = row;
        this.column = column;
        this.displayNumber = displayNumber;
        this.note = note;
        this.icon = icon;
        this.velocity = velocity;
    }

    /**
     * Конструктор - Дефолтный
     */

    public Beat () {

    }
}
