package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.view.View;

import com.drummerscope.android.R;

/**
 * Класс необходимый для отрисовки ноты Ромб
 *
 * Created by kmoaz on 16.01.2017.
 */

public class drawRhombus extends View {

    /**
     * Поле - Параметр для отрисовки ноты
     */

    private Paint paint = new Paint();

    /**
     * Поле - Параметр для соединения линий в фигуру
     */

    private Path path = new Path();

    /**
     * Поле - Параметр для определения прозрачности ноты
     */

    private int velocity;

    /**
     * Поле - Параметр для прозрачности ноты
     */

    private int transparency;

    /**
     * Поле - Первая точка для отрисовки ноты
     */

    Point point1;

    /**
     * Поле - Вторая точка для отрисовки ноты
     */

    Point point2;

    /**
     * Поле - Третья точка для отрисовки ноты
     */

    Point point3;

    /**
     * Поле - Четвёртая точка для отрисовки ноты
     */

    Point point4;

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Конструктор
     * @param context
     * @param velocity
     */

    public drawRhombus(Context context, int velocity) {
        super(context);
        mContext = context;
        this.velocity = velocity;
        TransparencyCalc velocityCalc = new TransparencyCalc();
        this.transparency = velocityCalc.calc(velocity);
        //this.transparency = (int)((double)velocity / 128.0 * 255.0);
    }

    /**
     * Метод отрисовки ноты
     * @param canvas
     */

    public void onDraw (Canvas canvas) {
        paint.setColor(mContext.getResources().getColor(R.color.myRed));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setAlpha(transparency);

        path.moveTo(point1.x, point1.y);
        path.lineTo(point2.x, point2.y);
        path.lineTo(point3.x, point3.y);
        path.lineTo(point4.x, point4.y);

        path.close();
        canvas.drawPath(path, paint);
    }
}
