package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.view.View;

/**
 * Класс необходимый для отрисовки номера такта
 *
 * Created by kmoaz on 31.01.2017.
 */

public class drawNumberOfTakt extends View {

    /**
     * Поле - Контекст
     */

    private Context context;

    /**
     * Поле - Параметр для отрисовки номера такта
     */

    private Paint _paint;

    /**
     * Поле - Точка для отрисовки бэкграунда номера такта
     */

    public Point point;

    /**
     * Поле - Точка для отрисовки номера такта
     */

    public Point pointNum;

    /**
     * Поле - Размер номера такта
     */

    private int sizeOfNumber;

    /**
     * Поле - Порядковый номер такта
     */

    private String _number;

    /**
     * Конструктор
     * @param context
     * @param number
     * @param sizeOfNumber
     */

    public drawNumberOfTakt (Context context, String number, Integer sizeOfNumber) {
        super(context);
        this.context = context;
        this._number = number;
        this.sizeOfNumber = sizeOfNumber;
    }

    /**
     * Метод отрисовки номера такта
     * @param canvas
     */

    @Override
    public void onDraw (Canvas canvas) {
        _paint = new Paint();

        _paint.setStrokeWidth(1);
        _paint.setStyle(Paint.Style.FILL_AND_STROKE);
        _paint.setColor(Color.parseColor("#19A4CB"));
        //canvas.drawRoundRect(pointNum.x - 3 * sizeOfNumber / 2, 3, pointNum.x + 2 * sizeOfNumber, pointNum.y + 2, sizeOfNumber / 2, sizeOfNumber / 2, _paint);
        canvas.drawRoundRect(new RectF(pointNum.x - 3 * sizeOfNumber / 2, 3, pointNum.x + 2 * sizeOfNumber, pointNum.y + 2), sizeOfNumber / 2, sizeOfNumber / 2, _paint);

        _paint.setStrokeWidth(1);
        _paint.setStyle(Paint.Style.FILL);
        _paint.setTextSize(sizeOfNumber);
        _paint.setColor(Color.parseColor("#FFFFFF"));
        canvas.drawText(_number, point.x, point.y, _paint);
    }
}
