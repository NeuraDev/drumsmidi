package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Класс необходимый для отрисовки названия дорожки стана
 *
 * Created by kmoaz on 01.02.2017.
 */

public class drawTrackTitle extends View {

    /**
     * Поле - Контекст
     */

    private Context context;

    /**
     * Поле - Название дорожки
     */

    private String _title;

    /**
     * Поле - Размер названия дорожки
     */

    public int sizeOfTitle = 16;

    /**
     * Поле - Точка для отрисовки названия дорожки
     */

    public Point point;

    /**
     * Поле - Параметр для отрисовки названия дорожки
     */

    private Paint _paint;

    /**
     * Конструктор
     * @param context - Контекст
     * @param title - Название дорожки
     */

    public drawTrackTitle (Context context, String title) {
        super(context);
        this.context = context;
        this._title = title;
    }

    /**
     * Метод отрисовки названия дорожки
     * @param canvas
     */

    @Override
    public void onDraw (Canvas canvas) {
        _paint = new Paint();
        _paint.setColor(Color.parseColor("#FFFFFF"));
        _paint.setStrokeWidth(2);
        _paint.setTextSize(sizeOfTitle);
        canvas.drawText(_title, point.x, point.y, _paint);
    }
}
