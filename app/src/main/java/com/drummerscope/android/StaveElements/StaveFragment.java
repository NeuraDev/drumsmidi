package com.drummerscope.android.StaveElements;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;

import com.drummerscope.android.R;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;

import java.util.ArrayList;
import java.util.List;


/**
 * Класс для отрисовки нотного стана с заданными параметрами
 *
 * Created by kmoaz on 22.10.2016.
 */

public class StaveFragment extends Fragment {

    /**
     * Поле -
     */

    public static final int STAVE_SCALE = 65;

    /**
     * Поле - Тэг
     */

    private static final String TAG = "STAVE_FRAGMENT";

    /**
     * Поле - Лэйаут нотного стана на котором идёт отрисовка его компонентов
     */

    private RelativeLayout relativeLayout;

    /**
     * Поле - Количество дорожек нотного стана с учётом нижней границы
     */

    private int rowCount = 7;

    /**
     * Поле - Массив названия дорожек нотного стана
     */

    private SparseIntArray Yrows = new SparseIntArray(rowCount - 1);

    /**
     * Поле - Ширина нотного стана на экране
     */

    public int width;

    /**
     * Поле - Высота нотного стана на экране
     */

    public int height;

    /**
     * Поле - "Размер" внутри такта
     */

    private double size = 4;

    /**
     * Поле - Номер текущего экрана
     */

    public double displayCount = 0;

    /**
     * Поле - Параметр не используется
     */

    public boolean flag = true;

    /**
     * Поле - Main view из onCreateView(...)
     */

    private View rootView;

    /**
     * Поле - Высота дорожки
     */

    private int countPixelOfHeight;

    /**
     * Поле - Параметр не используется
     */

    private int countPixelOfWidthInt;

    /**
     * Поле - Ширина одного "Размера" внутри такта
     */

    private double countPixelOfWidth;

    /**
     * Поле - Массив названий дорожек такта
     */

    public String[] trackTitleFromFile;

    /**
     * Поле - Количество тактов на нотном стане
     */

    public int taktCount;

    /**
     * Поле - Лист нот на нотном стане
     */

    public List<Beat> beats;

    /**
     * Поле - Лист нот для отрисовки на всём нотном стане после остановки
     */

    public List<Beat> beats4DrawStave;

    /**
     * Поле - Параметр не используется
     */

    public int divideTaktCount;

    /**
     * Поле - Объект из которого берётся информация из настроек по параметрам ноты
     */

    private ReadWriteJSONSettings readWriteJSONSettings;

    /**
     * Поле - Цвет ноты
     */

    private int color;

    /**
     * Поле - Высота ноты
     */

    private int _heightNote;

    /**
     * Поле - Ширина ноты
     */

    private int _widthNote;

    /**
     * Поле - Радиус ноты (для круглой фигуры)
     */

    private int _R;

    /**
     * Поле - Ширина от начала экрана до конца названия дорожек
     */

    private int _R1;

    /**
     * Поле - Размер номера такта
     */

    private int sizeOfNumber;

    /**
     * Метод инициализирует View фрагмента для создания нотного стана
     *
     * @param inflater           - компонент для заполнения View
     * @param container          - контейнер View
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает View фрагмента
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_of_stave, container, false);
        relativeLayout = (RelativeLayout) rootView.findViewById(R.id.stave);

        setSizeOfBeats(rootView.getContext());

        readWriteJSONSettings = new ReadWriteJSONSettings(rootView.getContext());
        color = Integer.valueOf(readWriteJSONSettings.read("notecolor"));

        divideTaktCount = Integer.valueOf(readWriteJSONSettings.read("devidetakt")) / Integer.valueOf(readWriteJSONSettings.read("notelength"));

        beats = new ArrayList<>();
        beats4DrawStave = new ArrayList<>();
        displayCount = 0;

        ViewTreeObserver vto = relativeLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                width = relativeLayout.getWidth() - (_R1 + _R);
                Log.e(TAG, "WIDTH FROM RELATIVE = " + String.valueOf(width));
                height = relativeLayout.getHeight();

                ViewTreeObserver obs = relativeLayout.getViewTreeObserver();
                obs.removeOnGlobalLayoutListener(this);
                _stave(size);
                displayCount = 0;
                tempflag = true;
            }
        });

        return rootView;
    }

    /**
     * Поле - Параметр не используется
     */

    boolean tempflag = true;

    /**
     * Метод для отрисовки нового нотного стана
     * @param size - "Размер" внутри такта
     */

    public void _stave(double size) {
        width = relativeLayout.getWidth() - (_R1 + _R);
        countPixelOfWidth = (double) (width - (_R1 + _R)) / (double) (taktCount * size);
        countPixelOfHeight = height / rowCount;
        countPixelOfHeight += Math.floor(countPixelOfHeight / 12);
        this.size = size;
        drawStave stave = new drawStave(rootView.getContext());
        stave.point1 = new Point(_R1 + _R, 0);
        stave.point2 = new Point(width, height);
        relativeLayout.addView(stave);

        //Отрисовка линий размера
        double p = 0;
        if (size % 1.0 != 0.0) {
            p = (double) (width - (_R1 + _R)) / (double) (taktCount) - Math.floor(size) * countPixelOfWidth;
        }
        for (int i = 0; i < taktCount; i++) {
            for (int j = 0; j < Math.floor(size); j++) {
                drawLine line = new drawLine(rootView.getContext());
                line.point1 = new Point((int) Math.round((j + (i * Math.floor(size)) + 1) * countPixelOfWidth + i * p) + _R1 + _R, 0);
                line.point2 = new Point((int) Math.round((j + (i * Math.floor(size)) + 1) * countPixelOfWidth + i * p) + _R1 + _R, height);
                line.stroke = 3;
                line.alpha = 150;
                //line.alpha = 60;
                //line.stroke = 4;
                relativeLayout.addView(line);
            }
        }

        //Отрисовка линий такта
        size = Double.valueOf(readWriteJSONSettings.read("size"));
        countPixelOfWidth = (double) (width - (_R1 + _R)) / (double) (taktCount * size);
        for (int i = 0; i < taktCount + 1; i++) {
            drawLine line = new drawLine(rootView.getContext());
            line.point1 = new Point((int) Math.round(i * countPixelOfWidth * size) + _R1 + _R, sizeOfNumber);
            line.point2 = new Point((int) Math.round(i * countPixelOfWidth * size) + _R1 + _R, height);
            line.stroke = 4;
            line.alpha = 255;
            line.color = "#19A4CB";
            relativeLayout.addView(line);
        }

        //Отрисовка линий внутри размера
        double temp = (Double.valueOf(readWriteJSONSettings.read("devidetakt")) / Double.valueOf(readWriteJSONSettings.read("notelength")) * size);
        if (Boolean.valueOf(readWriteJSONSettings.read("dividetriplet"))) {
            temp *= 1.5;
        }
        countPixelOfWidth = (double) (width - (_R1 + _R)) / (taktCount * temp);
        if (temp % 1.0 != 0.0) {
            p = (double) (width - (_R1 + _R)) / (double) (taktCount) - Math.floor(temp) * countPixelOfWidth;
        } else {
            p = 0;
        }

        for (int i = 0; i < taktCount; i++) {
            for (int j = 0; j < Math.floor(temp); j++) {
                drawLine line = new drawLine(rootView.getContext());
                line.point1 = new Point((int) Math.round((j + (i * Math.floor(temp)) + 1) * countPixelOfWidth + i * p) + _R1 + _R, 0);
                line.point2 = new Point((int) Math.round((j + (i * Math.floor(temp)) + 1) * countPixelOfWidth + i * p) + _R1 + _R, height);
                relativeLayout.addView(line);
            }
        }

        countPixelOfWidth = (double) (width - (_R1 + _R)) / (double) (taktCount * size);

        switch (taktCount) {
            case 1:
                drawNumberOfTakt dn1 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (displayCount + 1)), _R1);
                dn1.point = new Point((_R1 + _R) - String.valueOf((int) (displayCount + 1)).length() * sizeOfNumber / 4, sizeOfNumber);
                dn1.pointNum = new Point((_R1 + _R) - sizeOfNumber / 4, sizeOfNumber);
                relativeLayout.addView(dn1);

                drawNumberOfTakt dn2 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (displayCount + 2)), _R1);
                dn2.point = new Point((int) Math.round(countPixelOfWidth * size - String.valueOf((int) (displayCount + 2)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn2.pointNum = new Point((int) Math.round(countPixelOfWidth * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                relativeLayout.addView(dn2);

                displayCount += 0.5;
                /*if (flag) {
                    displayCount++;
                    flag = false;
                } else {
                    flag = true;
                }*/

                break;
            case 2:
                drawNumberOfTakt dn_1 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (displayCount * (taktCount - 1) + 1)), _R1);
                dn_1.point = new Point((_R1 + _R) - String.valueOf((int) (displayCount * (taktCount - 1) + 1)).length() * sizeOfNumber / 4, sizeOfNumber);
                dn_1.pointNum = new Point((_R1 + _R) - sizeOfNumber / 4, sizeOfNumber);
                relativeLayout.addView(dn_1);

                drawNumberOfTakt dn_2 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (displayCount * (taktCount - 1) + 2)), _R1);
                //dn2.point = new Point(countPixelOfWidth * size - String.valueOf((int) (displayCount * (taktCount - 1) + 2)).length() *  sizeOfNumber / 4, sizeOfNumber);
                dn_2.point = new Point((int) Math.round(countPixelOfWidth * size - String.valueOf((int) (displayCount * (taktCount - 1) + 2)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn_2.pointNum = new Point((int) Math.round(countPixelOfWidth * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                //dn2.pointNum = new Point(countPixelOfWidth * size -  sizeOfNumber / 4, sizeOfNumber);
                relativeLayout.addView(dn_2);

                drawNumberOfTakt dn_3 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (displayCount * (taktCount - 1) + 3)), _R1);
                //dn2.point = new Point(countPixelOfWidth * size - String.valueOf((int) (displayCount * (taktCount - 1) + 2)).length() *  sizeOfNumber / 4, sizeOfNumber);
                dn_3.point = new Point((int) Math.round(countPixelOfWidth * 2 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 3)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn_3.pointNum = new Point((int) Math.round(countPixelOfWidth * 2 * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                //dn2.pointNum = new Point(countPixelOfWidth * size -  sizeOfNumber / 4, sizeOfNumber);
                relativeLayout.addView(dn_3);

                displayCount++;
                /*if (flag) {
                    displayCount += 2;
                    flag = false;
                } else {
                    flag = true;
                }*/

                break;

            case 3:
                drawNumberOfTakt dn__1 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (Math.floor(displayCount * (taktCount - 1)) + 1)), _R1);
                dn__1.point = new Point((_R1 + _R) - String.valueOf((int) (displayCount * (taktCount - 1) + 1)).length() * sizeOfNumber / 4, sizeOfNumber);
                dn__1.pointNum = new Point((_R1 + _R) - sizeOfNumber / 4, sizeOfNumber);
                relativeLayout.addView(dn__1);

                drawNumberOfTakt dn__2 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (Math.floor(displayCount * (taktCount - 1)) + 2)), _R1);
                //dn_2.point = new Point(countPixelOfWidth * size - String.valueOf((int) (displayCount * (taktCount - 1) + 2)).length() *  sizeOfNumber / 4, sizeOfNumber);
                //dn_2.pointNum = new Point(countPixelOfWidth * size -  sizeOfNumber / 4, sizeOfNumber);
                dn__2.point = new Point((int) Math.round(countPixelOfWidth * size - String.valueOf((int) (displayCount * (taktCount - 1) + 2)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn__2.pointNum = new Point((int) Math.round(countPixelOfWidth * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                relativeLayout.addView(dn__2);

                drawNumberOfTakt dn__3 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (Math.floor(displayCount * (taktCount - 1)) + 3)), _R1);
                //dn3.point = new Point(countPixelOfWidth * 2 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 3)).length() *  sizeOfNumber / 4, sizeOfNumber);
                //dn3.pointNum = new Point(countPixelOfWidth * 2 * size -  sizeOfNumber / 4, sizeOfNumber);
                dn__3.point = new Point((int) Math.round(countPixelOfWidth * 2 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 3)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn__3.pointNum = new Point((int) Math.round(countPixelOfWidth * 2 * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                relativeLayout.addView(dn__3);

                drawNumberOfTakt dn__4 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (Math.floor(displayCount * (taktCount - 1)) + 4)), _R1);
                //dn3.point = new Point(countPixelOfWidth * 2 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 3)).length() *  sizeOfNumber / 4, sizeOfNumber);
                //dn3.pointNum = new Point(countPixelOfWidth * 2 * size -  sizeOfNumber / 4, sizeOfNumber);
                dn__4.point = new Point((int) Math.round(countPixelOfWidth * 3 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 4)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn__4.pointNum = new Point((int) Math.round(countPixelOfWidth * 3 * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                relativeLayout.addView(dn__4);

                displayCount += 0.75;
                /*if (flag) {
                    displayCount += 1.5;
                    flag = false;
                } else {
                    flag = true;
                }*/

                break;

            case 4:
                drawNumberOfTakt dn___1 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (Math.floor(displayCount * (taktCount - 1)) + 1)), _R1);
                dn___1.point = new Point((_R1 + _R) - String.valueOf((int) (displayCount * (taktCount - 1) + 1)).length() * sizeOfNumber / 4, sizeOfNumber);
                dn___1.pointNum = new Point((_R1 + _R) - sizeOfNumber / 4, sizeOfNumber);
                relativeLayout.addView(dn___1);

                drawNumberOfTakt dn___2 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) Math.floor((displayCount * (taktCount - 1)) + 2)), _R1);
                //dn__2.point = new Point(countPixelOfWidth * size - String.valueOf((int) (displayCount * (taktCount - 1) + 2)).length() *  sizeOfNumber / 4, sizeOfNumber);
                //dn__2.pointNum = new Point(countPixelOfWidth * size -  sizeOfNumber / 4, sizeOfNumber);
                dn___2.point = new Point((int) Math.round(countPixelOfWidth * size - String.valueOf((int) (displayCount * (taktCount - 1) + 2)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn___2.pointNum = new Point((int) Math.round(countPixelOfWidth * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                relativeLayout.addView(dn___2);

                drawNumberOfTakt dn___3 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (Math.floor(displayCount * (taktCount - 1)) + 3)), _R1);
                //dn_3.point = new Point(countPixelOfWidth * 2 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 3)).length() *  sizeOfNumber / 4, sizeOfNumber);
                //dn_3.pointNum = new Point(countPixelOfWidth * 2 * size -  sizeOfNumber / 4, sizeOfNumber);
                dn___3.point = new Point((int) Math.round(countPixelOfWidth * 2 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 3)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn___3.pointNum = new Point((int) Math.round(countPixelOfWidth * 2 * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                relativeLayout.addView(dn___3);

                drawNumberOfTakt dn___4 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (Math.floor(displayCount * (taktCount - 1)) + 4)), _R1);
                //dn4.point = new Point(countPixelOfWidth * 3 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 4)).length() *  sizeOfNumber / 4, sizeOfNumber);
                //dn4.pointNum = new Point(countPixelOfWidth * 3 * size -  sizeOfNumber / 4, sizeOfNumber);
                dn___4.point = new Point((int) Math.round(countPixelOfWidth * 3 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 4)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn___4.pointNum = new Point((int) Math.round(countPixelOfWidth * 3 * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                relativeLayout.addView(dn___4);

                drawNumberOfTakt dn___5 = new drawNumberOfTakt(rootView.getContext(), String.valueOf((int) (Math.floor(displayCount * (taktCount - 1)) + 5)), _R1);
                //dn4.point = new Point(countPixelOfWidth * 3 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 4)).length() *  sizeOfNumber / 4, sizeOfNumber);
                //dn4.pointNum = new Point(countPixelOfWidth * 3 * size -  sizeOfNumber / 4, sizeOfNumber);
                dn___5.point = new Point((int) Math.round(countPixelOfWidth * 4 * size - String.valueOf((int) (displayCount * (taktCount - 1) + 5)).length() * sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                dn___5.pointNum = new Point((int) Math.round(countPixelOfWidth * 4 * size - sizeOfNumber / 4) + (_R1 + _R), sizeOfNumber);
                relativeLayout.addView(dn___5);

                displayCount += 0.675;

                break;
        }

        //Отрисовка горизонтальных линий
        for (int i = 0; i < rowCount - 1; i++) {
            drawLine line = new drawLine(rootView.getContext());
            line.point1 = new Point(_R1 + _R, (i + 1) * countPixelOfHeight);
            line.point2 = new Point(width, (i + 1) * countPixelOfHeight);
            line.alpha = 120;
            line.stroke = 1;
            line.color = "#FFFFFF";
            Yrows.put(i, (i + 1) * countPixelOfHeight);
            relativeLayout.addView(line);
        }
        //Toast.makeText(rootView.getContext(), String.valueOf(System.currentTimeMillis() - temp), Toast.LENGTH_SHORT).show();
        Log.e("END DRAW STAVE", String.valueOf(String.valueOf(System.currentTimeMillis())));
    }

    /**
     * Метод для отрисовки ноты на нотном стане
     * @param beat - Текущая нота
     */

    public void _drawNote(Beat beat) {
        beats.add(beat);

        int x = beat.column;
        int y = Yrows.get(beat.row);


        /**if (x - _widthNote < 0) {
         //x = 0;
         x = _widthNote;
         }*/

        switch (beat.icon) {
            /*case "rectangle":
                drawFillRectangle rectangle = new drawFillRectangle(rootView.getContext(), beat.velocity);
                rectangle.color = color;
                rectangle.point1 = new Point(x - _R, y - _R);
                rectangle.point2 = new Point(x + _R, y + _R);
                relativeLayout.addView(rectangle);
                break;*/

            case "rhombus":
                drawFillRhombus fillRhombus = new drawFillRhombus(rootView.getContext(), beat.velocity);
                fillRhombus.color = color;
                fillRhombus.point1 = new Point(x, y - _R);
                fillRhombus.point2 = new Point(x + _R - 5, y);
                fillRhombus.point3 = new Point(x, y + _R);
                fillRhombus.point4 = new Point(x - _R + 5, y);
                relativeLayout.addView(fillRhombus);
                break;

            case "triangle_for_settings":
                drawFillTriangle fillTriangle = new drawFillTriangle(rootView.getContext(), beat.velocity);
                fillTriangle.color = color;
                fillTriangle.point1 = new Point(x, y - (_widthNote + 6) / 3);
                fillTriangle.point2 = new Point(x + (_widthNote + 6) / 3, y + (_heightNote + 6) / 3);
                fillTriangle.point3 = new Point(x - (_widthNote + 6) / 3, y + (_heightNote + 6) / 3);
                relativeLayout.addView(fillTriangle);
                break;

            /**case "fill_circle":
             drawFillCircle circle = new drawFillCircle(rootView.getContext(), beat.velocity);
             circle.color = color;
             circle.point = new Point(x, y);
             circle.R = _R;
             relativeLayout.addView(circle);
             break;*/

            /**case "triangle_for_settings":
             drawFillTriangle triangle = new drawFillTriangle(rootView.getContext(), beat.velocity);
             triangle.point1 = new Point(x, y - (_widthNote + 6) / 2);
             triangle.point2 = new Point(x + (_widthNote + 6) / 2, y + (_heightNote + 6) / 2);
             triangle.point3 = new Point(x - (_widthNote + 6) / 2, y + (_heightNote + 6) / 2);
             relativeLayout.addView(triangle);
             break;*/

            /**case "rhombus":
             drawFillRhombus rhombus = new drawFillRhombus(rootView.getContext(), beat.velocity);
             rhombus.point1 = new Point(x, y - (_heightNote + 6) / 2);
             rhombus.point2 = new Point(x + (_widthNote + 6) / 2, y);
             rhombus.point3 = new Point(x, y + (_heightNote + 6) / 2);
             rhombus.point4 = new Point(x - (_widthNote + 6) / 2, y);
             relativeLayout.addView(rhombus);
             break;*/

            case "cross":
                drawCross cross = new drawCross(rootView.getContext(), beat.velocity);
                cross.color = color;
                cross.point1 = new Point(x - (int) (_R / Math.sqrt(2)), y - (int) (_R / Math.sqrt(2)));
                cross.point2 = new Point(x + (int) (_R / Math.sqrt(2)), y + (int) (_R / Math.sqrt(2)));
                cross.point3 = new Point(x - (int) (_R / Math.sqrt(2)), y + (int) (_R / Math.sqrt(2)));
                cross.point4 = new Point(x + (int) (_R / Math.sqrt(2)), y - (int) (_R / Math.sqrt(2)));
                relativeLayout.addView(cross);
                break;

            case "cross_in_circle":
                drawCrossInCircle crossInCircle = new drawCrossInCircle(rootView.getContext(), beat.velocity);
                crossInCircle.color = color;
                crossInCircle.point = new Point(x, y);
                crossInCircle._R = _R;
                crossInCircle.point1 = new Point(x - (int) (_R / Math.sqrt(2)), y - (int) (_R / Math.sqrt(2)));
                crossInCircle.point2 = new Point(x + (int) (_R / Math.sqrt(2)), y + (int) (_R / Math.sqrt(2)));
                crossInCircle.point3 = new Point(x - (int) (_R / Math.sqrt(2)), y + (int) (_R / Math.sqrt(2)));
                crossInCircle.point4 = new Point(x + (int) (_R / Math.sqrt(2)), y - (int) (_R / Math.sqrt(2)));
                relativeLayout.addView(crossInCircle);
                break;

            case "line":
                drawVerticalLine drawVerticalLine = new drawVerticalLine(rootView.getContext(), beat.velocity);
                drawVerticalLine.color = color;
                drawVerticalLine.point1 = new Point(x, y - _R);
                drawVerticalLine.point2 = new Point(x, y + _R);
                relativeLayout.addView(drawVerticalLine);
                break;
        }
    }

    /**
     * public void _drawNote(Beat beat) {
     * beats.add(beat);
     * int x = beat.column;
     * int y = Yrows.get(beat.row);
     * <p>
     * /*if (x - _widthNote < 0) {
     * x = 0;
     * //x = _widthNote;
     * }/
     * <p>
     * switch (beat.icon) {
     * case "rectangle":
     * drawFillRectangle rectangle = new drawFillRectangle(rootView.getContext(), beat.velocity);
     * rectangle.color = color;
     * rectangle.point1 = new Point(x, y - _R);
     * rectangle.point2 = new Point(x + _widthNote, y + _R);
     * relativeLayout.addView(rectangle);
     * break;
     * <p>
     * case "fill_rectangle":
     * drawFillRectangle fillRectangle = new drawFillRectangle(rootView.getContext(), beat.velocity);
     * fillRectangle.color = color;
     * fillRectangle.point1 = new Point(x, y - _R);
     * fillRectangle.point2 = new Point(x + _widthNote, y + _R);
     * relativeLayout.addView(fillRectangle);
     * break;
     * <p>
     * case "rhombus":
     * drawRhombus rhombus = new drawRhombus(rootView.getContext(), beat.velocity);
     * rhombus.color = color;
     * rhombus.point1 = new Point(x, y - _R);
     * rhombus.point2 = new Point(x + _R, y);
     * rhombus.point3 = new Point(x, y + _R);
     * rhombus.point4 = new Point(x - _R, y);
     * relativeLayout.addView(rhombus);
     * break;
     * <p>
     * case "fill_rhombus":
     * drawFillRhombus fillRhombus = new drawFillRhombus(rootView.getContext(), beat.velocity);
     * fillRhombus.color = color;
     * fillRhombus.point1 = new Point(x, y - _R);
     * fillRhombus.point2 = new Point(x + _R, y);
     * fillRhombus.point3 = new Point(x, y + _R);
     * fillRhombus.point4 = new Point(x - _R, y);
     * relativeLayout.addView(fillRhombus);
     * break;
     * <p>
     * case "triangle_for_settings":
     * drawTriangle triangle = new drawTriangle(rootView.getContext(), beat.velocity);
     * triangle.color = color;
     * triangle.point1 = new Point(x, y - (_widthNote + 6) / 3);
     * triangle.point2 = new Point(x + (_widthNote + 6) / 3, y + (_heightNote + 6) / 3);
     * triangle.point3 = new Point(x - (_widthNote + 6) / 3, y + (_heightNote + 6) / 3);
     * relativeLayout.addView(triangle);
     * break;
     * <p>
     * case "fill_triangle_for_settings":
     * drawFillTriangle fillTriangle = new drawFillTriangle(rootView.getContext(), beat.velocity);
     * fillTriangle.color = color;
     * fillTriangle.point1 = new Point(x, y - (_widthNote + 6) / 3);
     * fillTriangle.point2 = new Point(x + (_widthNote + 6) / 3, y + (_heightNote + 6) / 3);
     * fillTriangle.point3 = new Point(x - (_widthNote + 6) / 3, y + (_heightNote + 6) / 3);
     * relativeLayout.addView(fillTriangle);
     * break;
     * <p>
     * case "circle":
     * drawCircle circle = new drawCircle(rootView.getContext(), beat.velocity);
     * circle.color = color;
     * circle.point = new Point(x, y);
     * circle.R = _R;
     * relativeLayout.addView(circle);
     * break;
     * <p>
     * case "fill_circle":
     * drawFillCircle fillCircle = new drawFillCircle(rootView.getContext(), beat.velocity);
     * fillCircle.color = color;
     * fillCircle.point = new Point(x, y);
     * fillCircle.R = _R;
     * relativeLayout.addView(fillCircle);
     * break;
     * <p>
     * case "cross":
     * drawCross cross = new drawCross(rootView.getContext(), beat.velocity);
     * cross.color = color;
     * cross.point1 = new Point(x - (int) (_R / Math.sqrt(2)), y - (int) (_R / Math.sqrt(2)));
     * cross.point2 = new Point(x + (int) (_R / Math.sqrt(2)), y + (int) (_R / Math.sqrt(2)));
     * cross.point3 = new Point(x - (int) (_R / Math.sqrt(2)), y + (int) (_R / Math.sqrt(2)));
     * cross.point4 = new Point(x + (int) (_R / Math.sqrt(2)), y - (int) (_R / Math.sqrt(2)));
     * relativeLayout.addView(cross);
     * break;
     * <p>
     * case "cross_in_circle":
     * drawCrossInCircle crossInCircle = new drawCrossInCircle(rootView.getContext(), beat.velocity);
     * crossInCircle.color = color;
     * crossInCircle.point = new Point(x, y);
     * crossInCircle._R = _R;
     * crossInCircle.point1 = new Point(x - (int) (_R / Math.sqrt(2)), y - (int) (_R / Math.sqrt(2)));
     * crossInCircle.point2 = new Point(x + (int) (_R / Math.sqrt(2)), y + (int) (_R / Math.sqrt(2)));
     * crossInCircle.point3 = new Point(x - (int) (_R / Math.sqrt(2)), y + (int) (_R / Math.sqrt(2)));
     * crossInCircle.point4 = new Point(x + (int) (_R / Math.sqrt(2)), y - (int) (_R / Math.sqrt(2)));
     * relativeLayout.addView(crossInCircle);
     * break;
     * <p>
     * case "line":
     * drawVerticalLine drawVerticalLine = new drawVerticalLine(rootView.getContext(), beat.velocity);
     * drawVerticalLine.color = color;
     * drawVerticalLine.point1 = new Point(x, y - _R);
     * drawVerticalLine.point2 = new Point(x, y + _R);
     * relativeLayout.addView(drawVerticalLine);
     * break;
     * }
     * }
     */

    /**
     * Метод для обновления нотного стана
     *
     * @param size - "Размер" внутри такта
     * @param isPlay - Флаг, если обнулили таймер
     */

    public void _clear(double size, boolean isPlay) {
        this.size = size;
        Log.e("START CLEAR STAVE", String.valueOf(System.currentTimeMillis()));
        relativeLayout.removeAllViews();
        Log.e("END CLEAR STAVE", String.valueOf(System.currentTimeMillis()));
        _stave(size);
        if (!isPlay) {
            beats.clear();
            beats4DrawStave.clear();
        }
    }

    /**
     * Метод для отрисовки полного нотного стана
     */

    public void _drawAllStave() {
        /*beats.add(new Beat(0, 0, 1, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 2, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 3, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 4, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 5, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 6, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 7, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 8, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 9, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 10, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 11, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 12, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 13, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 14, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 15, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 16, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 17, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 18, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 19, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 20, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 21, "", "cross_in_circle", 100));
        beats.add(new Beat(0, 0, 22, "", "cross_in_circle", 100));*/

        if (beats.size() != 0) {
            relativeLayout.removeAllViews();

            HorizontalScrollView hsv = new HorizontalScrollView(rootView.getContext());
            hsv.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

            addBeat();

            //Отрисовка стана
            drawScroll ds = new drawScroll(rootView.getContext(), width, height, size, beats4DrawStave, _heightNote, _widthNote, _R, Yrows, _R1, sizeOfNumber);

            hsv.addView(ds);
            relativeLayout.addView(hsv);
        }
    }

    /**
     * Метод для добавления ноты к общему списку нот
     */

    public void addBeat() {
        for (int i = 0; i < beats.size(); i++) {
            Beat beat = new Beat();
            beat.row = beats.get(i).row;
            //beat.column = width * (beats.get(i).displayNumber - 1) + beats.get(i).column/* + 4 * (beats.get(i).displayNumber - 1)*/;
            beat.column = (width - (_R + _R1)) * (beats.get(i).displayNumber - 1) + beats.get(i).column;
            //beat.column = beats.get(i).displayNumber == 1 ? width * (beats.get(i).displayNumber - 1) + beats.get(i).column + (beats.get(i).displayNumber) * _R : width * (beats.get(i).displayNumber - 1) + beats.get(i).column - (beats.get(i).displayNumber) * 2 * _R + 3 * _R;
            beat.displayNumber = beats.get(i).displayNumber;
            beat.icon = beats.get(i).icon;
            beat.note = beats.get(i).note;
            beat.velocity = beats.get(i).velocity;
            beats4DrawStave.add(beat);
        }
    }

    /**
     * Геттер ширины нотного стана
     * @return Ширина нотного стана
     */

    public int getWidth() {
        return width;
    }

    /**
     * Геттер "Размера" внутри такта
     * @return "Размер" внутри такта
     */

    public double getSize() {
        return size;
    }

    /**
     * Сеттер "Размера" внутри такта
     * @param beats - "Размер" внутри такта
     */

    public void setSize(double beats) {
        this.size = beats;
    }

    /**
     * Метод для отрисовки линии метронома
     * @param width - Ширина нотного стана
     */

    public void drawMetronomeLine(int width) {
        final MetronomeLine metronomeLine = new MetronomeLine(rootView.getContext());

        width += _R1;

        metronomeLine.point1 = new Point(width, 0);
        metronomeLine.point2 = new Point(width, this.height);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                relativeLayout.removeView(metronomeLine);
            }
        }, 5);

        relativeLayout.addView(metronomeLine);
    }

    /**
     * Метод для задания параметров нот и параметров нотного стана
     * @param context - Контекст
     */

    private void setSizeOfBeats(Context context) {
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Log.d("WIDTH= ", String.valueOf(width));
        Log.d("HEIGHT= ", String.valueOf(height));

        _R = width / STAVE_SCALE;
        _widthNote = _heightNote = _R * 2;
        _R1 = sizeOfNumber = _R + 3;

        Log.d("WIDTH BEAT= ", String.valueOf(_widthNote));
        Log.d("HEIGHT BEAT= ", String.valueOf(_heightNote));
        Log.d("R BEAT= ", String.valueOf(_R));
        Log.d("SIZE OF NUMBER TAKT= ", String.valueOf(sizeOfNumber));
        Log.d("R1= ", String.valueOf(_R1));
    }
}