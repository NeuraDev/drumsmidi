package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Класс необходимый для отрисовки ноты Линия
 *
 * Created by kmoaz on 20.10.2016.
 */

public class drawLine extends View {

    /**
     * Поле - Тэг
     */

    private static final String TAG = "DRAWING";

    /**
     * Поле - Параметр для отрисовки ноты
     */

    private Paint paint = new Paint();

    /**
     * Поле - Первая точка для отрисовки ноты
     */

    Point point1;

    /**
     * Поле - Вторая точка для отрисовки ноты
     */

    Point point2;

    /**
     * Поле - Толщина бордера
     */

    public int stroke = 3;

    /**
     * Поле - Прозрачность ноты
     */

    public int alpha = 60;

    /**
     * Поле - Цвет ноты
     */

    public String color = "#FFFFFF";

    /**
     * Конструктор
     *
     * @param context - Контекст
     */

    public drawLine(Context context) {
        super(context);
    }

    /**
     * Метод отрисовки ноты
     * @param canvas
     */

    public void onDraw (Canvas canvas) {
        paint.setColor(Color.parseColor(color));
        //if (alpha != null)
        paint.setAlpha(alpha);
        paint.setStrokeWidth(stroke);
        canvas.drawLine(point1.x, point1.y, point2.x, point2.y, paint);
        //Log.e(TAG, "POINT X = " + String.valueOf(point1.x));
    }
}
