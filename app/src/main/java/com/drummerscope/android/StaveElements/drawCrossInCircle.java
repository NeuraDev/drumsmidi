package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Класс для отрисовки ноты Крест в круге
 *
 * Created by kmoaz on 16.01.2017.
 */

public class drawCrossInCircle extends View {

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Поле - Параметр для отрисовки ноты
     */

    private Paint paint = new Paint();

    /**
     * Поле - Параметр для определения прозрачности ноты
     */

    private int velocity;

    /**
     * Поле - Параметр для прозрачности ноты
     */

    private int transparency;

    /**
     * Поле - Первая точка для отрисовки ноты
     */

    public Point point;

    /**
     * Поле - Вторая точка для отрисовки ноты
     */

    public Point point1;

    /**
     * Поле - Третья точка для отрисовки ноты
     */

    public Point point2;

    /**
     * Поле - Четвёртая точка для отрисовки ноты
     */

    public Point point3;

    /**
     * Поле - Пятая точка для отрисовки ноты
     */

    public Point point4;

    /**
     * Поле - Радиус окружности ноты
     */

    public int _R;

    /**
     * Поле - Цвет ноты
     */

    public int color = -65536;

    /**
     * Конструктор
     *
     * @param context - Контекст
     * @param velocity - Параметр для определения прозрачности ноты
     */

    public drawCrossInCircle(Context context, int velocity) {
        super(context);
        mContext = context;
        this.velocity = velocity;
        TransparencyCalc velocityCalc = new TransparencyCalc();
        this.transparency = velocityCalc.calc(velocity);
        //this.transparency = (int)((double)velocity / 128.0 * 255.0);
    }

    /**
     *
     * @param canvas
     */

    public void onDraw (Canvas canvas) {
        paint.setColor(color);
        paint.setStrokeWidth(3);
        paint.setAlpha(transparency);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(point.x, point.y, _R, paint);

        canvas.drawLine(point1.x, point1.y, point2.x, point2.y, paint);
        canvas.drawLine(point3.x, point3.y, point4.x, point4.y, paint);
    }
}
