package com.drummerscope.android.StaveElements;

/**
 * Класс необходимый для вычисления прозрачности объекта нотного стана
 *
 * Created by evgen on 27.02.2017.
 */

public class TransparencyCalc {

    /**
     * Поле - Параметр для вычисления прозрачности объекта нотного стана
     */

    private int transparency;

    /**
     * Конструктор
     */

    public TransparencyCalc() {

    }

    /**
     * Метод для вычисления прозрачности объекта нотного стана
     * @param velocity - Параметр для определения прозрачности ноты
     * @return Значение параметра прозрачности
     */

    public int calc(double velocity) {
        if (velocity < 36)
            transparency = 120;
        if (velocity > 35 && velocity < 66)
            transparency = 160;
        if (velocity > 65 && velocity < 96 )
            transparency = 200;
        if (velocity > 95 && velocity <= 128)
            transparency = 255;
        return transparency;
    }
}
