package com.drummerscope.android.StaveElements;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Класс необходимый для отрисовки ноты Квадрат
 *
 * Created by kmoaz on 23.02.2017.
 */

public class drawRectangle extends View {

    /**
     * Поле - Параметр для отрисовки ноты
     */

    Paint paint = new Paint();

    /**
     * Поле - Первая точка для отрисовки ноты
     */

    public Point point1;

    /**
     * Поле - Вторая точка для отрисовки ноты
     */

    public Point point2;

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Поле - Параметр для определения прозрачности ноты
     */

    private int velocity;

    /**
     * Поле - Параметр для прозрачности ноты
     */

    private int transparency;

    /**
     * Поле - Цвет ноты
     */

    public int color = -65536;

    /**
     * Конструктор
     * @param context
     * @param velocity
     */

    public drawRectangle(Context context, int velocity) {
        super(context);
        mContext = context;
        this.velocity = velocity;
        this.transparency = (int)((double)velocity / 128.0 * 255.0);
    }

    /**
     * Метод отрисовки ноты
     * @param canvas
     */

    @Override
    public void onDraw (Canvas canvas) {
        paint.setColor(color);
        paint.setStrokeWidth(3);
        paint.setAlpha(transparency);
        paint.setStyle(Paint.Style.STROKE);

        canvas.drawRect(point1.x, point1.y, point2.x, point2.y, paint);
    }
}
