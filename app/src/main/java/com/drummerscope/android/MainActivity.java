package com.drummerscope.android;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.drummerscope.android.Fragments.AboutUsFragment;
import com.drummerscope.android.Fragments.DonateFragment;
import com.drummerscope.android.Fragments.FeedbackFragment;
import com.drummerscope.android.Fragments.GlobalSettingsFragment;
import com.drummerscope.android.Fragments.JournalFragment;
import com.drummerscope.android.Fragments.MainFragment;
import com.drummerscope.android.Fragments.NotesSettingsFragment;
import com.drummerscope.android.Fragments.SettingsFragment;
import com.drummerscope.android.ShareDialogMenu.ChooserArrayAdapter;
import com.drummerscope.android.StaveElements.Beat;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /**
     * Поле - Контекст
     */

    private Context mContext;

    /**
     * Поле - Объект меню тул бара
     */

    public Menu menu;

    /**
     * Поле -
     */

    private Toolbar mToolbar;

    /**
     * Поле -
     */

    public boolean notesExist = false;

    /**
     * Поле -
     */

    private boolean itemInDrawerSelected = false;

    /**
     * Поле - Ноты л=для отрисовки на нотном стане
     */

    public List<Beat> beatsToDraw;

    /**
     * Поле -
     */

    public String timeSignatureValue1;

    /**
     * Поле -
     */

    public String timeSignatureValue2;

    /**
     * Поле - Тэг
     */


    private static final String TAG = "MainActivity";

    //private static UsbMidiDriver usbMidiDriver = null;

    //private MetronomeController metronomeController;

    /**
     * Поле - Список тэгов фрагментов
     */

    String TAG_ALL;

    /**
     * Поле - Объект для работы с файлами
     */

    private ReadWriteJSONSettings readWriteJSONSettings;

    /**
     * Поле - Список заголовков экранов
     */

    private String[] mScreenTitles = {"Main", "Metronome", "Settings", "Note", "Journal", "Donate", "Feedback", "About us"};

    /**
     * Поле - Боковое меню
     */

    private DrawerLayout mDrawerLayout;

    /**
     * Поле - Список элементов бокового меню
     */

    private ListView mDrawerList;

    /**
     * Поле - Бургер
     */

    private ActionBarDrawerToggle mDrawerToggle;
    //private CharSequence mDrawerTitle;
    //private CharSequence mTitle;

    /**
     * Поле - Текствью названия экрана
     */

    private TextView mTitle;

    /**
     * Поле - Мэнэджер фрагментов для навгации по ним
     */

    private FragmentManager fragmentManager;

    /**
     * Поле - Текущий фрагмент экрана
     */

    private Fragment pendingFragment;

    /**
     * Поле - Номер текущего фрагмента в стэке фрагментов
     */

    public int intentIndex;

    /**
     * Поле - Является ли текущий фрагмент, фрагментом журнала
     */

    private boolean isJournal = false;

    /**
     * Поле - Является ли текущий фрагмент, фрагментом метронома
     */

    public boolean isMetronome = false;

    /**
     * Поле - Значение БПМ для метронома (100 - дефолтное)
     */

    public int bpm = 100;

    /**
     * Поле - Не используется
     */

    private FrameLayout container;

    /**
     * Поле -
     */

    public boolean isHeadsetPluggedIn;

    /**
     * Поле - Не используется
     */

    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    /**
     * Поле - Список необходимых разрешений для корректной работы приложения
     */

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.MODIFY_AUDIO_SETTINGS
    };

    /**
     * Поле - Открыто ли боковое меню
     */

    private boolean drawerIsOpened = false;

    /**
     * Поле -
     */

    private boolean animated = false;

    /**
     * Метод создания активити
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN);

        }
        /**if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
         decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
         | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
         | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
         | View.SYSTEM_UI_FLAG_FULLSCREEN
         | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
         }*/

        setContentView(R.layout.main);

        LogTextFile.getInstance().setFileName("log_file_" + Calendar.getInstance().getTime().toString());

        mContext = this;

        timeSignatureValue1 = "4";
        timeSignatureValue2 = "4";

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        readWriteJSONSettings = new ReadWriteJSONSettings(mContext);

        final AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        isHeadsetPluggedIn = am.isWiredHeadsetOn();

        container = (FrameLayout) findViewById(R.id.container);
        fragmentManager = getFragmentManager();
        pendingFragment = MainFragment.newInstance();


        /**View decorView = getWindow().getDecorView();
         // Hide the status bar.
         int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
         decorView.setSystemUiVisibility(uiOptions);
         // Remember that you should never show the action bar if the
         // status bar is hidden, so hide that too if necessary.
         //ActionBar actionBar = mainActivity.getActionBar();
         //actionBar.hide();*/


        checkAllFiles(mContext);

        if (savedInstanceState == null) {
            addFragment(pendingFragment, TAG);
        }

        if (canMakeSmores())
            requestPermissions(PERMISSIONS_STORAGE, 200);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_menu);
        mToolbar.invalidate();
        setSupportActionBar(mToolbar);

        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getTitle());
        //mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        int[] drawablesid = {R.mipmap.ic_main_active, R.mipmap.ic_metromone_active, R.mipmap.ic_setting_active,
                R.mipmap.ic_note_active, R.mipmap.ic_diary_active, R.mipmap.ic_about_us_active,
                R.mipmap.ic_feedback_active, R.mipmap.ic_about_us_active};
        mDrawerList.setAdapter(new CustomAdapter(this, mScreenTitles, drawablesid));

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");

        //Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.ic_menu, getTheme());
        //Toolbar mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        //mToolbar.setNavigationIcon(R.drawable.ic_menu);

        /*mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });*/


        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(
                this, /* host Activity */
                mDrawerLayout, /* DrawerLayout object */
                R.drawable.ic_menu, /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open, /* "open drawer" description */
                R.string.drawer_close /* "close drawer" description */

        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                drawerIsOpened = false;
                if (!animated)
                    updateStateOfHamburger();
                mToolbar.setNavigationIcon(R.drawable.ic_menu);
                mToolbar.invalidate();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                // Insert the fragment by replacing any existing fragment
                if (pendingFragment != null) {
                    if (itemInDrawerSelected) {
                        fragmentTransaction.replace(R.id.container, pendingFragment);
                        int pos = pendingFragment.getArguments().getInt("position");
                        setTitle(mScreenTitles[pos]);
                        fragmentTransaction.addToBackStack(mScreenTitles[pos]);
                        fragmentTransaction.commit();
                        intentIndex = fragmentManager.getBackStackEntryCount();
                    }
                }

                animated = false;
                itemInDrawerSelected = false;
                //getSupportActionBar().setTitle(mTitle.getText());
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //mainFragment.onPause();
                drawerIsOpened = true;
                if (!animated)
                    updateStateOfHamburger();
                mToolbar.setNavigationIcon(R.drawable.md_nav_back);
                mToolbar.invalidate();
                Log.e(TAG, "OPENED");

                //mainFragment.onDrawerOpened();
                if (pendingFragment != null) {
                    if (pendingFragment instanceof MainFragment) {
                        Log.e(TAG, "PENDING IS NOT NULL");
                        ((MainFragment) pendingFragment).onDrawerOpened();
                    }
                    /*int pos = pendingFragment.getArguments().getInt("position");
                    if ((mScreenTitles[pos]).equals("Main")) {
                        Log.e(TAG, "PENDING IS NOT NULL");
                        MainFragment mainFragment1 = (MainFragment) pendingFragment;
                        mainFragment1.onDrawerOpened();
                    }*/
                } /*else {
                    //getSupportActionBar().setTitle(mDrawerTitle);
                    mainFragment.onDrawerOpened();
                }*/
                animated = false;
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        //mDrawerToggle.setDrawerIndicatorEnabled(true);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        mDrawerToggle.syncState();


        //mDrawerToggle.setDrawerIndicatorEnabled(false);
        //mToolbar.setNavigationIcon(R.drawable.ic_menu);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateStateOfHamburger();
                animated = true;

                if (drawerIsOpened) {
                    mToolbar.setNavigationIcon(R.drawable.ic_menu);
                    mToolbar.invalidate();
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else {
                    mToolbar.setNavigationIcon(R.drawable.md_nav_back);
                    mToolbar.invalidate();
                    mDrawerLayout.openDrawer(Gravity.START);
                }
            }
        });

        //mDrawerToggle.setHomeAsUpIndicator(drawable);
        Toast.makeText(getApplicationContext(), "This is 17 build version", Toast.LENGTH_SHORT).show();
    }

    /**
     * Метод добавления фрагментов бокового меню в стэк
     * @param fragment - Добавление и инициализация фрагментов в стэк
     * @param tag - Тэг фрагмента
     */

    private void addFragment(Fragment fragment, String tag) {
        getFragmentManager().beginTransaction()
                .add(R.id.container, fragment)
                .commit();
    }

    /**
     * Обновление состояние бургера бокового меню с анимацией
     */

    private void updateStateOfHamburger() {
        View view = getToolbarNavigationIcon(mToolbar);
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(view, "rotation", view.getRotation() + 180);
        animator.start();
    }

    /**
     * Получение иконки бургера бокового меню
     * @param toolbar - Тулбар
     * @return Вью тулбара
     */

    public static View getToolbarNavigationIcon(Toolbar toolbar) {
        //check if contentDescription previously was set
        boolean hadContentDescription = TextUtils.isEmpty(toolbar.getNavigationContentDescription());
        String contentDescription = !hadContentDescription ? (String) toolbar.getNavigationContentDescription() : "navigationIcon";
        toolbar.setNavigationContentDescription(contentDescription);
        ArrayList<View> potentialViews = new ArrayList<View>();
        //find the view based on it's content description, set programatically or with android:contentDescription
        toolbar.findViewsWithText(potentialViews, contentDescription, View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        //Nav icon is always instantiated at this point because calling setNavigationContentDescription ensures its existence
        View navIcon = null;
        if (potentialViews.size() > 0) {
            navIcon = potentialViews.get(0); //navigation icon is ImageButton
        }
        //Clear content description if not previously present
        if (hadContentDescription)
            toolbar.setNavigationContentDescription(null);
        return navIcon;
    }

    /**
     * Метод обработчик кнопки назад
     */

    @Override
    public void onBackPressed() {
        //mToolbar.setNavigationIcon(R.drawable.ic_menu);
        //mToolbar.invalidate();
        if (!drawerIsOpened) {
            getFragmentManager().popBackStack();
            int index = getFragmentManager().getBackStackEntryCount() - 1;
            String tag1 = "";
            try {
                tag1 = getFragmentManager().getBackStackEntryAt(index).getName();
            } catch (Exception e) {
                finish();
            }
            pendingFragment = getFragmentManager().findFragmentByTag(tag1);
        }
        if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
            //updateStateOfHamburger();
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            //super.onBackPressed();

            if (intentIndex > 0) {
                intentIndex--;
                FragmentManager.BackStackEntry backEntry = getFragmentManager().getBackStackEntryAt(intentIndex);
                String tag = backEntry.getName();
                setTitle(tag);
                isJournal = tag.equals("Journal");
                isMetronome = tag.equals("Metronome");

            } else {
                intentIndex--;
                if (intentIndex == -1) {
                    setTitle(mScreenTitles[0]);
                    //finish();
                }
                isJournal = false;
                isMetronome = false;
            }
            menu.findItem(R.id.plus).setVisible(isJournal);
            menu.findItem(R.id.viewAllNotes).setVisible(isJournal);
            menu.findItem(R.id.question).setVisible(isMetronome);

        }


    }

    /**
     *
     */


    @Override
    protected void onResume() {
        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN);

        }
        /**if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
         decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
         | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
         | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
         | View.SYSTEM_UI_FLAG_FULLSCREEN
         | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
         }*/
        super.onResume();
    }

    /**
     *
     */

    @Override
    protected void onPause() {
        super.onPause();
        // If the screen is off then the device has been locked
        PowerManager powerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            isScreenOn = powerManager.isInteractive();
        } else {
            isScreenOn = powerManager.isScreenOn();
        }

        if (!isScreenOn) {
            readWriteJSONSettings.write(String.valueOf(bpm), "bpm");
        }
    }

    /**
     *
     */

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Метод инициализации меню тулбара
     * @param menu - Меню тулбара
     * @return
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu;
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        //getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        inflater.inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    /**
     * Подготовка меню тулбара (цвет, размер и прочее)
     * @param menu - Меню тулбара
     * @return
     */

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        Drawable plus = menu.findItem(R.id.plus).getIcon();
        plus.mutate();
        plus.setColorFilter(Color.parseColor("#F4F4F4"), PorterDuff.Mode.SRC_ATOP);
        plus.invalidateSelf();
        menu.findItem(R.id.plus).setVisible(isJournal);

        Drawable viewAllNotes = menu.findItem(R.id.viewAllNotes).getIcon();
        viewAllNotes.mutate();
        viewAllNotes.setColorFilter(Color.parseColor("#F4F4F4"), PorterDuff.Mode.SRC_ATOP);
        viewAllNotes.invalidateSelf();
        menu.findItem(R.id.viewAllNotes).setVisible(isJournal);

        Drawable question = menu.findItem(R.id.question).getIcon();
        question.mutate();
        question.setColorFilter(Color.parseColor("#F4F4F4"), PorterDuff.Mode.SRC_ATOP);
        question.invalidateSelf();
        menu.findItem(R.id.question).setVisible(isMetronome);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Метод для выбора элемента меню тулбара
     * @param item - Выбранный элемент тулбара
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    /**
     * The click listener for ListView in the navigation drawer
     */

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
            drawerIsOpened = false;
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    /**
     * Обработка события по клике на элемент бокового меню
     * @param position - Номер выбранного элемента бокового меню
     */

    private void selectItem(int position) {
        // Update the main content by replacing fragments
        Fragment fragment = null;

        itemInDrawerSelected = true;

        Intent intent = null;
        updateStateOfHamburger();
        animated = true;
        mToolbar.setNavigationIcon(R.drawable.ic_menu);
        mToolbar.invalidate();

        switch (position) {
            case 0:
                fragment = MainFragment.newInstance();
                isJournal = false;
                isMetronome = false;
                break;

            case 1:
                fragment = new SettingsFragment();
                isJournal = false;
                isMetronome = true;
                break;

            case 2:
                fragment = new GlobalSettingsFragment();
                isJournal = false;
                isMetronome = false;
                break;

            case 3:
                fragment = new NotesSettingsFragment();
                isJournal = false;
                isMetronome = false;
                break;

            case 4:
                fragment = new JournalFragment();
                isJournal = true;
                isMetronome = false;
                break;

            case 5:
                fragment = new DonateFragment();
                isJournal = false;
                isMetronome = false;
                break;

            case 6:
                fragment = new FeedbackFragment();
                isJournal = false;
                isMetronome = false;
                break;

            case 7:
                fragment = new AboutUsFragment();
                isJournal = false;
                isMetronome = false;
                break;

            case 8:
                fragment = new DonateFragment();
                isJournal = false;
                isMetronome = false;
                break;
        }

        TAG_ALL = "FRAGMENT_" + String.valueOf(position);
        pendingFragment = fragment;


        // Create a new fragment and specify the planet to show based on position
        Bundle args = new Bundle();
        args.putInt("position", position);
        if (fragment != null)
            fragment.setArguments(args);

        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mScreenTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);

    }

    /**
     * Задание значения тайтла тулбара
     */

    @Override
    public void setTitle(CharSequence title) {
        mTitle.setText(title);
        //getSupportActionBar().setTitle(mTitle.getText());
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Проверка на необходимую версию андройда
     * @return
     */

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * Метод открытия ShareDialogMenu
     */

    public void shareDialog(View view) {
        final List<String> packages = new ArrayList<String>();
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        final List<ResolveInfo> resInfosNew = new ArrayList<ResolveInfo>();
        final List<ResolveInfo> resInfos = getPackageManager().queryIntentActivities(shareIntent, 0);

        resInfosNew.addAll(resInfos);
        if (!resInfos.isEmpty()) {
            System.out.println("Have package");
            int count = 0;
            for (ResolveInfo resInfo : resInfos) {
                String packageName = resInfo.activityInfo.packageName;
                if (packageName.contains("com.facebook.katana")) {
                    resInfosNew.remove(count);
                } else
                    packages.add(packageName);
                count++;
            }
        }

        if (packages.size() > 1) {
            ArrayAdapter<String> adapter = new ChooserArrayAdapter(this, android.R.layout.select_dialog_item, android.R.id.text1, packages);

            new AlertDialog.Builder(this)
                    //.setTitle(R.string.share_offer_with)
                    .setAdapter(adapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            invokeApplication(packages.get(item), resInfosNew.get(item));
                        }
                    })
                    .show();
        } else if (packages.size() == 1) {
            invokeApplication(packages.get(0), resInfos.get(0));
        }
    }

    /**
     * Метод для создания активити для шаринга
     */

    private void invokeApplication(String packageName, ResolveInfo resolveInfo) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Something.");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Nothing.");
        intent.setPackage(packageName);
        startActivity(intent);
    }

    /**
     * Метод для возвращения на главный экран с метрономмом
     */

    public void backToMain(View view) {
        pendingFragment = MainFragment.newInstance();
        replaceFragment(pendingFragment, TAG);
        setTitle("Main");
        menu.findItem(R.id.question).setVisible(false);
        isMetronome = false;
        intentIndex = fragmentManager.getBackStackEntryCount();
    }

    /**
     * Метод для переключение на новый экран
     * @param fragment - Выбранный фрагмент
     * @param tag - Тэг фрагмента
     */

    private void replaceFragment(Fragment fragment, String tag) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(tag)
                .commit();
    }

    /**
     * Метод для проверки на пустоту в файле и задание дефолтного значения
     * @param context - Контекст
     */

    private void checkAllFiles(Context context) {
        ReadWriteJSONSettings readWriteJSONSettings = new ReadWriteJSONSettings(context);

        if (readWriteJSONSettings.read("metronomeMarkerSwitch").isEmpty()) {
            readWriteJSONSettings.write("on", "metronomeMarkerSwitch");
        }

        if (readWriteJSONSettings.read("bps").isEmpty()) {
            readWriteJSONSettings.write("100", "bps");
        }

        if (readWriteJSONSettings.read("timer").isEmpty()) {
            readWriteJSONSettings.write("100", "timer");
        }

        if (readWriteJSONSettings.read("dateformat").isEmpty()) {
            readWriteJSONSettings.write("MM.DD.YYYY", "dateformat");
        }

        if (readWriteJSONSettings.read("taktcount").isEmpty()) {
            readWriteJSONSettings.write("2", "taktcount");
        }

        if (readWriteJSONSettings.read("tracktitle").isEmpty()) {
            readWriteJSONSettings.write("CR HH SD TT TT KD", "tracktitle");
        }

        if (readWriteJSONSettings.read("notecolor").isEmpty()) {
            readWriteJSONSettings.write("-65536", "notecolor");
        }

        if (readWriteJSONSettings.read("devidetakt").isEmpty()) {
            readWriteJSONSettings.write("4", "devidetakt");
        }

        if (readWriteJSONSettings.read("dividetriplet").isEmpty()) {
            readWriteJSONSettings.write(String.valueOf(false), "dividetriplet");
        }

        if (readWriteJSONSettings.read("size").isEmpty()) {
            readWriteJSONSettings.write("4", "size");
        }

        readWriteJSONSettings.write("4", "notelength");
    }

}
