package com.drummerscope.android.MidiDevice;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.drummerscope.android.Dialogs.CreateNote.CreateNoteDialog;
import com.drummerscope.android.Dialogs.CreateNote.SoundCheckDialog;
import com.drummerscope.android.LogTextFile;
import com.drummerscope.android.Metronome.MetronomeController;
import com.drummerscope.android.StaveElements.Beat;
import com.drummerscope.android.StaveElements.StaveFragment;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;
import com.google.firebase.crash.FirebaseCrash;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import jp.kshoji.driver.midi.device.MidiInputDevice;
import jp.kshoji.driver.midi.device.MidiOutputDevice;
import jp.kshoji.driver.midi.util.UsbMidiDriver;

/**
 * Класс, отвечающий за работу с MIDI-устройством
 */
public class MidiController {

    private static final String TAG = "MIDI CONTROLLER";

    /**
     * Массив с нотами
     */
    private String[] Tones = new String[]{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    private Context mContext;
    /**
     * Фрагмент с нотным станом
     */
    private StaveFragment staveFragment;
    /**
     * @see MetronomeController
     */
    private MetronomeController metronomeController;
    /**
     * JSON-объекты для сохранения информации о дорожках и принадлежащих им нотам
     */
    private JSONObject wholeFileTracksAndNotes, wholeFileTracksAndImages;
    /**
     * Объект для чтения/записи json-файлов
     */
    private ReadWriteJSONSettings readWriteJSONSettings;
    // stores connected UsbDevice noteInfo
    /**
     * Множество присоединенных девайсов
     */
    private final Set<UsbDevice> usbDevices = new HashSet<UsbDevice>();
    /**
     * @see UsbMidiDriver
     */
    public UsbMidiDriver usbMidiDriver = null;

    /**
     * Handler для обработки сигнала от MIDI-устройства
     * Рассчитывает координату для ноты на экране и вызывает метод отрисовки ноты на стане.
     */
    public final Handler uiThreadEventHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Log.e(TAG, "HANDLER");
            try {
                wholeFileTracksAndImages = new JSONObject(readJSON("images"));
                wholeFileTracksAndNotes = new JSONObject(readJSON("notes"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            if ("note on".equals(msg.obj)) {
                int noteTrack;
                String noteType;
                String noteName = Tones[msg.arg1 % 12];
                String octave = String.valueOf(msg.arg1 / 12);
                String message = noteName + octave;
                int velocity = msg.arg2;


                if (SoundCheckDialog.getSoundCheck()) {
                    try {
                        String msge = noteName + octave;
                        /* Set message for sound check process */
                        if (!SoundCheckDialog.dialogOpened)
                            SoundCheckDialog.setNoteInfoTextView(msge);
                        Log.e(TAG, msge);
                    } catch (Exception e) {
                        FirebaseCrash.report(e);
                    }
                }

                if (CreateNoteDialog.isNoteCreating) {
                    String msge = noteName + octave;
                    Log.e(TAG, msge);
                    if (!CreateNoteDialog.existingNoteDialogOpened)
                        try {
                            CreateNoteDialog.setNote(msge);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }

                Log.e(TAG, String.valueOf(metronomeController.isPlay()));
                Log.e(TAG, "VELOCITY = " + String.valueOf(velocity));

                if (metronomeController.isPlay() && velocity >= 0 &&
                        !metronomeController.metronome.getBarsBefore() &&
                        !metronomeController.metronome.isBeginningSilence()) {
                    if (wholeFileTracksAndNotes != null) {

                        for (int i = 0; i < 6; i++) {
                            try {
                                JSONObject noteNamesTrackI = wholeFileTracksAndNotes.getJSONObject("track" + String.valueOf(i));
                                JSONObject noteImagesTrackI = wholeFileTracksAndImages.getJSONObject("track" + String.valueOf(i));

                                for (int j = 0; j < noteNamesTrackI.length(); j++) {
                                    String note = noteNamesTrackI.getString("note" + String.valueOf(j));
                                    if (message.equals(note)) {
                                        String barsAmountString = readWriteJSONSettings.read("taktcount");
                                        int barsFromSettings = Integer.parseInt(barsAmountString);
                                        Log.e(TAG, "rowForNote: " + metronomeController.getCursorRow());
                                        noteTrack = i;
                                        noteType = noteImagesTrackI.getString("note" + String.valueOf(j));
                                        Log.e(TAG, "noteType =" + noteType);
                                        double bar = metronomeController.getBeatsPlayed() / (barsFromSettings * staveFragment.getSize()) + 1;
                                        Beat beat = new Beat(noteTrack, (int) metronomeController.getCursorRow(), (int) Math.floor(bar), note, noteType, velocity);
                                        staveFragment._drawNote(beat);
                                        i = 6;
                                        break;
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            return true;
        }
    });

    /**
     * Конструктор
     *
     * @param context - точка текущего состояния приложения/объекта
     * @param staveFragment - фрагмент с нотным станом
     * @param metronomeController
     */
    public MidiController(final Context context, final StaveFragment staveFragment, final MetronomeController metronomeController) {
        this.mContext = context;
        this.staveFragment = staveFragment;
        this.metronomeController = metronomeController;
        readWriteJSONSettings = new ReadWriteJSONSettings(mContext);
        Log.e(TAG, "MIDI CONTROLLER");
        //Create an object with descriptions of functions
        usbMidiDriver = new UsbMidiDriver(mContext) {

            // Быстрота сигнала
            int velocity = 127;

            void setVelocity(int velocity) {
                this.velocity = velocity;
            }

            int getVelocity() {
                return velocity;
            }

            /**
             * Вызывается после присоединения USB-девайса к смартфону
             *
             * @param usbDevice the attached UsbDevice
             */
            @Override
            public void onDeviceAttached(@NonNull UsbDevice usbDevice) {
                // store usbDevice instance
                LogTextFile.getInstance().generateLogFileOnSD(mContext, usbDevice.getDeviceName());
                synchronized (usbDevices) {
                    usbDevices.add(usbDevice);
                }
            }

            @Override
            public void onMidiInputDeviceAttached(@NonNull MidiInputDevice midiInputDevice) {
                //do nothing
            }

            @Override
            public void onMidiOutputDeviceAttached(@NonNull MidiOutputDevice midiOutputDevice) {
                //do nothing
            }

            /**
             * Вызывается после отсоединения USB-девайса от смартфона
             *
             * @param usbDevice the detached UsbDevice
             */
            @Override
            public void onDeviceDetached(@NonNull UsbDevice usbDevice) {
                // remove usbDevice instance
                synchronized (usbDevices) {
                    usbDevices.remove(usbDevice);
                }
            }

            @Override
            public void onMidiInputDeviceDetached(@NonNull MidiInputDevice midiInputDevice) {
                //do nothing
            }

            @Override
            public void onMidiOutputDeviceDetached(@NonNull MidiOutputDevice midiOutputDevice) {
                //do nothing
            }

            @Override
            public void onMidiMiscellaneousFunctionCodes(@NonNull MidiInputDevice midiInputDevice, int cable, int channel, int note, int velocity) {
                //do nothing
            }

            @Override
            public void onMidiCableEvents(@NonNull MidiInputDevice midiInputDevice, int i, int i1, int i2, int i3) {
                //do nothing
            }

            @Override
            public void onMidiSystemCommonMessage(@NonNull MidiInputDevice midiInputDevice, int i, byte[] bytes) {
                //do nothing
            }

            @Override
            public void onMidiSystemExclusive(@NonNull MidiInputDevice midiInputDevice, int i, byte[] bytes) {
                //do nothing
            }

            /**
             * Метод, вызывающийся по завершению передачи MIDI-сигнала
             *
             * @param midiInputDevice
             * @param cable the cable ID 0-15
             * @param channel 0-15
             * @param note 0-127
             * @param velocity 0-127
             */
            @Override
            public void onMidiNoteOff(@NonNull MidiInputDevice midiInputDevice, final int cable, final int channel, final int note, final int velocity) {
                uiThreadEventHandler.sendMessage(Message.obtain(uiThreadEventHandler, 0, note, getVelocity(), "note on"));
                setVelocity(velocity);
            }

            /**
             * Метод получения MIDI-сигнала с MIDI-устройства
             *
             * @param midiInputDevice - device which sends midi signals
             * @param cable
             * @param channel - канал (0-15)
             * @param note - нота (0-127)
             * @param velocity быстрота (0-127)
             */
            @Override
            public void onMidiNoteOn(@NonNull MidiInputDevice midiInputDevice, final int cable,
                                     final int channel, final int note, final int velocity) {
                setVelocity(velocity);
            }

            @Override
            public void onMidiPolyphonicAftertouch(@NonNull MidiInputDevice midiInputDevice, int i, int i1, int i2, int i3) {
                //Toast.makeText(mContext, "Midi Polyphonic Aftertouch", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMidiControlChange(@NonNull MidiInputDevice midiInputDevice, int i, int i1, int i2, int i3) {
                //Toast.makeText(mContext, "Midi Control Change", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMidiProgramChange(@NonNull MidiInputDevice midiInputDevice, int i, int i1, int i2) {
                //Toast.makeText(mContext, "Midi Program Change", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMidiChannelAftertouch(@NonNull MidiInputDevice midiInputDevice, int i, int i1, int i2) {
                //Toast.makeText(mContext, "Midi Channel Aftertouch", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMidiPitchWheel(@NonNull MidiInputDevice midiInputDevice, int i, int i1, int i2) {
                //Toast.makeText(mContext, "Midi Pitch Wheel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMidiSingleByte(@NonNull MidiInputDevice midiInputDevice, int i, int i1) {
                //Toast.makeText(mContext, "Midi Single Byte", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMidiTimeCodeQuarterFrame(@NonNull MidiInputDevice midiInputDevice, int i, int i1) {
                //Toast.makeText(mContext, "Midi Time Code Quarter Frame", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMidiSongSelect(@NonNull MidiInputDevice midiInputDevice, int i, int i1) {

            }

            @Override
            public void onMidiSongPositionPointer(@NonNull MidiInputDevice midiInputDevice, int i, int i1) {

            }

            @Override
            public void onMidiTuneRequest(@NonNull MidiInputDevice midiInputDevice, int i) {
                //Toast.makeText(mContext, "Midi Tune Request", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMidiTimingClock(@NonNull MidiInputDevice midiInputDevice, int i) {

            }

            @Override
            public void onMidiStart(@NonNull MidiInputDevice midiInputDevice, int i) {
                //Toast.makeText(mContext, "Midi Start", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMidiContinue(@NonNull MidiInputDevice midiInputDevice, int i) {
                //Toast.makeText(mContext, "Midi Continue", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMidiStop(@NonNull MidiInputDevice midiInputDevice, int i) {
                //Toast.makeText(mContext, "Midi Stop", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMidiActiveSensing(@NonNull MidiInputDevice midiInputDevice, int i) {
                //Toast.makeText(mContext, "Midi Active Sensing", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMidiReset(@NonNull MidiInputDevice midiInputDevice, int i) {
                //Toast.makeText(mContext, "Midi Reset", Toast.LENGTH_SHORT).show();
            }
        };
    }

    /**
     * Метод чтения json-файла
     *
     * @param name - имя файла
     * @return возвращает json-файл в виде строки
     */
    private String readJSON(String name) {
        try {
            String path = mContext.getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj.toString();
        } catch (JSONException ex) {
            //Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        } catch (IOException ex) {
            //Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        }
    }



}
