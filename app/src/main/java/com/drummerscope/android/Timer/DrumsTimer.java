package com.drummerscope.android.Timer;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.drummerscope.android.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Класс для таймера, на мэйн экране
 *
 * Created by kmoaz on 16.09.2016.
 */
public class DrumsTimer {

    /**
     * Поле - Вью, для нахождения и изменения текствью таймера
     */

    private View mView;

    /**
     * Поле - Текствью таймера
     */

    private TextView mCounterTextView;

    /**
     * Поле - Таймер
     */

    private Timer mTimer;

    /**
     * Поле - Объект класса таймер для его обновления
     */

    private MyTimerTask mMyTimerTask;

    /**
     * Поле - Минуты таймера
     */

    private int minutes = 0;

    /**
     * Поле - Секунды таймера
     */

    private int seconds = 0;

    /**
     * Поле - Миллисекунды таймера
     */

    private int miliseconds = 0;

    /**
     * Поле - Шаг в миллисекундах для обновления таймера
     */

    private int temp = 0;

    /**
     * Поле - Часы таймера
     */

    private int hour = 0;

    /**
     * Конструктор
     * @param view - Вью для нахождения текствью таймера
     */

    public DrumsTimer(View view) {
        this.mView = view;
        mCounterTextView = (TextView) mView.findViewById(R.id.textViewTimer);
        int i = 1;
    }

    /**
     * Поле - Станрт таймера
     */

    public void startTimer(int step) {
        temp = step;
        if (mTimer != null) {
            mTimer.cancel();
        }

        mTimer = new Timer();
        mMyTimerTask = new MyTimerTask();
        miliseconds += temp;
        mTimer.schedule(mMyTimerTask, step, step);
    }

    /**
     * Поле - Остановка таймера
     */

    public void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    /**
     * Поле - Обновление таймера
     */

    public void resetTimer() {
        if (mTimer == null) {
            miliseconds = 0;
            seconds = 0;
            minutes = 0;
            mCounterTextView.setText("0" + String.valueOf(minutes) + ":0" + String.valueOf(seconds) + ":0" + String.valueOf(miliseconds));
        }
    }

    /**
     * Поле - Поток для обновления таймера
     */

    private class MyTimerTask extends TimerTask {
        Handler handler = new Handler();
        @Override
        public void run() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (miliseconds >= 1000) {
                        miliseconds %= 1000;
                        seconds++;
                    }
                    if (seconds == 60) {
                        seconds = 0;
                        minutes++;
                    }
                    if (minutes == 60) {
                        minutes = 0;
                        hour++;
                    }

                    if (hour / 10 < 1 && minutes / 10 < 1 && seconds / 10 < 1) {
                        mCounterTextView.setText("0" + String.valueOf(hour) + ":0" + String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                    }
                    if (hour / 10 < 1 && minutes / 10 < 1 && seconds / 10 >= 1) {
                        mCounterTextView.setText("0" + String.valueOf(hour) + ":0" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
                    }
                    if (hour / 10 < 1 && minutes / 10 >= 1 && seconds / 10 < 1) {
                        mCounterTextView.setText("0" + String.valueOf(hour) + ":" + String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                    }
                    if (hour / 10 < 1 && minutes / 10 >= 1 && seconds / 10 >= 1) {
                        mCounterTextView.setText("0" + String.valueOf(hour) + ":" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
                    }
                    if (hour / 10 >= 1 && minutes / 10 < 1 && seconds / 10 < 1) {
                        mCounterTextView.setText(String.valueOf(hour) + ":0" + String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                    }
                    if (hour / 10 >= 1 && minutes / 10 < 1 && seconds / 10 >= 1) {
                        mCounterTextView.setText(String.valueOf(hour) + ":0" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
                    }
                    if (hour / 10 >= 1 && minutes / 10 >= 1 && seconds / 10 < 1) {
                        mCounterTextView.setText(String.valueOf(hour) + ":" + String.valueOf(minutes) + ":0" + String.valueOf(seconds));
                    }
                    if (hour / 10 >= 1 && minutes / 10 >= 1 && seconds / 10 >= 1) {
                        mCounterTextView.setText(String.valueOf(hour) + ":" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
                    }

                    miliseconds += temp;
                }
            });
        }
    }
}