package com.drummerscope.android.Metronome;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.drummerscope.android.MainActivity;
import com.drummerscope.android.R;
import com.drummerscope.android.StaveElements.StaveFragment;
import com.drummerscope.android.Timer.DrumsTimer;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;
import com.google.firebase.crash.FirebaseCrash;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Класс отвечает за управление метрономом и его состоянием
 *
 * @author evgenyrasskazov
 */
public class MetronomeController {

    private static final String TAG = "METRONOME_CONTROLLER";
    /**
     * Минимальное и максимальное значения BPM
     */
    private final short minBpm = 40;
    private final short maxBpm = 260;
    /**
     * Экземпляр объекта Handler для обработки результатов и изменения UI
     */
    private final Handler handler = new Handler();
    /**
     * Есть ли ноты на нотном стане
     */
    public boolean notesExist = false;
    /**
     * Координата, рассчитанная на предыдущем шаге
     */
    public double previousRowForNote = -1;
    /**
     * @see Metronome
     */
    public Metronome metronome;
    /**
     * Активирован ли обратный отсчет метронома
     */
    private boolean isPreCount = false;
    /**
     * Количество тактов обратного отсчета
     */
    private int preCountBars = 1;
    /**
     * Порядок доминирующего клика
     */
    private int clickNumber = 1;
    /**
     * Текущая координата для курсора
     */
    private double cursorRow;
    /**
     * Количество прошедших кликов
     */
    private double beatsPlayed;
    /**
     * Значение отступа с краев для нотного стана
     */
    private int staveConst;
    /**
     * Значение BPM
     */
    private short bpm = 0;
    /**
     * Значение в знаменателе длины клика
     */
    private short clickMetronomeLengthValue = 4;
    private short noteLengthValue = 4;
    /**
     * Количество разделителей в нотном стане
     */
    private short amountOfDividersForStave = 4;
    private double dividersForStaveDouble;
    /**
     * @see MetronomeAsyncTask
     */
    private MetronomeAsyncTask metroTask;
    /**
     * Частота сэмпла
     */
    private int rate = 8000;
    /**
     * Кнопка "сбросить таймер"
     */
    private Button resetTimer;
    /**
     * Текстовое поле для размера стана
     */
    private TextView timeSignatureText;
    /**
     * Смещение в кликах
     */
    private int offset;
    /**
     * Текстовое поле для ввода BPM
     */
    private EditText bpmText;
    /**
     * Логическая переменная: установлен ли режим триолей
     */
    private boolean isTriplet = false;
    /**
     * Логическая переменная: установлен ли режим ноты с точкой
     */
    private boolean isNoteWithPoint = false;
    /**
     * Точка текущего состояния приложения/объекта
     */
    private Context mContext;
    /**
     * Корневая View главного окна приложения с метрономом
     */
    private View mView;
    /**
     * Фрагмент с нотным станом
     */
    private StaveFragment staveFragment;
    /**
     * Главная активити, содержащая контейнер с фрагментами. Управляет фрагментами.
     */
    private MainActivity mainActivity;
    /**
     * Логическая переменная: запущен ли метроном
     */
    private boolean play;
    /**
     * Объект - таймер метронома
     */
    private DrumsTimer drumsTimer;
    /**
     * Логическая переменная: запущен ли таймер
     */
    private boolean timerIsPlaying = false;
    /**
     * Количество ударов в секунду (BPS)
     */
    private double frequencySecondsForCoordinate;
    private double delayForCoordinate;
    /**
     * Пикселей на клик
     */
    private double pixelsPerBeat;
    /**
     * Пикселей на миллисекунду
     */
    private double pixelsPerMs;
    /**
     * Количество отыгранных тактов
     */
    private int bars;
    /**
     * Объект для чтения/записи файлов json
     */
    private ReadWriteJSONSettings readWriteJSONSettings;
    /**
     * Строка: активирован ли курсор в настройках метронома
     */
    private String metronomeMarkerSwitchString;


    /**
     * Конструктор
     * @param view - корневая вью фрагмента
     * @param context - контекст
     * @param staveFragment - фрагмент со станом
     * @param mainActivity - главная активити
     * @param drumsTimer - таймер
     * @param timerIsPlaying - запущен ли таймер
     */
    public MetronomeController(View view, Context context, StaveFragment staveFragment, MainActivity mainActivity, DrumsTimer drumsTimer, boolean timerIsPlaying) {
        this.mContext = context.getApplicationContext();
        this.mView = view;
        this.staveFragment = staveFragment;
        this.mainActivity = mainActivity;
        this.drumsTimer = drumsTimer;
        this.timerIsPlaying = timerIsPlaying;
        play = false;
        onCreate();
    }

    /**
     * Метод инициализации всех компонентов и переменных
     */
    private void onCreate() {
        readWriteJSONSettings = new ReadWriteJSONSettings(mContext);
        try {

            String barsAmountString = readWriteJSONSettings.read("taktcount");
            bars = Integer.parseInt(barsAmountString);

            Log.e(TAG, "ON CREATE STARTED");
            metroTask = new MetronomeAsyncTask(new AudioGenerator(rate, mContext));

            resetTimer = (Button) mView.findViewById(R.id.buttonReset);

            final RelativeLayout main = (RelativeLayout) mView.findViewById(R.id.relativeLayout_Main);
            main.requestFocus();

        /* Set values and listeners to buttons and stuff */

            if (bpm == 0)
                bpm = 100;

            bpmText = (EditText) mView.findViewById(R.id.bps);
            bpmText.setText(String.valueOf(bpm));
            mainActivity.bpm = bpm;
            bpmText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (!String.valueOf(bpmText.getText()).matches("")) {

                            if (Short.parseShort(String.valueOf(bpmText.getText())) >= maxBpm) {
                                bpmText.setText(String.valueOf(maxBpm));
                            }
                            if (Short.parseShort(String.valueOf(bpmText.getText())) <= minBpm) {
                                bpmText.setText(String.valueOf(minBpm));
                            }
                            bpm = Short.parseShort(String.valueOf(bpmText.getText()));
                            mainActivity.bpm = bpm;
                            main.requestFocus();
                            return false;
                        } else {
                            Toast.makeText(mContext, "BPM value cann't be empty!", Toast.LENGTH_SHORT).show();
                            return true;
                        }


                    }
                    return false;
                }
            });

            timeSignatureText = (TextView) mView.findViewById(R.id.timesignature);
            JSONObject settings = readJSON("settings.txt");
            String timeSignature = timeSignatureText.getText().toString();
            String[] value = timeSignature.split("/");
            amountOfDividersForStave = Short.parseShort(value[0]);
            noteLengthValue = Short.parseShort(value[1]);
            dividersForStaveDouble = amountOfDividersForStave;
            while (noteLengthValue > 4) {
                noteLengthValue = (short) (noteLengthValue / 2);
                dividersForStaveDouble = dividersForStaveDouble / 2.0;
            }
            while (noteLengthValue < 4) {
                noteLengthValue = (short) (noteLengthValue * 2);
                dividersForStaveDouble = dividersForStaveDouble * 2.0;
            }
            if (settings != null) {
                try {
                    String clickMetronomeLength = settings.getString("clickMetronomeLengthValue");
                    String clickNumberString = settings.getString("clickNumberValue");
                    String offsetString = settings.getString("offsetValue");

                    clickMetronomeLengthValue = Short.parseShort(clickMetronomeLength);
                    clickNumber = Integer.parseInt(clickNumberString);
                    offset = Integer.parseInt(offsetString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            staveFragment.setSize(dividersForStaveDouble);
            Log.e(TAG, "ON CREATE HAS FINISHED");
        } catch (Exception e) {
            FirebaseCrash.log("Fail on MetronomeController");
            FirebaseCrash.report(e);
        }

    }

    /**
     * Метод обработки нажатия на кнопку старт/стоп метронома
     * @param view - View кнопки старт/стоп
     */
    public synchronized void onStartStopClick(View view) {
        ToggleButton button = (ToggleButton) view;
        String timeSignature = timeSignatureText.getText().toString();
        String[] value = timeSignature.split("/");
        amountOfDividersForStave = Short.parseShort(value[0]);
        dividersForStaveDouble = amountOfDividersForStave;
        noteLengthValue = Short.parseShort(value[1]);
        while (noteLengthValue > clickMetronomeLengthValue) {
            noteLengthValue = (short) (noteLengthValue / 2);
            amountOfDividersForStave = (short) Math.ceil(amountOfDividersForStave / 2);
        }
        while (noteLengthValue < clickMetronomeLengthValue) {
            noteLengthValue = (short) (noteLengthValue * 2);
            amountOfDividersForStave = (short) (amountOfDividersForStave * 2);
        }
        noteLengthValue = Short.parseShort(value[1]);
        while (noteLengthValue > 4) {
            noteLengthValue = (short) (noteLengthValue / 2);
            dividersForStaveDouble = dividersForStaveDouble / 2.0;
        }
        while (noteLengthValue < 4) {
            noteLengthValue = (short) (noteLengthValue * 2);
            dividersForStaveDouble = dividersForStaveDouble * 2.0;
        }
        String buttonText = button.getText().toString();
        Log.e(TAG, "ON CLICK TOGGLE ACTION");
        JSONObject settings = readJSON("settings.txt");
        if (settings != null) {
            boolean isChecked;
            try {
                isPreCount = settings.getBoolean("switchPreCount");
                preCountBars = settings.getInt("preCountValue");
                isChecked = settings.getBoolean("clickDisplacementSwitch");
                if (isChecked) {
                    String noteTypeString = settings.getString("noteTypeString");
                    if (noteTypeString.equals(NoteTypes.Doublet.toString())) {
                        isTriplet = false;
                        isNoteWithPoint = false;
                    }
                    if (noteTypeString.equals(NoteTypes.Triplet.toString())) {
                        isTriplet = true;
                        isNoteWithPoint = false;
                    }
                    if (noteTypeString.equals(NoteTypes.WithPoint.toString())) {
                        isTriplet = false;
                        isNoteWithPoint = true;
                    }
                }
            } catch (JSONException e) {
                isTriplet = false;
                isNoteWithPoint = false;
                e.printStackTrace();
            }
        }
        if (buttonText.equalsIgnoreCase("stop")) {
            play = true;
            try {
                metroTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
                resetTimer.setEnabled(false);
                bpmText.setEnabled(false);
                staveFragment._clear(dividersForStaveDouble, false);
                startTimer();
                metronomeMarkerSwitchString = readWriteJSONSettings.read("metronomeMarkerSwitch");
                staveConst = staveFragment.getWidth() / StaveFragment.STAVE_SCALE;
                drawCursor();
            } catch (IllegalStateException e) {
                stopMetronome();
            }

        } else {
            stopMetronome();
        }
    }

    /**
     * Метод запуска таймера
     */
    private void startTimer() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!metronome.getBarsBefore()) {
                    if (!timerIsPlaying) {
                        drumsTimer.startTimer(1000);
                        timerIsPlaying = true;
                    }
                }
            }
        });
    }

    public double getCursorRow() {
        return cursorRow;
    }

    public double getBeatsPlayed() {
        return beatsPlayed;
    }

    /**
     * Метод для отрисовки курсора на нотном стане
     */
    private void drawCursor() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (metronome.getBarsBefore()) {
                    drawCursor();
                } else {
                    try {
                        Log.e(TAG, "BEGIN TIME_METR: " + String.valueOf(getBeginTimeOfMetronome()));
                        if (getBeginTimeOfMetronome() != 0) {
                            frequencySecondsForCoordinate = (double) bpm / (double) 60; // 2bps
                            delayForCoordinate = (double) 1000000000 / frequencySecondsForCoordinate; // 0.5s
                            pixelsPerBeat = (double) (staveFragment.getWidth() - 2 * staveConst) / (bars * staveFragment.getSize());
                            pixelsPerMs = pixelsPerBeat / delayForCoordinate;
                            double timeNow = System.nanoTime() - getBeginTimeOfMetronome();
                            timeNow -= (double) 100000000;
                            beatsPlayed = Math.floor(timeNow / delayForCoordinate);
                            double insideNotePosition = timeNow % delayForCoordinate;
                            double beatsInThisBar = beatsPlayed % (bars * staveFragment.getSize());
                            cursorRow = staveConst + Math.round(beatsInThisBar * pixelsPerBeat + insideNotePosition * pixelsPerMs);// - delayBeforePlaying;
                            if (cursorRow < staveConst)
                                cursorRow = staveConst;

                            if (play) {
                                if (cursorRow < previousRowForNote && previousRowForNote != -1) {
                                    staveFragment._clear(dividersForStaveDouble, metronome.isPlay());
                                    staveFragment._stave(dividersForStaveDouble);
                                }
                                if (metronomeMarkerSwitchString.equals("on")) {
                                    if (cursorRow > previousRowForNote)
                                        staveFragment.drawMetronomeLine((int) cursorRow);
                                }
                                previousRowForNote = cursorRow;
                                drawCursor();
                            }
                        } else {
                            drawCursor();
                        }
                    } catch (Exception e) {
                        drawCursor();
                    }
                }
            }
        }, 3);
    }

    public boolean isPlay() {
        return play;
    }

    public long getBeginTimeOfMetronome() {
        return metronome.getBeginTimeOfMetronome();
    }

    public void setBarCount(int barCount) {
    }

    public short getBpm() {
        return bpm;
    }

    public void setBpm(short bpm) {
        this.bpm = bpm;
        bpmText.setText(String.valueOf(bpm));
    }

    /**
     * Метод остановки метронома
     */
    private void stopMetronome() {
        if (isPlay()) {
            play = false;
            previousRowForNote = -1;
            metroTask.stop();
            metroTask = null;
            metroTask = new MetronomeAsyncTask(new AudioGenerator(rate, mContext));
            Runtime.getRuntime().gc();
            if (timerIsPlaying) {
                drumsTimer.stopTimer();
                timerIsPlaying = false;
            }
            handler.removeCallbacksAndMessages(null);
            resetTimer.setEnabled(true);
            bpmText.setEnabled(true);
            staveFragment.displayCount = 0;
            staveFragment._drawAllStave();
            if (staveFragment.beats.size() > 0) {
                notesExist = true;
                mainActivity.beatsToDraw = staveFragment.beats;
            }
        }
    }

    /**
     * Метод чтения json-объекта
     * @param name
     * @return
     */
    public JSONObject readJSON(String name) {
        try {
            String path = mContext.getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj;
        } catch (JSONException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * AsyncTask, ответственный за работу метронома. Запускает метроном в фоновом потоке.
     */
    private class MetronomeAsyncTask extends AsyncTask<Void, Void, String> {

        /**
         * Конструктор - создает экземпляр класса Metronome
         * @see Metronome
         * @param audioGenerator - экземпляр объекта AudioGenerator
         *                       @see AudioGenerator
         */
        MetronomeAsyncTask(AudioGenerator audioGenerator) {
            metronome = new Metronome(false, false, mContext,
                    audioGenerator);
        }

        /**
         * Метод, выполняющий настройку и запуск метронома в фоновом потоке.
         *
         * @param params - параметры
         * @return возвращает null
         */
        protected String doInBackground(Void... params) {
            double beatSound = 6440;
            double sound = 2440;
            metronome.setIsTriplet(isTriplet);
            metronome.setIsNoteWithPoint(isNoteWithPoint);
            metronome.setBeat(amountOfDividersForStave);
            metronome.setClickLength(clickMetronomeLengthValue);
            metronome.setBpm(bpm);
            metronome.setBeatSound(beatSound);
            metronome.setSound(sound);
            metronome.setBarsBeforeEnabled(isPreCount);
            metronome.setBarsBefore(preCountBars);
            metronome.setClickNumber(clickNumber);
            metronome.setOffset(offset);
            metronome.setStaveDividers(amountOfDividersForStave);
            metronome.playSimple();
            return null;
        }

        /**
         * Метод остановки метронома
         */
        void stop() {
            metronome.stop();
            metronome = null;
        }

        /**
         * Устанавливает значение bpm
         *
         * @param bpm - ударов в минуту
         */
        public void setBpm(short bpm) {
            metronome.setBpm(bpm);
            metronome.calcSilence();
        }

    }
}