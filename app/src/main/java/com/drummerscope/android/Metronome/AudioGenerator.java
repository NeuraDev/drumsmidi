/**
 * Lines 25 and 31-38 were originally found here:
 * http://stackoverflow.com/questions/2413426/playing-an-arbitrary-tone-with-android
 * which came from here:
 * http://marblemice.blogspot.com/2010/04/generate-and-play-tone-in-android.html
 */
package com.drummerscope.android.Metronome;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Класс, отвечающий за генерацию звука метронома
 *
 * @author evgenyrasskazov
 */
class AudioGenerator {

    /**
     * Частота одного сэмпла
     */
    private int sampleRate;
    /**
     * Экземпляр AudioTrack
     */
    private AudioTrack audioTrack;
    /**
     * Экземпляр AudioManager
     */
    private AudioManager am;

    /**
     * Конструктор - инициализирует аудио менеджер и устанавливает значения
     *
     * @param sampleRate - частота одного сэмпла
     * @param context    - точка текущего состояния приложения/объекта
     */
    AudioGenerator(int sampleRate, Context context) {
        this.sampleRate = sampleRate;
        am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (am != null) am.setMode(AudioManager.MODE_NORMAL);
    }

    /**
     * Метод вычисляет звуковую синусоиду в виде массива значений
     *
     * @param samples         - количество сэмплов
     * @param sampleRate      - частота одного сэмпла
     * @param frequencyOfTone - частота тона
     * @return возвращает массив значений, составляющих звуковую синусоиду
     */
    double[] getSineWave(int samples, int sampleRate, double frequencyOfTone) {
        double[] sample = new double[samples];
        for (int i = 0; i < samples; i++) {
            sample[i] = Math.sin(2 * Math.PI * i / (sampleRate / frequencyOfTone));
        }
        return sample;
    }

    /**
     * Метод генерирует звуковой байтовый массив
     *
     * @param samples - массив сэмплов (синусоида)
     * @return звуковой байтовый массив
     */
    private byte[] get16BitPcm(double[] samples) {
        byte[] generatedSound = new byte[2 * samples.length];

        int numSamples = samples.length;
        int i = 0;
        int index = 0;
        int ramp = numSamples / 15;                                    // Amplitude ramp as a percent of sample count


        for (i = 0; i < ramp; ++i) {                                     // Ramp amplitude up (to avoid clicks)
            double dVal = samples[i];
            // Ramp up to maximum
            final short val = (short) ((dVal * 32767 * i / ramp));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSound[index++] = (byte) (val & 0x00ff);
            generatedSound[index++] = (byte) ((val & 0xff00) >>> 8);
        }

        for (i = i; i < numSamples - ramp; ++i) {                        // Max amplitude for most of the samples
            double dVal = samples[i];
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSound[index++] = (byte) (val & 0x00ff);
            generatedSound[index++] = (byte) ((val & 0xff00) >>> 8);
        }

        for (i = i; i < numSamples; ++i) {                               // Ramp amplitude down
            double dVal = samples[i];
            // Ramp down to zero
            final short val = (short) ((dVal * 32767 * (numSamples - i) / ramp));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSound[index++] = (byte) (val & 0x00ff);
            generatedSound[index++] = (byte) ((val & 0xff00) >>> 8);
        }

        return generatedSound;
    }

    /**
     * Метод создает экземпляр объекта AudioTrack, который необходим для воспроизведения звука
     */
    void createPlayer() {
        //FIXME sometimes audioTrack isn't initialized
        try {
            audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    sampleRate, AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    sampleRate,
                    //bufSize < minimumBufferSize ? minimumBufferSize : bufSize,
                    AudioTrack.MODE_STREAM);
            audioTrack.play();
        } catch (Exception e) {
            FirebaseCrash.log("Fail on AudioTrack creating");
            FirebaseCrash.report(e);

        }
    }

    /**
     * Метод записывает звук в экземпляр объекта AudioTrack
     *
     * @param samples - массив сэмплов (синусоида)
     */
    void writeSound(double[] samples) {
        byte[] generatedSnd = get16BitPcm(samples);
        try {
            audioTrack.write(generatedSnd, 0, generatedSnd.length);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            Log.e("MYAPP", "exception", e);
        }
    }

    /**
     * Метод уничтожает экземпляр объекта AudioTrack
     */
    void destroyAudioTrack() {
        if (audioTrack != null && audioTrack.getState() != AudioTrack.STATE_UNINITIALIZED) {
            if (audioTrack.getPlayState() != AudioTrack.PLAYSTATE_STOPPED) {

                try {
                    audioTrack.stop();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            }

            audioTrack.release();
            am.setMode(AudioManager.MODE_NORMAL);
        }

    }


}
