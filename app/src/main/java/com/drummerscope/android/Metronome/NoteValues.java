package com.drummerscope.android.Metronome;

public enum NoteValues{
	two("2"),
	four("4"),
	eighth("8"),
	sixteen("16"),
	thirtytwo("32");
	
	private String noteValue;

	NoteValues(String noteValue) {
		this.noteValue = noteValue;
	}
	
	@Override public String toString() {
	    return noteValue;
	}

	public short getNum() {
		return Short.parseShort(noteValue);
	}
}
