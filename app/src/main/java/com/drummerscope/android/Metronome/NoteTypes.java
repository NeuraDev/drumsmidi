package com.drummerscope.android.Metronome;

/**
 * Перечисление типа ноты. Влияет на кликанье метронома.
 */
public enum NoteTypes {
	Doublet("Doublet"),
	Triplet("Triplet"),
	WithPoint("WithPoint");

	private String noteType;

	NoteTypes(String noteType) {
		this.noteType = noteType;
	}
	
	@Override public String toString() {
	    return noteType;
	}
}
