package com.drummerscope.android.Metronome;

import android.content.Context;
import android.os.Message;
import android.util.Log;

import com.drummerscope.android.LogTextFile;


/**
 * Класс, отвечающий за математическую и алгоритмическую составляющие работы метронома
 *
 * @author evgenyrasskazov
 */
public class Metronome {

    private static final String TAG = "METRONOME";
    /**
     * Поле - нужно ли воспроизведение тишины перед началом кликанья метронома
     */
    private static boolean beginningSilence;
    /**
     * Активирован ли обратный отсчет в метрономе
     */
    private static boolean barsBeforeEnabled = true;
    private double bpm;
    /**
     * Номер доминирующего клика
     */
    private int beat;
    /**
     * Длина клика
     */
    private int clickLength;
    /**
     * Количество разделителей в нотном стане
     */
    private int staveDividers = 4;
    /**
     * Длительность тишины между кликами
     */
    private int silence;
    /**
     * Флаг для определения, установлено ли значение в поле "время старта метронома"
     */
    private boolean isStartTimeSet = false;
    /**
     * Флаг для определения, установлено ли значения в поле "время старта метронома" с учетом
     * выбранного стиля кликанья
     */
    private boolean isStartTimeSetWithClickType = false;
    /**
     * Количество тактов в обратном отсчете
     */
    private int barsBefore = 1;
    /**
     * Смещение в количестве кликов
     */
    private int offset = 0;
    /**
     * Частота доминирущего клика
     */
    private double beatSound = 6440;
    /**
     * Частота обычного клика
     */
    private double sound = 2440;
    /**
     * Количество сэмплов клика
     */
    private int tick = 300; // samples of tick
    /**
     * Частота сэмпла
     */
    private int rate = 8000;
    /**
     * Поле - играет ли метроном в данный момент
     */
    private boolean play;
    /**
     * @see AudioGenerator
     * Экземпляр класса AudioGenerator
     */
    private AudioGenerator audioGenerator;
    /**
     * Массивы, значения которых образуют звуковую волну
     */
    private double[] soundTickArray;
    private double[] soundTockArray;
    private double[] silenceSoundArray;
    private double[] silenceSoundArrayIfExceed;
    private double[] withoutArray;

    /**
     * Номер текущего клика метронома
     */
    private int currentBeat = 1;
    /**
     * Суммарное количество кликов
     */
    private int sumBeat = 0;
    /**
     * Суммарное количество кликов с учетом кастомного выбора доминирующего клика
     */
    private int sumBeatForClickNumber = 1;
    /**
     * Поле - активировано ли кликанье триолями
     */
    private boolean isTriplet = false;
    /**
     * Поле - активировано ли кликанье "нота с точкой"
     */
    private boolean isNoteWithPoint = false;
    /**
     * Поле - погрешность расчета длительности тишины и клика
     */
    private double rest = 0;
    private double restSum = 0;
    /**
     * Порядок доминирующего клика
     */
    private int clickNumber = 1;
    /**
     * Поле - true, если доминирующий клик выбран в качестве любого, кроме 1-го,
     * false - если доминирующий клик - это 1-ый клик в такте
     */
    private boolean clickNumberIsNotSimple = false;
    /**
     * Время запуска метронома
     */
    private long beginTimeOfMetronome = 0;
    /**
     * Поле - обнулены ли поля после окончания работы метронома
     */
    private boolean cleared = false;
    /**
     * Точка текущего состояния приложения/объекта
     */
    private Context context;

    /**
     * Конструктор - инициализация необходимых экземпляров объектов
     *
     * @param isTriplet       - активированы ли триоли в настройках
     * @param isNoteWithPoint - активированы ли "нота с точкой" в настройках
     * @param context         - точка текущего состояния приложения/объекта
     * @param audioGenerator  - экземпляр класса AudioGenerator
     */
    public Metronome(boolean isTriplet, boolean isNoteWithPoint, Context context,
                     AudioGenerator audioGenerator) {
        this.audioGenerator = audioGenerator;
        audioGenerator.createPlayer();
        this.isTriplet = isTriplet;
        this.isNoteWithPoint = isNoteWithPoint;
        this.play = false;
        this.context = context;
    }

    /**
     * Вычисление длительности тишины между кликами в зависимости от настроек
     */
    void calcSilence() {
        double buf = 0;
        if (isTriplet) {
            double bpmWithNoteLength = bpm / (double) 4 * (double) clickLength;
            buf = (((double) 60 / (bpmWithNoteLength * 1.5)) * (double) rate) - (double) tick;
            //silence = (int) Math.round((((double) 60 / (bpmWithNoteLength * 1.5)) * (double) rate) - (double) tick);
            silence = (int) Math.round(buf);
        }
        if (isNoteWithPoint) {
            double bpmWithNoteLength = bpm / (double) 4 * (double) clickLength;
            buf = (((double) 60 / (bpmWithNoteLength / 1.5)) * (double) rate) - (double) tick;
            silence = (int) Math.round(buf);
            //silence = (int) Math.round((((double) 60 / (bpmWithNoteLength / 1.5)) * (double) rate) - (double) tick);
        }
        if (!isNoteWithPoint && !isTriplet) {
            double bpmWithNoteLength = bpm / (double) 4 * (double) clickLength;
            buf = (((double) 60 / bpmWithNoteLength) * (double) rate) - (double) tick;
            //silence = (int) Math.round((((double) 60 / bpmWithNoteLength) * (double) rate) - (double) tick);
            silence = (int) Math.round(buf);

        }
        Log.e(TAG, "SILENCE = " + silence);
        int silenceIfExceed;
        if (buf / (double) silence > 1) {
            silenceIfExceed = (int) Math.ceil(buf);
            rest = buf % (double) silence;
        } else {
            silenceIfExceed = (int) Math.floor(buf);
            rest = (double) silence - buf;
        }
        //silence = (int) Math.round((((double) 60 / bpmWithNoteLength) * (double) 8000) - (double) tick);
        if (silence < 0) {
            int tickBuf = tick;
            tickBuf += silence;
            Log.e(TAG, "tick = " + String.valueOf(tickBuf));
            silence = 0;
        }
        if (silenceIfExceed < 0) {
            silenceIfExceed = 0;
        }
        Log.e(TAG, "SILENCE = " + silence);
        Log.e(TAG, "SILENCE IF EXCEED = " + silenceIfExceed);
        soundTickArray = new double[this.tick];
        soundTockArray = new double[this.tick];
        withoutArray = new double[this.tick];
        silenceSoundArray = new double[this.silence];
        silenceSoundArrayIfExceed = new double[silenceIfExceed];
        Message msg = new Message();
        msg.obj = "" + sumBeat;
        double[] tick = audioGenerator.getSineWave(this.tick, rate, beatSound);
        double[] tock = audioGenerator.getSineWave(this.tick, rate, sound);
        double[] without = audioGenerator.getSineWave(this.tick, rate, 0);
        for (int i = 0; i < this.tick; i++) {
            soundTickArray[i] = tick[i];
            soundTockArray[i] = tock[i];
            withoutArray[i] = without[i];
        }
        for (int i = 0; i < silence; i++)
            silenceSoundArray[i] = 0;

        for (int i = 0; i < silenceIfExceed; i++)
            silenceSoundArrayIfExceed[i] = 0;

        /*if (clickLength >= 4)
            beat = beat * (clickLength / 4);
        else
            beat = beat / (4 / clickLength);
        */
    }

    /**
     * Метод "разогрева метронома"
     * Используется в качестве возможного средства избавления от изредка возникающей задержки
     */
    private void warmUp() {
        withoutArray = new double[this.tick];
        withoutArray = audioGenerator.getSineWave(this.tick, rate, 0);
        beginningSilence = true;
        audioGenerator.writeSound(withoutArray);
        beginningSilence = false;
    }

    /**
     * Метод запуска метронома
     * Используются математические вычисления, необходимые для корректной обработки настроек
     * метронома.
     */
    void playSimple() {
        warmUp();
        calcSilence();
        play = true;

        do {
            if (barsBeforeEnabled) {
                if (currentBeat == 1) {
                    audioGenerator.writeSound(soundTockArray);
                    if (!isStartTimeSet) {
                        double frequencySeconds = (double) bpm / (double) 60; // 2bps
                        double delay = (double) 1000000000 / frequencySeconds; // 0.5s
                        //Log.e(TAG, "BEAT = " + String.valueOf(beat));
                        setBeginTimeOfMetronome(System.nanoTime() + Math.round((double) staveDividers * (double) barsBefore * delay));
                        //beginTimeOfMetronome = System.nanoTime() + Math.round((double) beat * (double) barsBefore * delay);
                        //Log.e(TAG, "TIME BEGIN BEFORE PRE COUNT = " + String.valueOf(beginTimeOfMetronome));
                        isStartTimeSet = true;
                    }

                } else {
                    audioGenerator.writeSound(soundTickArray);
                }
                Log.e(TAG, "REST SUM = " + restSum);
                if (restSum >= 1) {
                    audioGenerator.writeSound(silenceSoundArrayIfExceed);
                    Log.e(TAG, "IF EXCEED");
                    restSum -= 1;
                } else {
                    audioGenerator.writeSound(silenceSoundArray);
                }
                restSum += rest;
                currentBeat++;
                if (currentBeat > beat) {
                    currentBeat = 1;
                    barsBefore--;
                }
                if (barsBefore == 0) {
                    barsBeforeEnabled = false;

                }
            } else {
                //Log.e(TAG, "BEAT = " + String.valueOf(beat));
                if (!isStartTimeSet) {
                    setBeginTimeOfMetronome(System.nanoTime());
                    Log.e(TAG, "BEGIN TIME SET = " + String.valueOf(beginTimeOfMetronome));
                    isStartTimeSet = true;
                }

                if (offset > 0) {
                    audioGenerator.writeSound(withoutArray);
                    offset--;
                } else {
                    if (clickNumberIsNotSimple) {
                        if (!isStartTimeSetWithClickType) {
                            if (clickLength == 16 || clickLength == 32)
                                setBeginTimeOfMetronome(beginTimeOfMetronome -= 120000000);
                            isStartTimeSetWithClickType = true;
                        }

                        if (sumBeatForClickNumber % clickNumber == 0) {
                            if (currentBeat == 1) {
                                audioGenerator.writeSound(soundTockArray);
                            } else {
                                audioGenerator.writeSound(soundTickArray);
                            }
                        } else {
                            audioGenerator.writeSound(withoutArray);
                        }
                    } else {

                        if (currentBeat == 1) {
                            audioGenerator.writeSound(soundTockArray);
                        } else {
                            audioGenerator.writeSound(soundTickArray);
                        }

                    }
                    //Log.e(TAG, "currentBeat = " + currentBeat);
                    //Log.e(TAG, "sumBeat = " + sumBeat);

                    currentBeat++;
                    sumBeat++;
                    sumBeatForClickNumber++;
                    if (currentBeat > beat) {
                        currentBeat = 1;
                        if (cleared)
                            cleared = false;
                    }
                }
                Log.e(TAG, "REST SUM = " + restSum);
                if (restSum >= 1) {
                    audioGenerator.writeSound(silenceSoundArrayIfExceed);
                    Log.e(TAG, "IF EXCEED");
                    //Log.e(TAG, "WRITE SILENCE IF EXCEED");
                    restSum -= 1;
                } else {
                    audioGenerator.writeSound(silenceSoundArray);
                    //Log.e(TAG, "WRITE SILENCE");
                }
                restSum += rest;
            }
        } while (play);
    }

    /**
     * Метод остановки метронома
     */
    public void stop() {
        play = false;
        isStartTimeSet = false;
        isStartTimeSetWithClickType = false;
        //Log.e(TAG, "IT IS NOT PLAYING");
        //clearHandler.removeCallbacksAndMessages(null);
        audioGenerator.destroyAudioTrack();
        currentBeat = 1;
        sumBeat = 0;
        sumBeatForClickNumber = 1;
        //beginTimeOfMetronome = 0;
        restSum = 0;
    }

    /**
     * @return возвращает знечение количества ударов в минуту
     */
    public double getBpm() {
        return bpm;
    }

    /**
     * Устанавливает количество ударов в минуту
     *
     * @param bpm - количество ударов в минуту
     */
    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    /**
     * Устанавливает длину клика (например, 1/4, 1/16, ...)
     *
     * @param clickLength - длина клика
     */
    public void setClickLength(int clickLength) {
        this.clickLength = clickLength;
    }

    /**
     * Устанавливает порядок доминирующего клика
     *
     * @param beat - порядок доминирующего клика
     */
    public void setBeat(int beat) {
        if (isTriplet) {
            int counter = beat;
            if (Math.round((double) counter * 1.5) == ((double) counter * 1.5))
                counter = (int) Math.round((double) counter * 1.5);
            else {
                while (Math.round((double) counter * 1.5) != ((double) counter * 1.5)) {
                    counter += beat;
                }
                counter = (int) Math.round((double) counter * 1.5);
            }
            this.beat = counter;
            //this.beat = (int) Math.round((double) beat * 1.5);
        }
        if (isNoteWithPoint) {
            int counter = beat;
            double res = (double) counter / 1.5;
            if (Math.round((double) counter / 1.5) == ((double) counter / 1.5))
                counter = (int) Math.round((double) counter / 1.5);
            else {
                while (Math.round((double) counter / 1.5) != ((double) counter / 1.5)) {
                    counter += beat;
                    res = (double) counter / 1.5;
                }
                counter = (int) Math.round((double) counter / 1.5);
            }
            this.beat = counter;
            //this.beat = (int) Math.round((double) beat / 1.5);
        }
        if (!isTriplet && !isNoteWithPoint) {
            this.beat = beat;
        }
        Log.e(TAG, "SET BEAT = " + beat);
    }

    /**
     * Устанавливает частоту тона доминирующего клика
     *
     * @param sound1 - частота тона клика
     */
    public void setBeatSound(double sound1) {
        this.beatSound = sound1;
    }

    /**
     * Устанавливает частоту тона обычного клика
     *
     * @param sound2 - частота тона клика
     */
    public void setSound(double sound2) {
        this.sound = sound2;
    }

    /**
     * Устанавливает true - если активирована игра триолями, иначе - false
     *
     * @param isTriplet - активирована ли настройка триолями
     */
    public void setIsTriplet(boolean isTriplet) {
        this.isTriplet = isTriplet;
    }

    /**
     * Устанавливает true - если активирована "нота с точкой", иначе - false
     *
     * @param isNoteWithPoint - активирована ли настройка "нота с точкой"
     */
    public void setIsNoteWithPoint(boolean isNoteWithPoint) {
        this.isNoteWithPoint = isNoteWithPoint;
    }

    /**
     * Метод возвращает true - если метроном запущен, иначе - false
     *
     * @return возвращает значение, запущен ли метроном
     */
    public boolean isPlay() {
        return play;
    }

    /**
     * Метод возвращает значение времени запуска метронома
     *
     * @return возвращает время запуска метронома
     */
    public long getBeginTimeOfMetronome() {
        return beginTimeOfMetronome;
    }

    /**
     * Устанавливает время запуска метронома
     *
     * @param beginTimeOfMetronome - время запуска метронома
     */
    public void setBeginTimeOfMetronome(long beginTimeOfMetronome) {
        this.beginTimeOfMetronome = beginTimeOfMetronome;
        LogTextFile.getInstance().generateLogFileOnSD(context,
                "Begin time of ticking the metronome: " + String.valueOf(this.beginTimeOfMetronome));
    }

    /**
     * @return возвращает количество тактов обратного отсчета
     */
    public boolean getBarsBefore() {
        return barsBeforeEnabled;
    }

    /**
     * Устанавливает количество тактов обратного отсчета
     *
     * @param barsBefore - количество тактов обратного отсчета
     */
    public void setBarsBefore(int barsBefore) {
        this.barsBefore = barsBefore;
    }

    /**
     * Устанавливает логическое значение - активирован ли обратный отсчет
     *
     * @param barsBeforeEnabled - активирован ли обратный отсчет
     */
    public void setBarsBeforeEnabled(boolean barsBeforeEnabled) {
        Metronome.barsBeforeEnabled = barsBeforeEnabled;
    }

    /**
     * Устанавливает порядок доминирующего клика
     *
     * @param clickNumber - порядок доминирующего клика
     */
    public void setClickNumber(int clickNumber) {
        //Log.e(TAG, "CLICK NUMBER = " + String.valueOf(clickNumber));
        this.clickNumber = clickNumber;
        clickNumberIsNotSimple = this.clickNumber != 1;
    }

    /**
     * Устанавливает смещение в кликах
     *
     * @param offset - смещение в кликах
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * Устанавливает количество разделителей стана
     *
     * @param staveDividers - количество разделителей нотного стана
     */
    public void setStaveDividers(int staveDividers) {
        this.staveDividers = staveDividers;
    }

    /**
     * @return возвращает логическое значение, необходимо ли воспроизведение тишины перед запуском
     * метронома.
     */
    public boolean isBeginningSilence() {
        return beginningSilence;
    }

}
