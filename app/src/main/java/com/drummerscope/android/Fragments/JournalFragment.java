package com.drummerscope.android.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.drummerscope.android.Journal.CreateJournalNoteFragment;
import com.drummerscope.android.Journal.HighlightDaysDecorator;
import com.drummerscope.android.Journal.JournalItemModel;
import com.drummerscope.android.Journal.JournalRecyclerViewAdapter;
import com.drummerscope.android.Journal.SimpleSectionedRecyclerViewAdapter;
import com.drummerscope.android.MainActivity;
import com.drummerscope.android.R;
import com.drummerscope.android.Tools.ReadWriteJSON;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;
import com.drummerscope.android.util.DividerItemDecoration;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import me.nlmartian.silkcal.DatePickerController;
import me.nlmartian.silkcal.SimpleMonthAdapter;

/**
 * Класс, отвечающий за работу с журналом истории
 * <p>
 * Created by evgenyrasskazov on 17.09.2016.
 */
public class JournalFragment extends Fragment implements DatePickerController {
    /**
     * Фрагмент для добавления записей в журнал
     */
    public CreateJournalNoteFragment createJournalNoteFragment;
    /**
     * View с календарем для выбора даты
     */
    public MaterialCalendarView materialCalendarView;
    /**
     * Поле - календарный день
     */
    public CalendarDay calendarDay;
    /**
     * Map с записями журнала
     */
    public Map<Integer, List<String>> notes;
    private Context context;
    /**
     * Адаптер элементов журнала для RecyclerView
     */
    private JournalRecyclerViewAdapter journalRecyclerViewAdapter;
    /**
     * Линейный макет для элементов записей
     */
    private LinearLayout linearLayout;
    /**
     * RecyclerView для записей журнала
     */
    private RecyclerView recyclerView;
    /**
     * Поля - день, месяц, год
     */
    private String day, year, month;

    /**
     * Метод инициализации экземпляра View фрагмента и заполнения контентом с обработкой событий.
     *
     * @param inflater - компонент для заполнения разных View
     * @param container - родительский container ViewGroup
     * @param savedInstanceState - сохраненное состояние фрагмента
     * @return возвращает экземпляр View фрагмента
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);

        /* Инициализация графических компонентов */
        final View journalFragmentView = inflater.inflate(R.layout.journal_fragment, container, false);
        linearLayout = (LinearLayout) journalFragmentView.findViewById(R.id.get_going);
        context = journalFragmentView.getContext();
        MainActivity activity = (MainActivity) getActivity();
        Button addNote = (Button) journalFragmentView.findViewById(R.id.ADD_NOTE);
        recyclerView = (RecyclerView) journalFragmentView.findViewById(R.id.journal_recycler_view);
        materialCalendarView = (MaterialCalendarView) journalFragmentView.findViewById(R.id.calendar_view);
        final FrameLayout frnote = (FrameLayout) journalFragmentView.findViewById(R.id.fragmentfornote);

        /* Инициализация пустого списка записей журнала */
        final List<JournalItemModel> data = new ArrayList<>();
        final ReadWriteJSON readWriteJSON = new ReadWriteJSON();

        try {
            materialCalendarView.addDecorator(new HighlightDaysDecorator(journalFragmentView.getContext(), readWriteJSON.getAllDates(journalFragmentView.getContext())));
            materialCalendarView.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Чтение записей журнала из файла */
        notes = readWriteJSON.getORDERBYDates(journalFragmentView.getContext());

        data.clear();

        final ReadWriteJSONSettings readWriteJSONSettings = new ReadWriteJSONSettings(journalFragmentView.getContext());
        String formatdatefromfile = readWriteJSONSettings.read("dateformat");

        boolean flag = false;
        /* Отсортированный Map записей журнала по дате */
        Map<Integer, List<String>> sortNotes = new TreeMap<>(notes);
        for (List<String> noteData : sortNotes.values()) {
            if (!noteData.get(4).equals(formatdatefromfile)) {
                flag = true;
                break;
            }
            data.add(new JournalItemModel(noteData.get(0), noteData.get(1), noteData.get(2), noteData.get(3)));
        }

        if (flag) {
            readWriteJSON.changeDateFormat(context, formatdatefromfile);
            data.clear();
            notes = readWriteJSON.getORDERBYDates(journalFragmentView.getContext());
            sortNotes = new TreeMap<>(notes);

            for (List<String> noteData : sortNotes.values()) {
                data.add(new JournalItemModel(noteData.get(0), noteData.get(1), noteData.get(2), noteData.get(3)));
            }
        }

        if (data.size() > 0) {
            linearLayout.setBackgroundResource(0);
            linearLayout.setBackgroundColor(Color.parseColor("#393945"));
            linearLayout.invalidate();
        }

        if (data.size() == 0) {
            linearLayout.setBackgroundResource(R.mipmap.img_journal);
            linearLayout.invalidate();
        }

        activity.menu.findItem(R.id.plus).setVisible(true);
        View addNoteIconPlus = activity.findViewById(R.id.plus);

        /* Обработка нажатия на кнопку добавления новой записи журнала */
        addNoteIconPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarDay = materialCalendarView.getSelectedDate();

                if (calendarDay != null) {
                    recyclerView.setVisibility(View.GONE);
                    frnote.setVisibility(View.VISIBLE);
                    createJournalNoteFragment = new CreateJournalNoteFragment();

                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragmentfornote, createJournalNoteFragment)
                            .commit();

                    try {
                        materialCalendarView.addDecorator(new HighlightDaysDecorator(context, readWriteJSON.getAllDates(view.getContext())));
                        materialCalendarView.invalidate();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "Choose a day", Toast.LENGTH_SHORT).show();
                }
            }
        });

        activity.menu.findItem(R.id.viewAllNotes).setVisible(true);
        View viewAll = activity.findViewById(R.id.viewAllNotes);

        /* Обработка нажатия на кнопку "Показать все" */
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<Integer, List<String>> notes = readWriteJSON.getORDERBYDates(context);

                data.clear();
                for (int i = 0; i < notes.size(); i++) {
                    List<String> noteData = notes.get(i);
                    data.add(new JournalItemModel(noteData.get(0), noteData.get(1), noteData.get(2), noteData.get(3)));
                }

                if (data.size() > 0) {

                    List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();

                    String tempDate = "00.00.00";
                    int countSection = 0;
                    for (JournalItemModel journalItemModel : data) {
                        if (!journalItemModel.date.equals(tempDate)) {
                            sections.add(new SimpleSectionedRecyclerViewAdapter.Section(countSection, journalItemModel.date));
                            tempDate = journalItemModel.date;
                        }
                        countSection++;
                    }

                    SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                    SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(view.getContext(), R.layout.notes_date, R.id.notes_date_tv, journalRecyclerViewAdapter);
                    mSectionedAdapter.setSections(sections.toArray(dummy));
                    recyclerView.setAdapter(mSectionedAdapter);

                    linearLayout.setBackgroundResource(0);
                    linearLayout.setBackgroundColor(Color.parseColor("#393945"));
                    linearLayout.invalidate();

                    materialCalendarView.clearSelection();
                    materialCalendarView.invalidate();
                }

                if (data.size() == 0) {
                    recyclerView.setAdapter(journalRecyclerViewAdapter);

                    linearLayout.setBackgroundResource(R.mipmap.img_journal);
                    linearLayout.invalidate();
                }

            }
        });

        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                parseCalendar(materialCalendarView.getSelectedDate().toString().substring(materialCalendarView.getSelectedDate().toString().indexOf("{") + 1, materialCalendarView.getSelectedDate().toString().indexOf("}")));

                data.clear();
                String tempdate = "";

                day = (day.length() == 1) ? "0" + day : day;
                month = (month.length() == 1) ? "0" + month : month;

                switch (readWriteJSONSettings.read("dateformat")) {
                    case "MM.DD.YYYY":
                        notes = readWriteJSON.findJSONObject(context, month + "." + day + "." + year);
                        tempdate = month + "." + day + "." + year;
                        break;

                    case "DD.MM.YYYY":
                        notes = readWriteJSON.findJSONObject(context, day + "." + month + "." + year);
                        tempdate = day + "." + month + "." + year;
                        break;
                }

                Map<Integer, List<String>> sortNotes = new TreeMap<>(notes);
                for (List<String> noteData : sortNotes.values()) {
                    data.add(new JournalItemModel(noteData.get(0), noteData.get(1), noteData.get(2), noteData.get(3)));
                }

                if (data.size() > 0) {
                    List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();
                    sections.add(new SimpleSectionedRecyclerViewAdapter.Section(0, tempdate));
                    SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                    SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(journalFragmentView.getContext(), R.layout.notes_date, R.id.notes_date_tv, journalRecyclerViewAdapter);
                    mSectionedAdapter.setSections(sections.toArray(dummy));
                    recyclerView.setAdapter(mSectionedAdapter);

                    linearLayout.setBackgroundResource(0);
                    linearLayout.setBackgroundColor(Color.parseColor("#393945"));
                    linearLayout.invalidate();
                } else {
                    recyclerView.setAdapter(journalRecyclerViewAdapter);
                    linearLayout.setBackgroundResource(R.mipmap.img_journal);
                    linearLayout.invalidate();
                }
            }
        });

        materialCalendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                try {
                    materialCalendarView.addDecorator(new HighlightDaysDecorator(context, readWriteJSON.getAllDates(journalFragmentView.getContext())));
                    materialCalendarView.invalidate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarDay = materialCalendarView.getSelectedDate();

                if (calendarDay != null) {
                    recyclerView.setVisibility(View.GONE);
                    frnote.setVisibility(View.VISIBLE);
                    createJournalNoteFragment = new CreateJournalNoteFragment();
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragmentfornote, createJournalNoteFragment)
                            .commit();
                } else {
                    Toast.makeText(context, "Choose a day", Toast.LENGTH_SHORT).show();
                }
            }
        });


        setupRecyclerView(journalFragmentView, data);

        return journalFragmentView;
    }

    /**
     * Метод инициализации компонента RecyclerView со списком записей журнала
     *
     * @param view - View фрагмента журнала
     * @param data - список элементов журнала
     */
    private void setupRecyclerView(View view, List<JournalItemModel> data) {
        recyclerView.addItemDecoration(new DividerItemDecoration(context));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        journalRecyclerViewAdapter = new JournalRecyclerViewAdapter(data);

        if (data.size() > 0) {
            List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();
            String tempDate = "00.00.00";
            int countSection = 0;
            for (JournalItemModel journalItemModel : data) {
                if (!journalItemModel.date.equals(tempDate)) {
                    sections.add(new SimpleSectionedRecyclerViewAdapter.Section(countSection, journalItemModel.date));
                    tempDate = journalItemModel.date;
                }
                countSection++;
            }

            SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
            SimpleSectionedRecyclerViewAdapter sectionedAdapter = new SimpleSectionedRecyclerViewAdapter(view.getContext(), R.layout.notes_date, R.id.notes_date_tv, journalRecyclerViewAdapter);
            sectionedAdapter.setSections(sections.toArray(dummy));
            recyclerView.setAdapter(sectionedAdapter);
        } else {
            recyclerView.setAdapter(journalRecyclerViewAdapter);
        }
    }

    @Override
    public int getMaxYear() {
        return 0;
    }

    @Override
    public void onDayOfMonthSelected(int year, int month, int day) {

    }

    @Override
    public void onDateRangeSelected(SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays) {

    }

    /**
     * Метод для считывания значений из календаря в поля: день, месяц, год
     *
     * @param str - исходное значение полной даты из календаря
     */
    private void parseCalendar(String str) {
        year = str.substring(0, 4);
        str = str.substring(5, str.length());
        month = String.valueOf(Integer.parseInt(str.substring(0, str.indexOf("-"))) + 1);
        str = str.substring(str.indexOf("-") + 1, str.length());
        day = str;
    }
}