package com.drummerscope.android.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drummerscope.android.R;

/**
 * Класс, отвечающий за работу с покупками.
 * <p>
 * Created by sunwi on 10.12.2017.
 */

public class DonateFragment extends Fragment {
    private static final String TAG = DonateFragment.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_donate, container, false);
        Log.e(TAG, "Donate fragment");
        return root;
    }
}
