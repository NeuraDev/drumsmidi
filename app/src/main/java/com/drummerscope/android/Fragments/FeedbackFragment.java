package com.drummerscope.android.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.drummerscope.android.R;


/**
 * Класс, отвечающий за работу с отзывами о приложении
 *
 * Created by evgenyrasskazov on 17.09.2016.
 */
public class FeedbackFragment extends Fragment {

    /**
     * Изменяемые текстовые поля для отзыва
     */
    private EditText yourEmail;
    private EditText yourName;
    private EditText yourBody;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedback_fragment, container, false);

        yourEmail = (EditText) view.findViewById(R.id.feedback_email);
        yourName = (EditText) view.findViewById(R.id.feedback_name);
        yourBody = (EditText) view.findViewById(R.id.feedback_note);

        Button sendButton = (Button) view.findViewById(R.id.feedback_send);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(yourName.getText().toString())) {
                    yourName.setError("Fill your e-mail");
                } else {
                    if (TextUtils.isEmpty(yourEmail.getText().toString())) {
                        yourEmail.setError("Fill your name");
                    } else {
                        if (TextUtils.isEmpty(yourBody.getText().toString())) {
                            yourBody.setError("Fill your e-mail body");
                        } else {
                            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "drummerscope.app@gmail.com", null));
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback from " + yourName.getText().toString() + ".");
                            emailIntent.putExtra(Intent.EXTRA_TEXT, yourBody.getText().toString());

                            try {
                                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });

        return view;
    }
}