package com.drummerscope.android.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.drummerscope.android.Dialogs.QuestionDialog;
import com.drummerscope.android.MainActivity;
import com.drummerscope.android.Metronome.NoteTypes;
import com.drummerscope.android.Metronome.NoteValues;
import com.drummerscope.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;


/**
 * Класс, отвечающий за основные настройки нотного стана и метронома
 * <p>
 * Created by evgenyrasskazov on 16.08.2016.
 */
public class SettingsFragment extends Fragment {

    private static final String TAG = "Settings";
    private Context context;
    /**
     * View фрагмента
     */
    private View fragmentView;
    /**
     * Компоненты для выбора смещения
     */
    private Switch clickDisplacementSwitch;
    private Spinner offsetSpinner;
    private LinearLayout offsetLinearLayout;
    /**
     * Компоненты для выбора типа ноты
     */
    private Spinner noteTypeSpinner;
    private LinearLayout noteTypeLinearLayout;
    /**
     * Компонент для выбора длины клика
     */
    private Spinner clickMetronomeLengthSpinner;
    /**
     * Компоненты для выбора каличества пропускаемых кликов метронома
     */
    private LinearLayout skippingClicksLinearLayout;
    private Spinner skippingClicksSpinner;
    /**
     * Компоненты для выбора обратного отсчета перед стартом метронома
     */
    private LinearLayout preCountLinearLayout;
    private Switch switchPreCount;
    private Spinner spinnerPreCount;
    /**
     * Полоски-разделители между названиями настроек
     */
    private LinearLayout firstDivide;
    private LinearLayout secondDivide;
    private LinearLayout thirdDivide;
    private LinearLayout fourthDivide;
    private JSONObject settings = new JSONObject();
    /**
     * Слушатель для обработки событий выбора смещения
     */
    private AdapterView.OnItemSelectedListener offsetSpinnerListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            try {
                ((TextView) arg0.getChildAt(0)).setTextColor(getResources().getColor(R.color.myWhite));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {

        }

    };
    /**
     * Слушатель для обработки событий выбора длины ноты
     */
    private AdapterView.OnItemSelectedListener noteValueSpinnerListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            try {
                ((TextView) arg0.getChildAt(0)).setTextColor(getResources().getColor(R.color.myWhite));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }

    };
    /**
     * Слушатель для обработки событий выбора типа ноты
     */
    private AdapterView.OnItemSelectedListener noteTypesSpinnerListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            TextView spinnerText = (TextView) noteTypeSpinner.getSelectedView().findViewById(android.R.id.text1);
            spinnerText.setTextColor(Color.WHITE);

            NoteTypes noteType = (NoteTypes) arg0.getItemAtPosition(arg2);

            switch (String.valueOf(noteType)) {
                case "Doublet":
                    break;
                case "Triplet":
                    break;
                case "WithPoint":
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            TextView spinnerText = (TextView) noteTypeSpinner.getSelectedView().findViewById(android.R.id.text1);
            spinnerText.setTextColor(Color.WHITE);
        }

    };
    /**
     * Слушатель для обработки событий выбора количества тактов обратного отсчета
     */
    private AdapterView.OnItemSelectedListener barsPreCountSpinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if (spinnerPreCount.isEnabled()) {
                try {
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.myWhite));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            if (spinnerPreCount.isEnabled()) {
                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.myWhite));
            }
        }
    };
    /**
     * Слушатель для обработки событий выбора количества пропускаемых кликов метронома
     */
    private AdapterView.OnItemSelectedListener skippingClicksSpinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
            try {
                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.myWhite));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    /**
     * Метод инициализирует View фрагмента
     *
     * @param inflater           - компонент для заполнения View
     * @param container          - контейнер View
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает View фрагмента
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.settings_fragment, container, false);
        initComponents();
        return fragmentView;
    }

    /**
     * Метод для записи JSON-объекта в файл
     *
     * @param name - имя файла
     */
    public void recordJSON(String name) {
        File fileName = null;
        String sdState = android.os.Environment.getExternalStorageState();
        if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
            String path = context.getApplicationInfo().dataDir;
            fileName = new File(path, name);
        } else {
            fileName = context.getCacheDir();
        }
        if (!fileName.exists())
            fileName.getParentFile().mkdirs();
        try {
            FileWriter f = new FileWriter(fileName);

            f.write(settings.toString());
            f.flush();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для чтения JSON-объекта из файла
     *
     * @param name - имя файла
     * @return возвращает JSON-объект
     */
    public JSONObject readJSON(String name) {
        try {
            String path = context.getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj;
        } catch (JSONException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Сохранение настроек
     */
    public void onPause() {
        super.onPause();
        String noteTypePosition = String.valueOf(noteTypeSpinner.getSelectedItemPosition());
        String clickMetronomeLengthPosition = String.valueOf(clickMetronomeLengthSpinner.getSelectedItemPosition());
        try {
            settings.put("noteTypePosition", noteTypePosition);
            settings.put("noteTypeString", noteTypeSpinner.getSelectedItem().toString());
            settings.put("clickDisplacementSwitch", clickDisplacementSwitch.isChecked());
            settings.put("switchPreCount", switchPreCount.isChecked());
            settings.put("preCountValue", spinnerPreCount.getSelectedItem().toString());
            settings.put("preCountPosition", String.valueOf(spinnerPreCount.getSelectedItemPosition()));
            settings.put("clickMetronomeLengthValue", clickMetronomeLengthSpinner.getSelectedItem().toString());
            settings.put("clickMetronomeLengthPosition", clickMetronomeLengthPosition);
            settings.put("clickNumberValue", String.valueOf(skippingClicksSpinner.getSelectedItem().toString()));
            settings.put("clickNumberPosition", String.valueOf(skippingClicksSpinner.getSelectedItemPosition()));
            settings.put("offsetPosition", String.valueOf(offsetSpinner.getSelectedItemPosition()));
            settings.put("offsetValue", String.valueOf(offsetSpinner.getSelectedItem().toString()));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        recordJSON("settings.txt");
        Log.e(TAG, "RECORDED");
    }

    /**
     * Чтение настроек, восстановление состояния фрагмента
     */
    @Override
    public void onResume() {
        super.onResume();
        initComponents();
    }

    /**
     * Инициализация всех настроек и компонентов
     */
    private void initComponents() {
        setHasOptionsMenu(true);

        context = fragmentView.getContext();

        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.menu.findItem(R.id.question).setVisible(true);

        View question = mainActivity.findViewById(R.id.question);
        question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                QuestionDialog questionDialog = new QuestionDialog();
                questionDialog.show(fragmentManager, "");
            }
        });

        clickMetronomeLengthSpinner = (Spinner) fragmentView.findViewById(R.id.click_length_spinner);

        ArrayAdapter<NoteValues> clickMetronomeLengthAdapter =
                new ArrayAdapter<NoteValues>(context,
                        android.R.layout.simple_spinner_item, NoteValues.values());
        clickMetronomeLengthSpinner.setAdapter(clickMetronomeLengthAdapter);
        clickMetronomeLengthSpinner.setSelection(NoteValues.four.ordinal());
        clickMetronomeLengthAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        clickMetronomeLengthSpinner.setOnItemSelectedListener(noteValueSpinnerListener);

        skippingClicksSpinner = (Spinner) fragmentView.findViewById(R.id.click_number_choose_spinner);

        String[] clickNumbers = {"1", "2", "3", "4"};

        ArrayAdapter<String> clickNumberChooseAdapter =
                new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_item, clickNumbers);
        skippingClicksSpinner.setAdapter(clickNumberChooseAdapter);
        skippingClicksSpinner.setSelection(0);
        clickNumberChooseAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        skippingClicksSpinner.setOnItemSelectedListener(skippingClicksSpinnerListener);

        noteTypeLinearLayout = (LinearLayout) fragmentView.findViewById(R.id.note_type_linear_layout);

        noteTypeSpinner = (Spinner) fragmentView.findViewById(R.id.note_type_spinner);
        final ArrayAdapter<NoteTypes> noteTypesAdapter =
                new ArrayAdapter<NoteTypes>(context,
                        android.R.layout.simple_spinner_item, NoteTypes.values());
        noteTypeSpinner.setAdapter(noteTypesAdapter);
        noteTypesAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        noteTypeSpinner.setOnItemSelectedListener(noteTypesSpinnerListener);
        //noteTypeSpinner.getBackground().setColorFilter(getResources().getColor(R.color.myWhite), PorterDuff.Mode.SRC_ATOP);

        final RelativeLayout relativeLayout = (RelativeLayout) fragmentView.findViewById(R.id.settings_fragment_relative_layout);
        relativeLayout.setFocusable(true);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                relativeLayout.requestFocus();
            }
        });

        offsetSpinner = (Spinner) fragmentView.findViewById(R.id.offset_spinner);
        final ArrayAdapter<String> offsetSpinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.offsetArray));
        offsetSpinner.setAdapter(offsetSpinnerAdapter);
        offsetSpinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        offsetSpinner.setOnItemSelectedListener(offsetSpinnerListener);

        //Divides
        firstDivide = (LinearLayout) fragmentView.findViewById(R.id.first_divide);
        secondDivide = (LinearLayout) fragmentView.findViewById(R.id.second_divide);
        thirdDivide = (LinearLayout) fragmentView.findViewById(R.id.third_divide);
        fourthDivide = (LinearLayout) fragmentView.findViewById(R.id.fourth_divide);

        clickDisplacementSwitch = (Switch) fragmentView.findViewById(R.id.switch_metronome_mode);
        skippingClicksLinearLayout = (LinearLayout) fragmentView.findViewById(R.id.choose_click_linear_layout);
        offsetLinearLayout = (LinearLayout) fragmentView.findViewById(R.id.offset_linear_layout);
        if (clickDisplacementSwitch.isChecked()) {
            noteTypeSpinner.setEnabled(true);
            noteTypeLinearLayout.setVisibility(View.VISIBLE);
            skippingClicksLinearLayout.setVisibility(View.VISIBLE);
            offsetLinearLayout.setVisibility(View.VISIBLE);

            firstDivide.setVisibility(View.VISIBLE);
            secondDivide.setVisibility(View.VISIBLE);
            thirdDivide.setVisibility(View.VISIBLE);
        } else {
            noteTypeSpinner.setEnabled(false);
            skippingClicksSpinner.setSelection(0);
            offsetSpinner.setSelection(0);
            noteTypeLinearLayout.setVisibility(View.GONE);
            skippingClicksLinearLayout.setVisibility(View.GONE);
            offsetLinearLayout.setVisibility(View.GONE);

            firstDivide.setVisibility(View.GONE);
            secondDivide.setVisibility(View.GONE);
            thirdDivide.setVisibility(View.GONE);
        }
        clickDisplacementSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (clickDisplacementSwitch.isChecked()) {
                    noteTypeSpinner.setEnabled(true);
                    noteTypeLinearLayout.setVisibility(View.VISIBLE);
                    skippingClicksLinearLayout.setVisibility(View.VISIBLE);
                    offsetLinearLayout.setVisibility(View.VISIBLE);

                    firstDivide.setVisibility(View.VISIBLE);
                    secondDivide.setVisibility(View.VISIBLE);
                    thirdDivide.setVisibility(View.VISIBLE);
                } else {
                    noteTypeSpinner.setEnabled(false);
                    skippingClicksSpinner.setSelection(0);
                    offsetSpinner.setSelection(0);
                    noteTypeLinearLayout.setVisibility(View.GONE);
                    skippingClicksLinearLayout.setVisibility(View.GONE);
                    offsetLinearLayout.setVisibility(View.GONE);

                    firstDivide.setVisibility(View.GONE);
                    secondDivide.setVisibility(View.GONE);
                    thirdDivide.setVisibility(View.GONE);
                }
                int pos = noteTypeSpinner.getSelectedItemPosition();
                noteTypeSpinner.setAdapter(noteTypesAdapter);
                noteTypeSpinner.setSelection(pos);
            }
        });

        preCountLinearLayout = (LinearLayout) fragmentView.findViewById(R.id.pre_count_linear_layout);

        spinnerPreCount = (Spinner) fragmentView.findViewById(R.id.pre_count_bars_spinner);
        final ArrayAdapter<String> preCountBarsAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.precount));
        spinnerPreCount.setAdapter(preCountBarsAdapter);
        preCountBarsAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        spinnerPreCount.setOnItemSelectedListener(barsPreCountSpinnerListener);
        switchPreCount = (Switch) fragmentView.findViewById(R.id.switch_pre_count);
        if (switchPreCount.isChecked()) {
            spinnerPreCount.setEnabled(true);
            preCountLinearLayout.setVisibility(View.VISIBLE);

            fourthDivide.setVisibility(View.VISIBLE);
        } else {
            spinnerPreCount.setEnabled(false);
            preCountLinearLayout.setVisibility(View.GONE);

            fourthDivide.setVisibility(View.GONE);
        }
        switchPreCount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (switchPreCount.isChecked()) {
                    spinnerPreCount.setEnabled(true);
                    preCountLinearLayout.setVisibility(View.VISIBLE);

                    fourthDivide.setVisibility(View.VISIBLE);
                } else {
                    spinnerPreCount.setEnabled(false);
                    preCountLinearLayout.setVisibility(View.GONE);

                    fourthDivide.setVisibility(View.GONE);
                }
                int pos = spinnerPreCount.getSelectedItemPosition();
                spinnerPreCount.setAdapter(preCountBarsAdapter);
                spinnerPreCount.setSelection(pos);
            }
        });


        final Button resetButton = (Button) fragmentView.findViewById(R.id.reset_settings_button);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noteTypeSpinner.setSelection(NoteTypes.Doublet.ordinal());
                clickDisplacementSwitch.setChecked(false);
                switchPreCount.setChecked(false);
                spinnerPreCount.setSelection(0);
                skippingClicksSpinner.setSelection(0);
                offsetSpinner.setSelection(0);
                clickMetronomeLengthSpinner.setSelection(1);
                String noteTypePosition = String.valueOf(noteTypeSpinner.getSelectedItemPosition());
                try {
                    settings.put("noteTypePosition", noteTypePosition);
                    settings.put("noteTypeString", noteTypeSpinner.getSelectedItem().toString());
                    settings.put("clickDisplacementSwitch", clickDisplacementSwitch.isChecked());
                    settings.put("switchPreCount", switchPreCount.isChecked());
                    settings.put("preCountValue", spinnerPreCount.getSelectedItem().toString());
                    settings.put("preCountPosition", String.valueOf(spinnerPreCount.getSelectedItemPosition()));
                    settings.put("clickNumberValue", String.valueOf(skippingClicksSpinner.getSelectedItem().toString()));
                    settings.put("clickNumberPosition", String.valueOf(skippingClicksSpinner.getSelectedItemPosition()));
                    settings.put("offsetPosition", String.valueOf(offsetSpinner.getSelectedItemPosition()));
                    settings.put("offsetValue", String.valueOf(offsetSpinner.getSelectedItem().toString()));
                    settings.put("clickMetronomeLengthValue", clickMetronomeLengthSpinner.getSelectedItem().toString());
                    settings.put("clickMetronomeLengthPosition", clickMetronomeLengthSpinner.getSelectedItemPosition());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        ScrollView settingsScrollView = (ScrollView) fragmentView.findViewById(R.id.settings_scroll_view);
        settingsScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = relativeLayout.getScrollY();
                Log.e(TAG, "scrollY = " + scrollY);
            }
        });

        JSONObject stg = readJSON("settings.txt");
        if (stg != null) {
            try {
                String noteTypePosition = stg.getString("noteTypePosition");
                String preCountPosition = stg.getString("preCountPosition");
                String clickMetronomeLengthPosition = stg.getString("clickMetronomeLengthPosition");
                String clickNumberChoosePosition = stg.getString("clickNumberPosition");
                String offsetPosition = stg.getString("offsetPosition");
                noteTypeSpinner.setSelection(Integer.parseInt(noteTypePosition));
                clickDisplacementSwitch.setChecked(stg.getBoolean("clickDisplacementSwitch"));
                switchPreCount.setChecked(stg.getBoolean("switchPreCount"));
                spinnerPreCount.setSelection(Integer.parseInt(preCountPosition));
                clickMetronomeLengthSpinner.setSelection(Integer.parseInt(clickMetronomeLengthPosition));
                skippingClicksSpinner.setSelection(Integer.parseInt(clickNumberChoosePosition));
                offsetSpinner.setSelection(Integer.parseInt(offsetPosition));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
