package com.drummerscope.android.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.drummerscope.android.Dialogs.CustomDialogAboutUs;
import com.drummerscope.android.R;
import com.eugeneek.smilebar.SmileBar;

/**
 * Класс, отвечающий за отображение информации "О нас"
 * <p>
 * Created by evgenyrasskazov on 17.09.2016.
 */
public class AboutUsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_us, container, false);

        final SmileBar smileBar = (SmileBar) view.findViewById(R.id.starBar);
        smileBar.setOnRatingSliderChangeListener(new SmileBar.OnRatingSliderChangeListener() {
            @Override
            public void onPendingRating(int rating) {
                smileBar.setRating(rating);

                if (rating <= 3) {
                    FragmentManager fragmentManager = getFragmentManager();
                    CustomDialogAboutUs customDialogAboutUs = new CustomDialogAboutUs();
                    customDialogAboutUs.show(fragmentManager, "");
                }
            }

            @Override
            public void onFinalRating(int rating) {
                Log.i("onFinalRating", "" + rating);
            }

            @Override
            public void onCancelRating() {
                Log.i("onCancelRating", "cancel");
            }
        });

        return view;
    }
}
