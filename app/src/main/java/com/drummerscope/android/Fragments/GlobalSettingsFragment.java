package com.drummerscope.android.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.drummerscope.android.GlobalSettings.ColorPickerFragment;
import com.drummerscope.android.R;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;

/**
 * Класс, отвечающий за настройки приложения
 * <p>
 * Created by kmoaz on 20.01.2017.
 */

public class GlobalSettingsFragment extends Fragment {
    /**
     * Поле - текущее состояние приложения
     */
    private static Context mContext;

    /**
     * Поле - объект для чтения/записи JSON-файлов
     */
    private ReadWriteJSONSettings readWriteJSONSettings;

    /**
     * Метод, отвечающий за наполнение фрагмента контентом
     *
     * @param inflater - экземпляр объекта для преобразования XML во View
     * @param container - контейнер
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает экземпляр корневой View фрагмента
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.global_settings_fragment, container, false);

        mContext = view.getContext();
        readWriteJSONSettings = new ReadWriteJSONSettings(mContext);

        /*
        Курсор метронома
         */

        final Switch metronomeMarkerSwitch = (Switch) view.findViewById(R.id.metronome_marker_switch);
        if (readWriteJSONSettings.read("metronomeMarkerSwitch").equals("on"))
            metronomeMarkerSwitch.setChecked(true);
        else
            metronomeMarkerSwitch.setChecked(false);
        metronomeMarkerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (metronomeMarkerSwitch.isChecked())
                    readWriteJSONSettings.write("on", "metronomeMarkerSwitch");
                else
                    readWriteJSONSettings.write("off", "metronomeMarkerSwitch");
            }
        });

        /*
        Формат даты
         */

        final String[] formatDateArray = {"MM.DD.YYYY", "DD.MM.YYYY"};
        final ArrayAdapter<String> adapterFormatDate = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, formatDateArray);
        adapterFormatDate.setDropDownViewResource(R.layout.spinner_dropdown);

        final Spinner spinnerFormatDate = (Spinner) view.findViewById(R.id.format_date);
        spinnerFormatDate.setAdapter(adapterFormatDate);
        String formatDate = readWriteJSONSettings.read("dateformat");
        int currentIDFormatDate = 0;

        for (int i = 0; i < adapterFormatDate.getCount(); i++) {
            if (adapterFormatDate.getItem(i).equals(formatDate)) {
                currentIDFormatDate = i;
                break;
            }
        }

        spinnerFormatDate.setSelection(currentIDFormatDate);

        spinnerFormatDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.myWhite));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                readWriteJSONSettings.write(spinnerFormatDate.getSelectedItem().toString(), "dateformat");
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        /*
        Количество кликов в такте
         */

        final String[] taktCountArray = {"1", "2", "3", "4"};
        final ArrayAdapter<String> adapterTaktCount = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, taktCountArray);
        adapterTaktCount.setDropDownViewResource(R.layout.spinner_dropdown);

        final Spinner spinnerTaktCount = (Spinner) view.findViewById(R.id.takt_count);
        spinnerTaktCount.setAdapter(adapterTaktCount);
        String taktCount = readWriteJSONSettings.read("taktcount");
        int currentIDTaktCount = 0;

        for (int i = 0; i < adapterTaktCount.getCount(); i++) {
            if (adapterTaktCount.getItem(i).equals(taktCount)) {
                currentIDTaktCount = i;
                break;
            }
        }

        spinnerTaktCount.setSelection(currentIDTaktCount);

        spinnerTaktCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.myWhite));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                readWriteJSONSettings.write(spinnerTaktCount.getSelectedItem().toString(), "taktcount");
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        /*
        Цвет нот
         */

        Button change_color = (Button) view.findViewById(R.id.change_color);
        change_color.setBackgroundDrawable(customView(Integer.valueOf(readWriteJSONSettings.read("notecolor"))));
        change_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    ColorPickerFragment colorPickerFragment = new ColorPickerFragment();

                    fragmentTransaction.replace(R.id.container, colorPickerFragment);
                    fragmentTransaction.addToBackStack("ColorFragment");
                    fragmentTransaction.commit();
                } catch (Exception e) {
                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        /*
        Настройка сетки нотного стана
         */

        final String[] dividersTaktCount = {"2", "4", "8", "16", "32"};
        final ArrayAdapter<String> adapterDividersTakt = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, dividersTaktCount);
        adapterDividersTakt.setDropDownViewResource(R.layout.spinner_dropdown);

        final Spinner spinnerDividersTakt = (Spinner) view.findViewById(R.id.divide_takt_count);
        spinnerDividersTakt.setAdapter(adapterDividersTakt);
        String dividersTakt = readWriteJSONSettings.read("devidetakt");
        int currentIDdividersTakt = 0;

        for (int i = 0; i < adapterDividersTakt.getCount(); i++) {
            if (adapterDividersTakt.getItem(i).equals(dividersTakt)) {
                currentIDdividersTakt = i;
                break;
            }
        }

        spinnerDividersTakt.setSelection(currentIDdividersTakt);

        spinnerDividersTakt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.myWhite));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                readWriteJSONSettings.write(spinnerDividersTakt.getSelectedItem().toString(), "devidetakt");
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        /*
        Активация триолей
         */

        Switch switch_divide = (Switch) view.findViewById(R.id.triplet_divide);
        switch_divide.setChecked(Boolean.valueOf(readWriteJSONSettings.read("dividetriplet")));
        switch_divide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                readWriteJSONSettings.write(String.valueOf(b), "dividetriplet");
            }
        });



        /*final Switch testWithPatternSwitch = (Switch) view.findViewById(R.id.test_with_pattern_switch);
        testWithPatternSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (testWithPatternSwitch.isChecked())
                    readWriteJSONSettings.write("true", "test");
                else
                    readWriteJSONSettings.write("false", "test");
            }
        });
        if (readWriteJSONSettings.read("test").isEmpty()) {
            testWithPatternSwitch.setChecked(false);
            readWriteJSONSettings.write("false", "test");
        } else {
            testWithPatternSwitch.setChecked(Boolean.valueOf(readWriteJSONSettings.read("test")));
        }*/


        return view;
    }

    /**
     * Метод для отрисовки градиентного Drawable
     *
     * @param color - id ресурса с цветом
     * @return возвращает градиентную фигуру
     */
    private GradientDrawable customView(int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{10, 10, 10, 10, 10, 10, 10, 10});
        shape.setColor(color);
        return shape;
    }
}
