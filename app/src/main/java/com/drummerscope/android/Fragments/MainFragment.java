package com.drummerscope.android.Fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.drummerscope.android.Dialogs.CreateNote.DialogClickListener;
import com.drummerscope.android.Dialogs.TimeSignatureDialog;
import com.drummerscope.android.LogTextFile;
import com.drummerscope.android.MainActivity;
import com.drummerscope.android.Metronome.MetronomeController;
import com.drummerscope.android.MidiDevice.MidiController;
import com.drummerscope.android.R;
import com.drummerscope.android.StaveElements.StaveFragment;
import com.drummerscope.android.Timer.DrumsTimer;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import jp.kshoji.driver.midi.util.UsbMidiDriver;

/**
 * Класс, отвечающий за работу главного фрагмента с нотным станом, метрономом.
 * <p>
 * Created by evgenyrasskazov on 27.11.2016.
 */

public class MainFragment extends Fragment implements DialogClickListener {

    private static final String TAG = "MainActivity";

    /**
     * Компонент View фрагмента
     */
    private View mainFragmentView;
    /**
     * Ссылка на родительскую активити MainActivity
     */
    private MainActivity mainActivity;
    /**
     * Ссылка на экземпляр класса UsbMidiDriver - драйвер для midi-устройства, подключенного через usb
     */
    private UsbMidiDriver usbMidiDriver;
    /**
     * Поле - подключены ли наушники
     */
    private boolean isHeadsetPluggedIn;
    /**
     * Контроллер метронома
     */
    private MetronomeController metronomeController;
    /**
     * Контроллер midi-устройства
     */
    private MidiController device;
    /**
     * Текстовое поле для отображения размера метронома
     */
    private TextView timeSignatureTextView;
    /**
     * Фрагмент нотного стана
     */
    private StaveFragment staveFragment;
    /**
     * Таймер
     */
    private DrumsTimer drumsTimer;
    /**
     * Поле - запущен ли таймер
     */
    private boolean timerIsPlaying = false;
    /**
     * Дефолтный массив названий для дорожек нотного стана
     */
    private String[] trackNamesDefault = {"CR", "HH", "SD", "TT", "TT", "KD"};
    /**
     * Экземпляр класса для чтения/записи JSON-объектов в файл
     */
    private ReadWriteJSONSettings readWriteJSONSettings;

    /**
     * Метод создает экземпляр фрагмента
     *
     * @return возвращает экземпляр фрагмента
     */
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    /**
     * Метод инициализирует View фрагмента и наполняет контентом
     *
     * @param inflater           - компонент для заполнения View
     * @param container          - родительский контейнер
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает View фрагмента
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mainFragmentView = inflater.inflate(R.layout.activity_main, container, false);
        mainActivity = (MainActivity) getActivity();

        final RelativeLayout main = (RelativeLayout) mainFragmentView.findViewById(R.id.relativeLayout_Main);
        main.requestFocus();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        readWriteJSONSettings = new ReadWriteJSONSettings(mainFragmentView.getContext());

        TextView firstTrackTitle = (TextView) mainFragmentView.findViewById(R.id.firstrows);
        TextView secondTrackTitle = (TextView) mainFragmentView.findViewById(R.id.secondrow);
        TextView thirdTrackTitle = (TextView) mainFragmentView.findViewById(R.id.thridrow);
        TextView fourthTrackTitle = (TextView) mainFragmentView.findViewById(R.id.fourthrow);
        TextView fifthTrackTitle = (TextView) mainFragmentView.findViewById(R.id.fifthrow);
        TextView sixthTrackTitle = (TextView) mainFragmentView.findViewById(R.id.sixthrow);

        ImageView metronomeImage = (ImageView) mainFragmentView.findViewById(R.id.metronome_image);
        metronomeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment settingsFragment = new SettingsFragment();
                FragmentManager fragmentManager = mainActivity.getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, settingsFragment);

                mainActivity.setTitle("Metronome");
                mainActivity.menu.findItem(R.id.question).setVisible(true);
                mainActivity.isMetronome = true;
                fragmentTransaction.addToBackStack("Metronome");
                fragmentTransaction.commit();
                mainActivity.intentIndex = fragmentManager.getBackStackEntryCount();

            }
        });


        timeSignatureTextView = (TextView) mainFragmentView.findViewById(R.id.timesignature);
        timeSignatureTextView.setText("" + mainActivity.timeSignatureValue1 + "/" + mainActivity.timeSignatureValue2);

        LinearLayout timeSignatureLayout = (LinearLayout) mainFragmentView.findViewById(R.id.TS);
        final Fragment fragment = this;
        timeSignatureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!metronomeController.isPlay()) {
                    String[] values = timeSignatureTextView.getText().toString().split("/");
                    int beats = Integer.parseInt(values[0]);
                    int length = Integer.parseInt(values[1]);
                    final DialogFragment newFragment = new TimeSignatureDialog().newInstance(beats, length);
                    newFragment.setTargetFragment(fragment, 0);
                    newFragment.setCancelable(false);
                    newFragment.show(getFragmentManager(), newFragment.getClass().getName());
                }
            }
        });


        final AudioManager am = (AudioManager) mainFragmentView.getContext().getSystemService(Context.AUDIO_SERVICE);

        Log.e(TAG, "CREATE STAVE FRAGMENT STARTED");
        ReadWriteJSONSettings readWriteJSONSettings = new ReadWriteJSONSettings(mainFragmentView.getContext());
        /* Инициализация нотного стана и установка параметров */
        staveFragment = new StaveFragment();
        try {
            staveFragment.taktCount = Integer.valueOf(readWriteJSONSettings.read("taktcount"));
        } catch (Exception e) {
            staveFragment.taktCount = 2;
        }
        JSONObject trackNames;
        try {
            trackNames = new JSONObject(readJSON("trackNames"));
            Log.e(TAG, trackNames.toString());
            String[] trackNamesString = new String[6];
            for (int i = 0; i < 6; i++) {
                try {
                    trackNamesString[i] = new JSONObject(readJSON("trackNames")).getString("track" + String.valueOf(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            staveFragment.trackTitleFromFile = trackNamesString;
            firstTrackTitle.setText(trackNamesString[0]);
            secondTrackTitle.setText(trackNamesString[1]);
            thirdTrackTitle.setText(trackNamesString[2]);
            fourthTrackTitle.setText(trackNamesString[3]);
            fifthTrackTitle.setText(trackNamesString[4]);
            sixthTrackTitle.setText(trackNamesString[5]);


        } catch (JSONException e1) {
            trackNames = new JSONObject();
            for (int i = 0; i < 6; i++) {
                try {
                    trackNames.put("track" + String.valueOf(i), trackNamesDefault[i]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            recordJSON("trackNames", trackNames);
            staveFragment.trackTitleFromFile = trackNamesDefault;
            firstTrackTitle.setText(trackNamesDefault[0]);
            secondTrackTitle.setText(trackNamesDefault[1]);
            thirdTrackTitle.setText(trackNamesDefault[2]);
            fourthTrackTitle.setText(trackNamesDefault[3]);
            fifthTrackTitle.setText(trackNamesDefault[4]);
            sixthTrackTitle.setText(trackNamesDefault[5]);
            e1.printStackTrace();
        }


        Log.e(TAG, "NEW STAVE FRAGMENT");

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment, staveFragment);
        fragmentTransaction.commit();

        Log.e(TAG, "STAVE HAS CREATED");

        readWriteJSONSettings.write("00:00", "timer");

        if (readWriteJSONSettings.read("bps").isEmpty()) {
            readWriteJSONSettings.write("100", "bps");
        }

        if (readWriteJSONSettings.read("taktcount").isEmpty()) {
            readWriteJSONSettings.write("2", "taktcount");
        }

        final ToggleButton playMetronomeButton = (ToggleButton) mainFragmentView.findViewById(R.id.playMetronomeButton);
        //Обработка нажатия на кнопку Старт/Стоп метронома
        playMetronomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isHeadsetPluggedIn = am.isWiredHeadsetOn();
                Log.e(TAG, "HEADSET IS: " + isHeadsetPluggedIn);
                //Выдавал ошибку
                //device.setHeadsetPluggedIn(isHeadsetPluggedIn);
                LogTextFile.getInstance().generateLogFileOnSD(mainFragmentView.getContext().getApplicationContext(),
                        "Click to play button time (nano time): " + String.valueOf(System.nanoTime()));
                metronomeController.onStartStopClick(playMetronomeButton);

                if (timerIsPlaying) {
                    ReadWriteJSONSettings readWriteJSONSettings = new ReadWriteJSONSettings(mainFragmentView.getContext());

                    String[] strings = ((TextView) mainFragmentView.findViewById(R.id.textViewTimer)).getText().toString().split(":");
                    int hour = Integer.parseInt(strings[0]) * 60 + Integer.parseInt(strings[1]);

                    String time = String.valueOf(hour);
                    if (time.length() < 2) {
                        time = "0" + time;
                    }

                    time += ":" + strings[2];

                    readWriteJSONSettings.write(time, "timer");

                    String bpsStr = ((TextView) mainFragmentView.findViewById(R.id.bps)).getText().toString();
                    readWriteJSONSettings.write(bpsStr, "bps");

                    staveFragment.displayCount = 0;
                    staveFragment.flag = true;
                }

            }
        });

        drumsTimer = new DrumsTimer(mainFragmentView);
        Button resetButton = (Button) mainFragmentView.findViewById(R.id.buttonReset);

        //Обработка сброса параметров метронома и стана
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drumsTimer.resetTimer();
                String timeSignature = timeSignatureTextView.getText().toString();
                String[] value = timeSignature.split("/");
                int amountOfDividersForStave = Short.parseShort(value[0]);
                int noteLengthValue = Short.parseShort(value[1]);
                double dividersForStaveDouble = amountOfDividersForStave;
                while (noteLengthValue > 4) {
                    noteLengthValue = (short) (noteLengthValue / 2);
                    dividersForStaveDouble = dividersForStaveDouble / 2.0;
                }
                while (noteLengthValue < 4) {
                    noteLengthValue = (short) (noteLengthValue * 2);
                    dividersForStaveDouble = dividersForStaveDouble * 2.0;
                }

                staveFragment._clear(dividersForStaveDouble, false);
                staveFragment.displayCount = 0;
                if (mainActivity.beatsToDraw != null)
                    mainActivity.beatsToDraw.clear();
                mainActivity.notesExist = false;
                metronomeController.previousRowForNote = -1;
            }
        });


        metronomeController = new MetronomeController(mainFragmentView, mainFragmentView.getContext(), staveFragment, mainActivity, drumsTimer, timerIsPlaying);
        if (mainActivity.notesExist)
            metronomeController.notesExist = true;
        Log.e(TAG, "PRE DEVICE INITIALISATION");
        device = new MidiController(mainFragmentView.getContext().getApplicationContext(), staveFragment, metronomeController);
        usbMidiDriver = device.usbMidiDriver;


        if (usbMidiDriver != null) {
            LogTextFile.getInstance().generateLogFileOnSD(mainFragmentView.getContext().getApplicationContext(),
                    "USB Midi Driver is NOT NULL");
            usbMidiDriver.open();
        } else {
            LogTextFile.getInstance().generateLogFileOnSD(mainFragmentView.getContext().getApplicationContext(),
                    "USB Midi Driver is NULL");
        }

        return mainFragmentView;
    }

    /**
     * Восстановление значений метронома из файла
     */
    @Override
    public void onResume() {
        super.onResume();
        short bpm;
        try {
            String bpmString = readWriteJSONSettings.read("bpm");
            bpm = Short.parseShort(bpmString);
        } catch (Exception e) {
            bpm = 100;
        }
        if (metronomeController != null) {
            if (bpm == 0)
                metronomeController.setBpm((short) 100);
            else if (metronomeController.getBpm() != 0)
                metronomeController.setBpm(bpm);
        }
        mainActivity.bpm = bpm;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (usbMidiDriver != null) {
            usbMidiDriver.close();
        }
    }


    /**
     * Остановка метронома и запись текущих параметров
     */
    @Override
    public void onPause() {
        if (metronomeController != null) {
            mainActivity.notesExist = metronomeController.notesExist;
            mainActivity.beatsToDraw = staveFragment.beats;
            if (metronomeController.metronome != null)
                if (metronomeController.metronome.isPlay()) {
                    metronomeController.metronome.stop();
                    metronomeController.setBarCount(1);
                }
        }

        // Определение, активен ли экран
        PowerManager powerManager = (PowerManager) mainFragmentView.getContext().getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            isScreenOn = powerManager.isInteractive();
        } else {
            isScreenOn = powerManager.isScreenOn();
        }

        if (isScreenOn) {
            mainActivity.bpm = metronomeController.getBpm();
            readWriteJSONSettings.write(String.valueOf(metronomeController.getBpm()), "bpm");
        }

        super.onPause();
    }

    /**
     * Установка параметров метронома (размер, ...)
     *
     * @param beats  - количество кликов
     * @param length - доля клика
     */
    @Override
    public void onYesClick(int beats, int length) {
        double dividersForStaveDouble = beats;
        timeSignatureTextView.setText("" + String.valueOf(beats) + "/" + String.valueOf(length));
        mainActivity.timeSignatureValue1 = String.valueOf(beats);
        mainActivity.timeSignatureValue2 = String.valueOf(length);
        ReadWriteJSONSettings readWriteJSONSettings = new ReadWriteJSONSettings(mainFragmentView.getContext());
        readWriteJSONSettings.write(String.valueOf(length), "notelength");
        staveFragment.divideTaktCount = Integer.valueOf(readWriteJSONSettings.read("devidetakt")) / length;
        staveFragment.displayCount = 0;
        readWriteJSONSettings.write(String.valueOf(beats), "size");
        while (length > 4) {
            length = length / 2;
            dividersForStaveDouble = dividersForStaveDouble / 2.0;
            beats = (int) Math.ceil(beats / 2);
        }
        while (length < 4) {
            length = length * 2;
            dividersForStaveDouble = dividersForStaveDouble * 2.0;
            beats = beats * 2;
        }
        staveFragment.setSize(dividersForStaveDouble);
        staveFragment._stave(dividersForStaveDouble);
    }

    @Override
    public void onYesClick(String[] booleanArray, int second) {

    }

    @Override
    public void onYesClick() {

    }

    @Override
    public void onNoClick() {

    }

    /**
     * Приостановка метронома по открытию меню навигации
     */
    public void onDrawerOpened() {
        if (metronomeController != null)
            if (metronomeController.isPlay()) {
                Log.e(TAG, "STOP METRONOME");
                ToggleButton playMetronomeButton = (ToggleButton) mainFragmentView.findViewById(R.id.playMetronomeButton);
                playMetronomeButton.setChecked(true);
                metronomeController.onStartStopClick(playMetronomeButton);
            }
    }

    private void recordJSON(String name, JSONObject jsonFile) {
        File fileName = null;
        String sdState = android.os.Environment.getExternalStorageState();
        if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
            String path = mainFragmentView.getContext().getApplicationInfo().dataDir;
            fileName = new File(path, name);
        } else {
            fileName = mainFragmentView.getContext().getCacheDir();
        }

        if (!fileName.exists()) {
            fileName.getParentFile().mkdirs();
        }
        try {
            FileWriter f = new FileWriter(fileName);
            if (name.equals("notes")) {
                f.write(jsonFile.toString());
            } else {
                f.write(jsonFile.toString());
            }
            f.flush();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readJSON(String name) {
        try {
            String path = mainFragmentView.getContext().getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj.toString();
        } catch (JSONException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        }
    }


}
