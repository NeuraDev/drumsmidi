package com.drummerscope.android.Fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.drummerscope.android.Dialogs.CreateNote.DialogClickListener;
import com.drummerscope.android.Dialogs.CreateNote.SoundCheckDialog;
import com.drummerscope.android.NotesRecyclerView.NotesItemModel;
import com.drummerscope.android.NotesRecyclerView.NotesRecyclerViewRecyclerAdapter;
import com.drummerscope.android.R;
import com.drummerscope.android.util.DividerItemDecoration;
import com.github.aakira.expandablelayout.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, отвечающий за настройки и установку нот на дорожки
 * <p>
 * Created by evgen on 31.08.2016.
 */
public class NotesSettingsFragment extends Fragment implements DialogClickListener {

    private static final String TAG = "NOTES_SETTINGS_TAG";

    /**
     * Адаптер RecyclerView для отображения списка нот каждой дорожки
     */
    private static NotesRecyclerViewRecyclerAdapter notesRecyclerViewRecyclerAdapter;

    /**
     * Метод, уведомляющий адаптер о том, чего его элемент изменил свое содержание
     *
     * @param numPosition - позиция элемента в адаптере
     */
    public static void setNotesRecyclerViewRecyclerAdapter(int numPosition) {
        notesRecyclerViewRecyclerAdapter.notifyItemChanged(numPosition);
    }

    /**
     * Метод инициализирует View фрагмента и наполняет ее контентом
     *
     * @param inflater           - компонент для заполнения View
     * @param container          - контейнер View
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает View фрагмента
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.notes_settings_fragment, container, false);
        Context context = view.getContext().getApplicationContext();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        final DialogFragment newFragment = SoundCheckDialog.newInstance(0);
        newFragment.setTargetFragment(this, 0);

        Button checkSoundButton = (Button) view.findViewById(R.id.soundCheckButton);
        checkSoundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newFragment.setCancelable(false);
                newFragment.show(getFragmentManager(), newFragment.getClass().getName());
            }
        });


        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(context));
        recyclerView.setLayoutManager(linearLayoutManager);

        List<NotesItemModel> data = new ArrayList<>();
        data.add(new NotesItemModel(
                "TRACK 1",
                R.color.myBlackForLowerPanel,
                R.color.myBlackForLowerPanel,
                Utils.createInterpolator(Utils.DECELERATE_INTERPOLATOR)));
        data.add(new NotesItemModel(
                "TRACK 2",
                R.color.myBlackForLowerPanel,
                R.color.myBlackForLowerPanel,
                Utils.createInterpolator(Utils.DECELERATE_INTERPOLATOR)));
        data.add(new NotesItemModel(
                "TRACK 3",
                R.color.myBlackForLowerPanel,
                R.color.myBlackForLowerPanel,
                Utils.createInterpolator(Utils.DECELERATE_INTERPOLATOR)));
        data.add(new NotesItemModel(
                "TRACK 4",
                R.color.myBlackForLowerPanel,
                R.color.myBlackForLowerPanel,
                Utils.createInterpolator(Utils.DECELERATE_INTERPOLATOR)));
        data.add(new NotesItemModel(
                "TRACK 5",
                R.color.myBlackForLowerPanel,
                R.color.myBlackForLowerPanel,
                Utils.createInterpolator(Utils.DECELERATE_INTERPOLATOR)));
        data.add(new NotesItemModel(
                "TRACK 6",
                R.color.myBlackForLowerPanel,
                R.color.myBlackForLowerPanel,
                Utils.createInterpolator(Utils.DECELERATE_INTERPOLATOR)));

        notesRecyclerViewRecyclerAdapter = new NotesRecyclerViewRecyclerAdapter(data, NotesSettingsFragment.this, linearLayoutManager, recyclerView);
        recyclerView.setAdapter(notesRecyclerViewRecyclerAdapter);
        return view;
    }

    /**
     * Метод обратного вызова для обработки результата саундчека
     */
    @Override
    public void onYesClick() {
        notesRecyclerViewRecyclerAdapter.notifyDataSetChanged();
        Log.e(TAG, "SOUNDCHECK IS ENDED");
    }

    @Override
    public void onNoClick() {
    }

    @Override
    public void onYesClick(int first, int second) {

    }

    @Override
    public void onYesClick(String[] booleanArray, int second) {

    }

}
