package com.drummerscope.android;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Класс для логирования
 *
 * Created by rev7a on 16.01.2018.
 */

public class LogTextFile {

    /**
     * Поле - Объект текущего класса
     */

    private static LogTextFile logger;

    /**
     * Поле - Не используется
     */

    private boolean isFinished;

    /**
     * Поле - Не используется
     */

    private String textToSave;

    /**
     * Поле - Имя файла лога
     */

    private String fileName;

    /**
     * Поле - Объект для записи данных в файл
     */

    private FileWriter writer;

    /**
     * Поле - Объект файл, в который ведётся запись
     */

    private File logFile;

    /**
     * Конструктор
     */

    private LogTextFile() {
        isFinished = false;
    }

    /**
     * Синглтон
     * @return Объект класса LogTextFile
     */

    public static LogTextFile getInstance() {
        if (logger == null) {
            logger = new LogTextFile();
        }
        return logger;
    }

    /**
     * Формирование файла с текстом для лога
     * @param context - Контекст
     * @param text - Текст для лога
     */


    public void generateLogFileOnSD(final Context context, String text) {

        File root = new File(Environment.getExternalStorageDirectory(), "Log");
        if (!root.exists()) {
            root.mkdirs();
        }
        logFile = new File(root, fileName);
        try {
            if (!logFile.exists()) {
                logFile.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(logFile, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            outputStreamWriter.append(text);
            outputStreamWriter.append("\n");
            outputStreamWriter.flush();
            outputStreamWriter.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для закрытия потока записи данных
     */

    public void close() {
        if (writer == null) return;
        try {
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Сеттер имени файла
     * @param fileName - Имя файла
     */

    public void setFileName(String fileName) {
        if (this.fileName != null) return;
        this.fileName = fileName + ".txt";
    }

    /**
     * Геттер имени файла
     * @return Имя файла
     */

    public String getFileName() {
        return fileName;
    }
}
