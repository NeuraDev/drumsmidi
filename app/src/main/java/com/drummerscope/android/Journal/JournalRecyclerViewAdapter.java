package com.drummerscope.android.Journal;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.drummerscope.android.R;
import com.drummerscope.android.Tools.ReadWriteJSON;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.rambler.libs.swipe_layout.SwipeLayout;

/**
 * Класс, отвечающий за работу адаптера компонента RecyclerView журнала
 * <p>
 * Created by evgen on 17.09.2016.
 */
public class JournalRecyclerViewAdapter extends RecyclerView.Adapter<JournalRecyclerViewAdapter.ViewHolder> {

    /**
     * Список записей журнала
     */
    private static List<JournalItemModel> data = null;
    /**
     * Макет для списка записей журнала
     */
    private LinearLayout linearLayout;
    /**
     * Компонент View календаря
     */
    private MaterialCalendarView materialCalendarView;
    /**
     * Поле-флаг, если это свайп = true, иначе = false
     */
    private boolean isSwipe = false;
    /**
     * Точка текущего состояния приложения/объекта
     */
    private Context context;

    /**
     * Конструктор - устанавливает список записей журнала
     *
     * @param data - список записей журнала
     */
    public JournalRecyclerViewAdapter(List<JournalItemModel> data) {
        JournalRecyclerViewAdapter.data = data;
    }

    public JournalRecyclerViewAdapter() {

    }

    /**
     * Метод, возвращающий список записей журнала
     *
     * @return возвращает список записей журнала
     */
    public static List<JournalItemModel> getData() {
        return data;
    }

    /**
     * Метод создает новые View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     *
     * @param parent   - родительский ViewGroup
     * @param viewType - тип View
     * @return возвращает ViewHolder, управляющий соответствующей View записи журнала
     */
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notes_items, parent, false);
        final ViewHolder viewHolder = new ViewHolder(itemView);

        linearLayout = (LinearLayout) ((Activity) context).findViewById(R.id.get_going);
        materialCalendarView = (MaterialCalendarView) ((Activity) context).findViewById(R.id.calendar_view);

        // Обработка свайпа
        viewHolder.swipeLayout.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {
                isSwipe = true;
            }

            @Override
            public void onSwipeClampReached(SwipeLayout swipeLayout, boolean moveToRight) {
                if (!moveToRight) {

                    ReadWriteJSON readWriteJSON = new ReadWriteJSON();
                    readWriteJSON.removeJSONObject(context, viewHolder._bpm.getText().toString(), viewHolder._date.getText().toString(), viewHolder._time.getText().toString(), viewHolder._exercise.getText().toString());

                    data.clear();
                    Map<Integer, List<String>> notes = readWriteJSON.getORDERBYDates(context);
                    for (int i = 0; i < notes.size(); i++) {
                        data.add(new JournalItemModel(notes.get(i).get(0), notes.get(i).get(1), notes.get(i).get(2), notes.get(i).get(3)));
                    }

                    JournalRecyclerViewAdapter journalRecyclerViewAdapter = new JournalRecyclerViewAdapter(data);

                    RecyclerView recyclerView = (RecyclerView) parent.findViewById(R.id.journal_recycler_view);

                    materialCalendarView.addDecorator(new HighlightDaysDecorator(parent.getContext(), readWriteJSON.getAllDates(context)));
                    materialCalendarView.invalidate();

                    if (data.size() > 0) {

                        List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();

                        String tempDate = "00.00.00";
                        int countSection = 0;
                        for (JournalItemModel journalItemModel : data) {
                            if (!journalItemModel.date.equals(tempDate)) {
                                sections.add(new SimpleSectionedRecyclerViewAdapter.Section(countSection, journalItemModel.date));
                                tempDate = journalItemModel.date;
                            }
                            countSection++;
                        }

                        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                        SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(context, R.layout.notes_date, R.id.notes_date_tv, journalRecyclerViewAdapter);
                        mSectionedAdapter.setSections(sections.toArray(dummy));
                        try {
                            recyclerView.setAdapter(mSectionedAdapter);
                        } catch (Exception e) {
                            Log.e(e.toString(), e.toString());
                        }

                        linearLayout.setBackgroundResource(0);
                        linearLayout.setBackgroundColor(Color.parseColor("#393945"));
                        linearLayout.invalidate();
                    } else {
                        linearLayout.setBackgroundResource(R.mipmap.img_journal);
                        linearLayout.invalidate();

                        try {
                            recyclerView.setAdapter(journalRecyclerViewAdapter);
                        } catch (Exception e) {
                            Log.e(e.toString(), e.toString());
                        }
                    }
                    isSwipe = false;
                }
            }

            @Override
            public void onLeftStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isSwipe = false;
                    }
                }, 100);
            }

            @Override
            public void onRightStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isSwipe = false;
                    }
                }, 100);

            }
        });

        return viewHolder;
    }

    /**
     * Заполнение виджетов View данными из элемента списка с позицией i
     *
     * @param holder   - соответсвующий ViewHolder
     * @param position - позиция записи журнала в адаптере
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        JournalItemModel dataItem = data.get(position);
        holder._date.setText(dataItem.date);
        holder._time.setText(dataItem.time);
        holder._exercise.setText(dataItem.exercise);
        holder._bpm.setText("BPM: " + dataItem.bpm);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSwipe) {
                    final RecyclerView recyclerView = (RecyclerView) ((Activity) context).findViewById(R.id.journal_recycler_view);
                    final FrameLayout frnote = (FrameLayout) ((Activity) context).findViewById(R.id.fragmentfornote);

                    recyclerView.setVisibility(View.GONE);
                    frnote.setVisibility(View.VISIBLE);

                    EditJournalNoteFragment editMyFragment = EditJournalNoteFragment.newInstance(holder.getAdapterPosition());

                    String tempDate = "00.00.00";
                    int countSection = 0;
                    for (int i = 0; i <= position; i++) {
                        if (!data.get(i).date.equals(tempDate)) {
                            countSection++;
                            tempDate = data.get(i).date;
                        }
                    }
                    if (countSection == 0) {
                        countSection++;
                    }

                    editMyFragment.sectionNumber = countSection;
                    FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragmentfornote, editMyFragment);
                    fragmentTransaction.commit();
                }
            }
        });
    }

    /**
     * @return возвращает количество записей журнала
     */
    public int getItemCount() {
        return data.size();
    }

    /**
     * Метод добавляет запись в список
     *
     * @param dataItem - запись журнала
     */
    public void addDataItem(JournalItemModel dataItem) {
        data.add(0, dataItem);
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты.
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        private final SwipeLayout swipeLayout;
        private TextView _date;
        private TextView _exercise;
        private TextView _time;
        private TextView _bpm;

        /**
         * Конструктор - инициализирует все поля
         *
         * @param itemView - шаблон View записи журнала
         */
        public ViewHolder(View itemView) {
            super(itemView);
            _date = (TextView) itemView.findViewById(R.id.d);
            _exercise = (TextView) itemView.findViewById(R.id.e);
            _time = (TextView) itemView.findViewById(R.id.t);
            _bpm = (TextView) itemView.findViewById(R.id.b);
            String temp = _bpm.toString();
            temp.substring(temp.indexOf(" "), temp.length());
            _bpm.setText(temp);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe_layout);
        }
    }
}