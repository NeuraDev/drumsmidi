package com.drummerscope.android.Journal;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.drummerscope.android.R;
import com.drummerscope.android.Tools.ReadWriteJSON;
import com.google.firebase.crash.FirebaseCrash;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, отвечающий за редактирование записи журнала
 * <p>
 * Created by kmoaz on 04.12.2016.
 */

public class EditJournalNoteFragment extends Fragment {

    /**
     * //TODO 15.04.2018 тут указать, что это за поле
     */
    public int sectionNumber = 1;
    /**
     * Изменяемые текстовые поля BPM, дата, время, название упражнения
     */
    private EditText newBPM, newdate, newtime, newexercise;
    /**
     * Поля - старые значения BPM, даты, времени, названия упражнения
     */
    private String oldBPM, olddate, oldtime, oldexercise;
    /**
     * Экземпляр модели записи журнала
     */
    private JournalItemModel item;

    /**
     * Метод создает экземпляр фрагмента
     *
     * @param num - позиция записи журнала в общем списке записей
     * @return возвращает экземпляр фрагмента
     */
    public static EditJournalNoteFragment newInstance(int num) {
        EditJournalNoteFragment f = new EditJournalNoteFragment();
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }

    /**
     * Метод инициализирует View фрагмента и наполняет контентом
     *
     * @param inflater           - компонент, используемый для заполнения
     * @param container          - контейнер View
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает заполненную View фрагмента
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.journal_edit_dialog, container, false);

        newBPM = (EditText) view.findViewById(R.id.journal_item_bpm_change);
        newdate = (EditText) view.findViewById(R.id.journal_item_date_change);
        newtime = (EditText) view.findViewById(R.id.journal_item_time_change);
        newexercise = (EditText) view.findViewById(R.id.journal_item_exercise_change);

        newdate.setKeyListener(null);

        int notePosition = getArguments().getInt("num");
        final List<JournalItemModel> items = JournalRecyclerViewAdapter.getData();

        item = items.get(notePosition - sectionNumber);
        newBPM.setText(item.bpm + " bpm");
        newdate.setText(item.date);
        newtime.setText(item.time + " min");
        newexercise.setText(item.exercise);

        oldBPM = item.bpm;
        olddate = item.date;
        oldtime = item.time;
        oldexercise = item.exercise;

        final RecyclerView recyclerView = (RecyclerView) ((Activity) view.getContext()).findViewById(R.id.journal_recycler_view);
        final FrameLayout fragmentfornote = (FrameLayout) ((Activity) view.getContext()).findViewById(R.id.fragmentfornote);

        final JournalRecyclerViewAdapter journalRecyclerViewAdapter = new JournalRecyclerViewAdapter();

        final ImageView exit;
        final TextView save, discard;

        newexercise.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                newBPM.setText(item.bpm);
                newtime.setText(item.time);
                return false;
            }
        });

        newtime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                newBPM.setText(item.bpm);
                newtime.setText(item.time);
                return false;
            }
        });

        newtime.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                newBPM.setText(newBPM.getText().toString() + " bpm");
                newtime.setText(newtime.getText().toString() + " min");
                return false;
            }
        });

        newBPM.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                newBPM.setText(item.bpm);
                newtime.setText(item.time);
                return false;
            }
        });

        save = (TextView) view.findViewById(R.id.SAVE_CHANGE);
        /* Обработка события сохранения изменений */
        save.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View view) {
                item.bpm = newBPM.getText().toString().contains("bpm") ? newBPM.getText().toString().substring(0, newBPM.getText().toString().indexOf(" ")) : newBPM.getText().toString();
                item.date = newdate.getText().toString();
                item.time = newtime.getText().toString().contains("min") ? newtime.getText().toString().substring(0, newtime.getText().toString().indexOf(" ")) : newtime.getText().toString();
                item.exercise = newexercise.getText().toString();

                ReadWriteJSON readWriteJSON = new ReadWriteJSON();
                readWriteJSON.editJSONObject(view.getContext(), oldBPM, olddate, oldtime, oldexercise, item.bpm, item.date, item.time, item.exercise);

                List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();

                String tempDate = "00.00.00";
                int countSection = 0;
                for (JournalItemModel journalItemModel : items) {
                    if (!journalItemModel.date.equals(tempDate)) {
                        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(countSection, journalItemModel.date));
                        tempDate = journalItemModel.date;
                    }
                    countSection++;
                }

                SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(view.getContext(), R.layout.notes_date, R.id.notes_date_tv, journalRecyclerViewAdapter);
                mSectionedAdapter.setSections(sections.toArray(dummy));
                recyclerView.setAdapter(mSectionedAdapter);

                fragmentfornote.setVisibility(FrameLayout.GONE);
                recyclerView.setVisibility(View.VISIBLE);

            }
        });

        discard = (TextView) view.findViewById(R.id.DISCARD_CHANGE);
        /* Обработка события отклонения изменений */
        discard.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentfornote.setVisibility(FrameLayout.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    FirebaseCrash.log("Fail on click discard Edit Journal");
                    FirebaseCrash.report(e);
                }
            }
        });

        exit = (ImageView) view.findViewById(R.id.EXIT_CHANGE);
        /* Обработка события закрытия фрагмента изменений */
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentfornote.setVisibility(FrameLayout.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    FirebaseCrash.log("Fail on click exit Edit Journal");
                    FirebaseCrash.report(e);
                }
            }
        });

        return view;
    }
}