package com.drummerscope.android.Journal;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.drummerscope.android.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, отвечающий за подсвечивание дней в календаре
 * <p>
 * Created by evgenyrasskazov on 25.01.2017.
 */

public class HighlightDaysDecorator implements DayViewDecorator {

    /**
     * Фигура, использующаяся в качестве подсветки
     */
    private final Drawable highlightDrawable;
    /**
     * Список дат, которые необходимо выделять (подсвечивать)
     */
    private List<String> datesDecorates = new ArrayList<>();

    /**
     * Конструктор - инициализирует фигуру и заполняет список дат
     *
     * @param context        - точка текущего состояния приложения/объекта
     * @param datesDecorates - массив дат, необходимых для выделения
     */
    public HighlightDaysDecorator(Context context, String[] datesDecorates) {
        highlightDrawable = ContextCompat.getDrawable(context, R.drawable.material_calendar_view);

        for (String dateDecorate : datesDecorates) {
            String[] tempDecorate = dateDecorate.split("-");
            String temp = tempDecorate[0] + "-";

            if (tempDecorate[1].length() == 2 && tempDecorate[1].substring(0, 1).equals("0")) {
                temp += tempDecorate[1].substring(1, 2) + "-";
            } else {
                temp += tempDecorate[1] + "-";
            }

            if (tempDecorate[2].length() == 2 && tempDecorate[2].substring(0, 1).equals("0")) {
                temp += tempDecorate[2].substring(1, 2);
            } else {
                temp += tempDecorate[2];
            }

            this.datesDecorates.add("CalendarDay{" + temp + "}");
        }
    }

    /**
     * Метод, определяюший следует ли выделять дату или нет
     *
     * @param day - календарный день
     * @return возвращает true - если дату надо выделять, false - если не надо выделять
     */
    @Override
    public boolean shouldDecorate(CalendarDay day) {
        for (String dateDecorate : this.datesDecorates) {
            if (String.valueOf(day).equals(dateDecorate) && datesDecorates != null) {
                this.datesDecorates.remove(dateDecorate);
                return true;
            }
        }
        return false;
    }

    /**
     * Метод, устанавливающий фон (выделение) соотвествуюей View
     *
     * @param view - View, которую необходимо подсветить
     */
    @Override
    public void decorate(DayViewFacade view) {
        view.setBackgroundDrawable(highlightDrawable);
    }
}