package com.drummerscope.android.Journal;

/**
 * Модель элемента записи журнала
 * <p>
 * Created by evgen on 17.09.2016.
 */
public class JournalItemModel {
    public String bpm;
    public String date;
    public String time;
    public String exercise;

    /**
     * Конструктор, инициализирующий экземпляр записи журнала
     *
     * @param bpm      - количество ударов в минуту
     * @param date     - дата
     * @param time     - время
     * @param exercise - название упражнения
     */
    public JournalItemModel(String bpm, String date, String time, String exercise) {
        this.bpm = bpm;
        this.date = date;
        this.exercise = exercise;
        this.time = time;
    }
}
