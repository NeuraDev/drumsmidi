package com.drummerscope.android.Journal;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.drummerscope.android.R;
import com.drummerscope.android.Tools.ReadWriteJSON;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;
import com.google.firebase.crash.FirebaseCrash;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.List;


/**
 * Класс, отвечающий за создание записи в журнале
 * <p>
 * Created by kmoaz on 02.12.2016.
 */

public class CreateJournalNoteFragment extends Fragment {

    /**
     * Изменяемые текстовые поля для указания BPM, даты, времени и названия занятия
     */
    private EditText BPM, date, time, exercise;
    /**
     * Поля - день, месяц, год
     */
    private String day, month, year;

    /**
     * Метод инициализирует View фрагмента для создания записи в журнале и наполняет информацией
     *
     * @param inflater           - компонент для заполнения View
     * @param container          - контейнер View
     * @param savedInstanceState - сохраненное состояние
     * @return возвращает View фрагмента
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.journal_create_dialog, container, false);

        final RecyclerView recyclerView = (RecyclerView) ((Activity) view.getContext()).findViewById(R.id.journal_recycler_view);
        final FrameLayout fragmentfornote = (FrameLayout) ((Activity) view.getContext()).findViewById(R.id.fragmentfornote);
        final JournalRecyclerViewAdapter journalRecyclerViewAdapter = new JournalRecyclerViewAdapter();

        final ImageView exit;
        final TextView save, discard;

        BPM = (EditText) view.findViewById(R.id.journal_item_bpm);
        date = (EditText) view.findViewById(R.id.journal_item_date);
        time = (EditText) view.findViewById(R.id.journal_item_time);
        exercise = (EditText) view.findViewById(R.id.journal_item_exercise);

        date.setKeyListener(null);

        final MaterialCalendarView materialCalendarView = (MaterialCalendarView) getActivity().findViewById(R.id.calendar_view);
        parseCalendar(materialCalendarView.getSelectedDate().toString().substring(materialCalendarView.getSelectedDate().toString().indexOf("{") + 1, materialCalendarView.getSelectedDate().toString().indexOf("}")));

        ReadWriteJSONSettings readWriteJSONSettings = new ReadWriteJSONSettings(view.getContext());
        BPM.setHint(readWriteJSONSettings.read("bps") + " tempo");
        time.setHint("00 min");

        if (readWriteJSONSettings.read("dateformat").isEmpty()) {
            readWriteJSONSettings.write("MM.DD.YYYY", "dateformat");
        }

        final String dateformat = readWriteJSONSettings.read("dateformat");

        day = (day.length() == 1) ? "0" + day : day;
        month = (month.length() == 1) ? "0" + month : month;

        switch (dateformat) {
            case "MM.DD.YYYY":
                date.setText(month + "." + day + "." + year);
                break;
            case "DD.MM.YYYY":
                date.setText(day + "." + month + "." + year);
                break;
        }

        save = (TextView) view.findViewById(R.id.SAVE);
        save.setClickable(true);
        save.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (date.getText().toString().isEmpty() || exercise.getText().toString().isEmpty() || BPM.getText().toString().isEmpty() || time.getText().toString().isEmpty()) {
                    Toast.makeText(view.getContext(), "Insert empty values", Toast.LENGTH_SHORT).show();
                } else {
                    journalRecyclerViewAdapter.addDataItem(new JournalItemModel(BPM.getText().toString(), date.getText().toString(), time.getText().toString(), exercise.getText().toString()));

                    ReadWriteJSON readWriteJSON = new ReadWriteJSON();
                    readWriteJSON.writeJSONObject(view.getContext(), BPM.getText().toString(), date.getText().toString(), time.getText().toString(), exercise.getText().toString(), dateformat);

                    try {
                        materialCalendarView.addDecorator(new HighlightDaysDecorator(view.getContext(), readWriteJSON.getAllDates(view.getContext())));
                        materialCalendarView.invalidate();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();
                    sections.add(new SimpleSectionedRecyclerViewAdapter.Section(0, date.getText().toString()));
                    SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                    SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(view.getContext(), R.layout.notes_date, R.id.notes_date_tv, journalRecyclerViewAdapter);
                    mSectionedAdapter.setSections(sections.toArray(dummy));
                    recyclerView.setAdapter(mSectionedAdapter);

                    LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.get_going);
                    linearLayout.setBackgroundResource(0);
                    linearLayout.setBackgroundColor(Color.parseColor("#393945"));
                    linearLayout.invalidate();

                    fragmentfornote.setVisibility(FrameLayout.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    //getChildFragmentManager().popBackStack();
                }
            }
        });

        discard = (TextView) view.findViewById(R.id.DISCARD);
        discard.setClickable(true);
        discard.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentfornote.setVisibility(FrameLayout.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    FirebaseCrash.log("Fail on click discard Create Journal");
                    FirebaseCrash.report(e);
                }
            }
        });

        exit = (ImageView) view.findViewById(R.id.EXIT);
        exit.setClickable(true);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    fragmentfornote.setVisibility(FrameLayout.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    FirebaseCrash.log("Fail on click exit Create Journal");
                    FirebaseCrash.report(e);
                }
            }
        });

        return view;
    }

    /**
     * Метод считывает из исходной строки календаря дату, месяц и год
     *
     * @param str - исходная строка
     */
    private void parseCalendar(String str) {
        year = str.substring(0, 4);
        str = str.substring(5, str.length());
        month = String.valueOf(Integer.parseInt(str.substring(0, str.indexOf("-"))) + 1);
        str = str.substring(str.indexOf("-") + 1, str.length());
        day = str;
    }
}