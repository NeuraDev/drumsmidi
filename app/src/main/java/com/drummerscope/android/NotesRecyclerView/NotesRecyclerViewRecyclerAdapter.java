package com.drummerscope.android.NotesRecyclerView;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.drummerscope.android.Dialogs.CreateNote.CreateNoteDialog;
import com.drummerscope.android.Dialogs.NoteImageDialog;
import com.drummerscope.android.Fragments.NotesSettingsFragment;
import com.drummerscope.android.R;
import com.drummerscope.android.Tools.ReadWriteJSONSettings;
import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import ru.rambler.libs.swipe_layout.SwipeLayout;

/**
 * Класс-адаптер для RecyclerView.
 */
public class NotesRecyclerViewRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerViewRecyclerAdapter.ViewHolder> {

    private static final String TAG = "NOTE_ADAPTER";

    /**
     * Последняя позиция адаптера
     */
    private static int lastPosition = -1;
    /**
     * Логическая переменная: было ли открыто диалоговое окно для добавления новой ноты
     */
    private static boolean createNoteDialogWasOpened = false;
    /**
     * Объект для получения и обработки результата из бекграунда и отображения его в UI-потоке
     * @see Handler
     */
    private final Handler handler = new Handler();
    /**
     * Список
     * @see NotesItemModel
     */
    private final List<NotesItemModel> data;
    /**
     * Строка
     */
    private final String NOTE = "note";
    /**
     * @see ReadWriteJSONSettings
     */
    private ReadWriteJSONSettings readWriteJSONSettings;
    /**
     * @see Context
     */
    private Context mContext;
    /**
     * Массив для хранения состояний раскрывающихся элементов
     * @see SparseBooleanArray
     */
    private SparseBooleanArray expandState = new SparseBooleanArray();
    /**
     * Фрагмент для настройки нот
     */
    private NotesSettingsFragment notesSettingsFragment;

    /**
     * Параметры верстки
     */
    private LinearLayout.LayoutParams textViewParams =
            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
    private LinearLayout.LayoutParams imageNoteViewParams =
            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
    private LinearLayout.LayoutParams imageCreateViewParams =
            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
    /**
     * Менеджер ленейного макета
     * @see LinearLayoutManager
     */
    private LinearLayoutManager mLinearLayoutManager;
    /**
     * RecyclerView для отображения элементов дорожек и нот
     * @see RecyclerView
     */
    private RecyclerView mRecyclerView;
    /**
     * Массив с исходными названиями дорожек
     */
    private String[] trackNamesDefault = {"HH", "S", "CD", "CR", "RD", "T"};

    /**
     * Конструктор
     *
     * @param data - список объектов с нотами
     * @param fragment - фрагмент для настройки нот
     * @param linearLayoutManager - менеджер линейного макета
     * @param recyclerView - RecyclerView для отображения элементов дорожек и нот
     */
    public NotesRecyclerViewRecyclerAdapter(final List<NotesItemModel> data, NotesSettingsFragment fragment, LinearLayoutManager linearLayoutManager, RecyclerView recyclerView) {
        this.data = data;
        this.notesSettingsFragment = fragment;
        for (int i = 0; i < data.size(); i++) {
            expandState.append(i, false);
        }
        mLinearLayoutManager = linearLayoutManager;
        mRecyclerView = recyclerView;
    }

    /**
     * Метод создания объекта-аниматора с возможностью запустить анимацию вращения
     *
     * @param target - целевая View
     * @param from - значение, с которого начнется анимация
     * @param to - значение, на котором закончится анимация
     * @return возвращает объект-аниматор
     */
    private static ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }

    /**
     * Метод создания объекта ViewHolder для каждого элемента RecyclerView
     * @see ViewHolder
     *
     * @param parent - родительская ViewGroup
     * @param viewType - тип View
     * @return возвращает ViewHolder соответствующего элемента RecyclerView
     */
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.mContext = parent.getContext();

        return new ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.notes_settings_recycler_view_list_row, parent, false));
    }

    /**
     * Метод отрисовки и наполнения элемента ViewHolder.
     * Тут просто ад :(
     *
     * @param holder - ViewHolder конкретного элемента RecyclerView
     * @param position - позиция адаптера
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NotesItemModel item = data.get(position);

        final DialogFragment createNoteDialog = CreateNoteDialog.newInstance(position);
        createNoteDialog.setTargetFragment(notesSettingsFragment, 0);
        final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);

        String trackName = "";
        try {
            trackName = new JSONObject(readJSON("trackNames")).getString("track" + String.valueOf(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!trackName.isEmpty())
            holder.textView.setText(trackName);
        else
            holder.textView.setText(item.description);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.textView.setVisibility(View.GONE);
                holder.editText.setText(holder.textView.getText());
                holder.editText.setVisibility(View.VISIBLE);
                holder.editText.requestFocus();

                imm.showSoftInput(holder.editText, InputMethodManager.SHOW_FORCED);

                //holder.editText.selectAll();
            }
        });


        holder.editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if (!String.valueOf(textView.getText()).matches("")) {
                        String textFromTextView = holder.editText.getText().toString();
                        holder.textView.setText(textFromTextView);
                        holder.textView.setVisibility(View.VISIBLE);
                        holder.editText.setVisibility(View.GONE);
                        holder.textView.requestFocus();
                        imm.hideSoftInputFromWindow(holder.editText.getWindowToken(), 0);
                        JSONObject trackNames;
                        try {
                            trackNames = new JSONObject(readJSON("trackNames"));
                            trackNames.put("track" + String.valueOf(position), textFromTextView);
                        } catch (JSONException e) {
                            trackNames = new JSONObject();
                            try {
                                for (int j = 0; j < 6; j++) {
                                    if (trackNames.getString("track" + String.valueOf(position)).isEmpty())
                                        trackNames.put("track" + String.valueOf(position), trackNamesDefault[j]);
                                }
                                trackNames.put("track" + String.valueOf(position), textFromTextView);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                        recordJSON("trackNames", trackNames);
                        readWriteJSONSettings.write(textFromTextView, "tracktitle");
                        return false;
                    } else {
                        Toast.makeText(mContext, "The field cann't be empty!", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                } else {
                    holder.textView.setVisibility(View.VISIBLE);
                    holder.editText.setVisibility(View.GONE);
                }
                return false;
            }
        });


        holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, item.colorId1));
        holder.expandableLayout.setBackgroundColor(ContextCompat.getColor(mContext, item.colorId2));
        holder.expandableLayout.setInterpolator(item.interpolator);
        holder.expandableLayout.setExpanded(expandState.get(position));

        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.buttonLayout, 0f, 180f).start();
                expandState.put(position, true);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mLinearLayoutManager.smoothScrollToPosition(mRecyclerView, null, holder.getAdapterPosition());
                    }
                }, 200);

                //mRecyclerView.smoothScrollToPosition(holder.linearLayoutVertical.getBottom());
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.buttonLayout, 180f, 0f).start();
                expandState.put(position, false);
            }
        });

        holder.notesTitleLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //holder.expandableLayout.toggle();
                Log.e(TAG, "EXPANDABLE HEIGHT = " + holder.expandableLayout.getHeight());
                Log.e(TAG, "EXPANDABLE CHILD COUNT = " + holder.expandableLayout.getChildCount());
                Log.e(TAG, "EXPANDABLE CLOSE POSITION = " + holder.expandableLayout.getClosePosition());
                Log.e(TAG, "LINEAR LAYOUT HEIGHT = " + holder.linearLayoutVertical.getHeight());
                Log.e(TAG, "LINEAR LAYOUT CHILD COUNT = " + holder.linearLayoutVertical.getChildCount());
                //mRecyclerView.smoothScrollToPosition(holder.linearLayoutVertical.getChildCount());
                //holder.linearLayoutVertical.requestChildFocus(holder.createImageView, holder.createImageView);
                //mRecyclerView.smoothScrollToPosition(holder.linearLayoutVertical.getBottom());
                //mRecyclerView.smoothScrollToPosition(View.FOCUS_DOWN);
                onClickButton(holder.expandableLayout, holder.linearLayoutVertical.getBottom());

            }
        });

        holder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        holder.buttonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                onClickButton(holder.expandableLayout, holder.linearLayoutVertical.getBottom());

            }
        });


        holder.linearLayoutVertical.removeAllViews();
        try {

            final JSONObject fromFileImages = new JSONObject(readJSON("images")).getJSONObject("track" + String.valueOf(position));
            final JSONObject fromFile = new JSONObject(readJSON("notes")).getJSONObject("track" + String.valueOf(position));
            Log.e(TAG, "notes: " + fromFile.toString());
            Log.e(TAG, "images: " + fromFileImages.toString());

            ImageView create = new ImageView(mContext);
            create.setImageResource(android.R.drawable.ic_input_add);
            setCreateImageViewParams(imageCreateViewParams);
            create.setLayoutParams(imageCreateViewParams);
            create.setPadding(dpToPixels(16), dpToPixels(16), dpToPixels(16), dpToPixels(16));
            holder.createImageView = create;
            holder.createImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    createNoteDialog.setCancelable(false);
                    createNoteDialog.show(((Activity) mContext).getFragmentManager(), createNoteDialog.getClass().getName());
                    createNoteDialogWasOpened = true;
                    lastPosition = position;
                }
            });

            holder.linearLayoutVertical.addView(holder.createImageView);
            for (int i = 0; i < fromFile.length(); i++) {
                String note = fromFile.getString("note" + String.valueOf(i));
                String image = fromFileImages.getString("note" + String.valueOf(i));
                int id;

                holder.myTextViewsForNotes[i].setText(note);
                switch (image) {
                    /*case "fill_circle":
                        id = R.drawable.ic_circle_notes;
                        break;*/
                    /*case "rectangle":
                        id = R.drawable.rectangle;
                        break;*/
                    case "triangle_for_settings":
                        id = R.drawable.ic_triangle_notes;
                        break;
                    case "rhombus":
                        id = R.drawable.ic_rhombus_notes;
                        break;
                    case "line":
                        id = R.drawable.ic_line_notes;
                        break;
                    case "cross_in_circle":
                        id = R.drawable.ic_cross_in_circle_notes;
                        break;
                    case "cross":
                        id = R.drawable.ic_cross_notes;
                        break;
                    default:
                        id = R.drawable.triangle_for_settings;
                        break;
                }
                holder.imageViewsNote[i].setImageResource(id);
                final int finalI = i;
                holder.swipeLayouts[i].setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
                    @Override
                    public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {

                    }

                    @Override
                    public void onSwipeClampReached(SwipeLayout swipeLayout, boolean moveToRight) {

                        if (!moveToRight) {
                            try {
                                JSONObject notes = new JSONObject(readJSON("notes"));
                                JSONObject images = new JSONObject(readJSON("images"));
                                Log.e(TAG, "READED DATA = " + notes.toString());
                                Log.e(TAG, "I TO DELETE = " + String.valueOf(finalI));
                                JSONObject trackNotes = notes.getJSONObject("track" + String.valueOf(position));
                                JSONObject trackImages = images.getJSONObject("track" + String.valueOf(position));


                                LinkedList<String> _notesList = new LinkedList<>();
                                LinkedList<String> _imagesList = new LinkedList<>();
                                for (int i = 0; i < trackNotes.length(); i++) {
                                    _notesList.add(trackNotes.getString(NOTE + String.valueOf(i)));
                                    _imagesList.add(trackImages.getString(NOTE + String.valueOf(i)));
                                }

                                String note = holder.myTextViewsForNotes[finalI].getText().toString();
                                int index = _notesList.indexOf(note);
                                _notesList.remove(index);
                                _imagesList.remove(index);

                                holder.linearLayoutVertical.removeView(holder.swipeLayouts[finalI]);
                                /*trackImages.remove("note" + String.valueOf(finalI));
                                trackNotes.remove("note" + String.valueOf(finalI));*/

                                JSONObject bufferNotes = new JSONObject();
                                JSONObject bufferImagesOfNotes = new JSONObject();

                                for (String item : _notesList) {
                                    bufferNotes.put(NOTE + String.valueOf(bufferNotes.length()), item);
                                }
                                for (String item : _imagesList) {
                                    bufferImagesOfNotes.put(NOTE + String.valueOf(bufferImagesOfNotes.length()), item);
                                }

                                try {
                                    notes.put("track" + String.valueOf(position), bufferNotes);
                                    images.put("track" + String.valueOf(position), bufferImagesOfNotes);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.e(TAG, "TO SAVE DATA = " + notes.toString());
                                recordJSON("notes", notes);
                                recordJSON("images", images);

                                holder.expandableLayout.initLayout();
                                holder.expandableLayout.expand();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onLeftStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {

                    }

                    @Override
                    public void onRightStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {

                    }
                });

                holder.linearLayoutVertical.addView(holder.swipeLayouts[i]);
                //holder.linearLayoutVertical.invalidate();
                //holder.linearLayoutVertical.requestLayout();

                holder.imageViewsNote[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        DialogFragment noteFragment = NoteImageDialog.newInstance(position, finalI);
                        noteFragment.show(((Activity) mContext).getFragmentManager(), "noteImageDialog");
                        createNoteDialogWasOpened = true;
                        lastPosition = position;
                    }
                });

            }


        } catch (JSONException e) {
            e.printStackTrace();
            ImageView create = new ImageView(mContext);
            create.setImageResource(android.R.drawable.ic_input_add);
            setCreateImageViewParams(imageCreateViewParams);
            create.setPadding(dpToPixels(16), dpToPixels(16), dpToPixels(16), dpToPixels(16));
            create.setLayoutParams(imageCreateViewParams);
            holder.createImageView = create;
            holder.createImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    createNoteDialog.setCancelable(false);
                    createNoteDialog.show(((Activity) mContext).getFragmentManager(), createNoteDialog.getClass().getName());
                }
            });

            holder.linearLayoutVertical.addView(holder.createImageView);
            holder.expandableLayout.initLayout();


        }

        holder.expandableLayout.initLayout();
        if (createNoteDialogWasOpened && (lastPosition == position)) {
            holder.expandableLayout.expand();
            createNoteDialogWasOpened = false;
            lastPosition = -1;
        }

    }

    /**
     * Метод вызывается по нажатию на раскрывающийся элемент
     *
     * @param expandableLayout
     * @param size
     */
    private void onClickButton(final ExpandableLayout expandableLayout, int size) {
        expandableLayout.toggle();
        //mRecyclerView.smoothScrollToPosition(20000);
    }

    /**
     * Метод для установки параметров верстки LinearLayout
     *
     * @param params - параметры
     */
    private void setTextViewParams(LinearLayout.LayoutParams params) {
        //params.weight = (float)0;
        params.setMargins(dpToPixels(16), dpToPixels(16), dpToPixels(0), dpToPixels(16));
        params.gravity = Gravity.END;
        params.gravity = Gravity.CENTER_VERTICAL;
    }

    /**
     * public void setDeleteImageViewParams(LinearLayout.LayoutParams params) {
     * params.gravity = Gravity.CENTER_VERTICAL;
     * params.setMargins(440, 16, 16, 16);
     * }
     */

    /**
     * Метод для установки параметров верстки ImageView
     *
     * @param params - параметры
     */
    private void setImageNoteViewParams(LinearLayout.LayoutParams params) {
        //params.weight = (float)2;
        params.gravity = Gravity.START;
        params.gravity = Gravity.CENTER_VERTICAL;
        params.setMargins(dpToPixels(16), dpToPixels(16), dpToPixels(16), dpToPixels(16));

    }

    /**
     * Метод для установки параметров верстки ImageView
     *
     * @param params - параметры
     */
    private void setCreateImageViewParams(LinearLayout.LayoutParams params) {
        params.gravity = Gravity.CENTER;
        //params.setMargins(dpToPixels(16), dpToPixels(16), dpToPixels(16), dpToPixels(16));
    }

    /**
     * Метод для получения размера списка нот
     *
     * @return возвращает размер списка
     */
    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * Метод записи json-файла
     *
     * @param name - имя файла
     * @param jsonFile - соответствующий json-файл
     */
    private void recordJSON(String name, JSONObject jsonFile) {
        File fileName = null;
        String sdState = android.os.Environment.getExternalStorageState();
        if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
            String path = mContext.getApplicationInfo().dataDir;
            fileName = new File(path, name);
        } else {
            fileName = mContext.getCacheDir();
        }

        if (!fileName.exists()) {
            fileName.getParentFile().mkdirs();
        }
        try {
            FileWriter f = new FileWriter(fileName);
            if (name.equals("notes")) {
                f.write(jsonFile.toString());
            } else {
                f.write(jsonFile.toString());
            }
            f.flush();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод чтения json-файла
     *
     * @param name - имя файла
     */
    private String readJSON(String name) {
        try {
            String path = mContext.getApplicationInfo().dataDir;
            File fileName = new File(path, name);
            FileInputStream fiStream = new FileInputStream(fileName);
            byte[] bytes = new byte[fiStream.available()];
            fiStream.read(bytes);
            fiStream.close();

            String jsonStr = new String(bytes, "UTF-8");
            JSONObject jsonObj = new JSONObject(jsonStr);
            return jsonObj.toString();
        } catch (JSONException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
            ex.printStackTrace();
            return "1";
        }
    }

    /**
     * Метод конвертирует dp значения в pixels (пиксели)
     *
     * @param dpValue - исходное значение dp
     * @return возвращает целочисленное значение - количество пикселей
     */
    private int dpToPixels(int dpValue) {
        float d = mContext.getResources().getDisplayMetrics().density;
        return (int) (dpValue * d);
    }

    /**
     * Класс, отвечающий за работу ViewHolder элементов RecyclerView
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        final int N = 10; // total number of textviews to add
        public TextView textView;
        public RelativeLayout buttonLayout;
        public EditText editText;
        public TextView[] myTextViewsForNotes = new TextView[N];
        public ImageView[] imageViewsDelete = new ImageView[N];
        public ImageView[] imageViewsNote = new ImageView[N];
        //public LinearLayout[] linearLayouts = new LinearLayout[N];
        public ImageView createImageView;
        public SwipeLayout[] swipeLayouts = new SwipeLayout[N];
        public LinearLayout linearLayoutVertical;
        public LinearLayout centralLinearLayoutInSwipe;
        /**
         * You must use the ExpandableLinearLayout in the recycler view.
         * The ExpandableRelativeLayout doesn't work.
         */
        public ExpandableLinearLayout expandableLayout;
        private LinearLayout notesTitleLinearLayout;

        /**
         * Конструктор
         *
         * @param v - View, которой управляет ViewHolder
         */
        public ViewHolder(View v) {
            super(v);
            readWriteJSONSettings = new ReadWriteJSONSettings(v.getContext());
            editText = (EditText) v.findViewById(R.id.notes_title_edit_text);

            textView = (TextView) v.findViewById(R.id.notes_title_text_view);

            notesTitleLinearLayout = (LinearLayout) v.findViewById(R.id.notes_title_linear_layout);

            buttonLayout = (RelativeLayout) v.findViewById(R.id.open_close_list);
            expandableLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
            linearLayoutVertical = (LinearLayout) v.findViewById(R.id.linear_layout_vertical_expandable_child);
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            for (int i = 0; i < N; i++) {

                SwipeLayout swipeLayout = (SwipeLayout) inflater.inflate(R.layout.swipe_layout_note, null);
                centralLinearLayoutInSwipe = (LinearLayout) swipeLayout.findViewById(R.id.central_linear_in_swipe);

                ImageView noteImage = new ImageView(v.getContext());
                noteImage.setImageResource(R.drawable.triangle_for_settings);
                setImageNoteViewParams(imageNoteViewParams);
                noteImage.setLayoutParams(imageNoteViewParams);

                final TextView rowTextView = new TextView(v.getContext());
                rowTextView.setTextColor(v.getContext().getResources().getColor(R.color.myWhite));
                setTextViewParams(textViewParams);
                rowTextView.setLayoutParams(textViewParams);

                centralLinearLayoutInSwipe.addView(rowTextView);
                centralLinearLayoutInSwipe.addView(noteImage);

                myTextViewsForNotes[i] = rowTextView;
                imageViewsNote[i] = noteImage;
                swipeLayouts[i] = swipeLayout;

            }
        }

    }
}