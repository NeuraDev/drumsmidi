package com.drummerscope.android.NotesRecyclerView;

import android.animation.TimeInterpolator;

/**
 * Класс, описывающий модель компонента для отображения ноты в меню настройки нот.
 */
public class NotesItemModel {
    /**
     * Название ноты
     */
    public final String description;
    /**
     * ID цвета
     */
    public final int colorId1;
    public final int colorId2;
    /**
     * @see TimeInterpolator
     */
    public final TimeInterpolator interpolator;

    public NotesItemModel(String description, int colorId1, int colorId2, TimeInterpolator interpolator) {
        this.description = description;
        this.colorId1 = colorId1;
        this.colorId2 = colorId2;
        this.interpolator = interpolator;
    }
}