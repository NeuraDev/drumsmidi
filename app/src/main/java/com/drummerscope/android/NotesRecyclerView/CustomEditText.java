package com.drummerscope.android.NotesRecyclerView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

import com.drummerscope.android.R;

/**
 * Класс, отвечающий за работу кастомного текстового поля EditText.
 * Необходимо, чтобы после завершения изменения названия дорожки и нажатия на back
 * это поле скрывалось.
 *
 * Created by evgenyrasskazov on 15.02.2017.
 */

public class CustomEditText extends EditText {

    Context mContext;
    private EditText editText;

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        editText = (EditText) findViewById(R.id.notes_title_edit_text);
    }

    /**
     * Метод обработки нажатия. Если нажатие back, то скрыть компонент EditText.
     *
     * @param keyCode - код клавиши
     * @param event - тип события
     * @return
     */
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // User has pressed Back key. So hide the keyboard
            //InputMethodManager mgr = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            //mgr.hideSoftInputFromWindow(this.getWindowToken(), 0);
            editText.setVisibility(GONE);

            // TODO: Hide your view as you do it in your activity
        }
        return false;
    }


}